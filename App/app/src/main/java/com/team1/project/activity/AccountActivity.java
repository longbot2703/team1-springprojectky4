package com.team1.project.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.team1.project.model.Notify;
import com.team1.project.adapter.NotifyAdapter;
import com.team1.project.R;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class AccountActivity extends AppCompatActivity {
    private List<Notify> notifyList = new ArrayList<>();
    TextView tvNotifyData ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notify);

        tvNotifyData = findViewById(R.id.tv_notify_data);


        // navigate in home
        BottomNavigationView bottomNavigationView = findViewById(R.id.btn_navigate_notify);

        bottomNavigationView.setSelectedItemId(R.id.account);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull @NotNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.cart:
                        startActivity(new Intent(getApplicationContext(), CartActivity.class));
                        overridePendingTransition(0,0);
                        return true;
                    case R.id.order:
                        startActivity(new Intent(getApplicationContext(), OrderActivity.class));
                        overridePendingTransition(0,0);
                        return true;
                    case R.id.like:
                        startActivity(new Intent(getApplicationContext(), LikeActivity.class));
                        overridePendingTransition(0,0);
                        return true;
                    case R.id.home:
                        startActivity(new Intent(getApplicationContext(), HomeActivity.class));
                        overridePendingTransition(0,0);
                        return true;

                }
                return false;
            }
        });

        //set data using adapter => list notify



    }

    private void initData(){
        notifyList.add(new Notify(1,R.drawable.ic_notify,"Title image","Title notify","Description notify","date time noti"));
        notifyList.add(new Notify(2,R.drawable.ic_notify,"Title image","Title notify 2","Description notify","date time noti2"));
        notifyList.add(new Notify(3,R.drawable.ic_notify,"Title image","Title notify 3","Description notify","date time noti3"));
    }
}