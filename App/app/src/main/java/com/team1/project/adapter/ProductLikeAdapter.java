package com.team1.project.adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.team1.project.R;
import com.team1.project.model.Product;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class ProductLikeAdapter extends RecyclerView.Adapter {
    Activity activity;
    List<Product> productList;


    public ProductLikeAdapter( Activity activity,List<Product> productList) {
        this.productList = productList;
        this.activity = activity;
    }

    @NonNull
    @NotNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View itemView = activity.getLayoutInflater().inflate(R.layout.product_like_item,parent,false);
        ProductLikeViewHolder holder = new ProductLikeViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull RecyclerView.ViewHolder holder, int position) {
        ProductLikeViewHolder vh = (ProductLikeViewHolder) holder;
        Product data = productList.get(position);
        vh.tv_count_like.setText("222");
        vh.tv_product_name.setText(data.getName());
        vh.tv_price.setText("$20000");
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    public class ProductLikeViewHolder extends RecyclerView.ViewHolder{
        TextView tv_count_like, tv_product_name, tv_price;
        ImageView imv_like;

        public ProductLikeViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            tv_count_like = itemView.findViewById(R.id.tv_count_like);
            tv_product_name = itemView.findViewById(R.id.tv_product_name);
            tv_price = itemView.findViewById(R.id.tv_price);
            imv_like = itemView.findViewById(R.id.imv_like);
        }
    }
}
