package com.team1.project.activity.account;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.team1.project.config.Constant;
import com.team1.project.activity.HomeActivity;
import com.team1.project.R;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    EditText ed_username,ed_password;
    Button btn_login,btn_register;
    TextView tv_forget_password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ed_username = findViewById(R.id.ed_username);
        ed_password = findViewById(R.id.ed_password);
        btn_login = findViewById(R.id.btn_login);
        btn_register = findViewById(R.id.btn_register);
        tv_forget_password = findViewById(R.id.tv_forget_pass);

        btn_register.setOnClickListener(this);
        btn_login.setOnClickListener(this);
        tv_forget_password.setOnClickListener(this);

        ed_password.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return false;
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_login:
                if(ed_username.getText().toString().isEmpty() || ed_password.getText().toString().isEmpty()){
                    Toast.makeText(LoginActivity.this,"Hãy nhập đầy đủ thông tin",Toast.LENGTH_LONG).show();
                }else {
                    Toast.makeText(LoginActivity.this,"Đăng nhập thành công ",Toast.LENGTH_LONG).show();
                    saveStateLogin();
                    if(getStateLogin()){
                        goHome();
                        finish();
                    }
                }
                break;
            case  R.id.btn_register:
                goRegister();
                break;
            case R.id.tv_forget_pass:
                goForgotPassword();
                break;
            default:
                break;
        }

    }
    private void goHome(){
        Intent main = new Intent(LoginActivity.this, HomeActivity.class);
        startActivity(main);
    }

    private void goRegister(){
        Intent main = new Intent(LoginActivity.this, RegisterActivity.class);
        startActivity(main);
    }

    private void goForgotPassword(){
        Intent main = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
        startActivity(main);
    }

    private void saveStateLogin(){
        // tạo
        SharedPreferences setting =getSharedPreferences(Constant.File_name,MODE_PRIVATE);
        // ghi
        SharedPreferences.Editor editor = setting.edit();
        editor.putBoolean(Constant.Is_login,true);
        editor.putString(Constant.User_name,ed_username.getText().toString());
        // lưu
        editor.commit();
    }

    public boolean getStateLogin(){
        SharedPreferences setting =getSharedPreferences(Constant.File_name,MODE_PRIVATE);
        // getdata , mặc định là false
        boolean isLogin = setting.getBoolean(Constant.Is_login, false);
        return isLogin;
    }
}