package com.team1.project.config;

import java.util.regex.Pattern;

public class Constant {
    public static final String BASE_SERVER = "http://172.17.32.1:8001/";
//    public static final String BASE_SERVER = "http://apinongnghiepsach.cf/";

    public static final  String Token ="TOKEN";
    public static final  String File_name ="Vegetable";
    public static final  String Is_login ="IS_LOGIN";
    public static final  String User_name ="USER_NAME";
    public static final  String Full_name ="FULL_NAME";

    public static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

}
