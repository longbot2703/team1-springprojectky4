package com.team1.project.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.team1.project.R;
import com.team1.project.model.Product;
import com.team1.project.adapter.ProductLikeAdapter;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class LikeActivity extends AppCompatActivity {
    private List<Product> productList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_like);

        // navigate in home
        BottomNavigationView bottomNavigationView = findViewById(R.id.btn_navigate_like);

        bottomNavigationView.setSelectedItemId(R.id.like);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull @NotNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.cart:
                        startActivity(new Intent(getApplicationContext(), CartActivity.class));
                        overridePendingTransition(0,0);
                        return true;
                    case R.id.order:
                        startActivity(new Intent(getApplicationContext(), OrderActivity.class));
                        overridePendingTransition(0,0);
                        return true;
                    case R.id.home:
                        startActivity(new Intent(getApplicationContext(), HomeActivity.class));
                        overridePendingTransition(0,0);
                        return true;
                    case R.id.account:
                        startActivity(new Intent(getApplicationContext(), AccountActivity.class));
                        overridePendingTransition(0,0);
                        return true;

                }
                return false;
            }
        });


        //set data using adapter => list notify

        // B1:Data Source
        initData();

        //B2 : Adapter
        ProductLikeAdapter adapter =  new ProductLikeAdapter(this,productList);

        //B3 : Layout Manager
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this,1);

        //B4 : RecyclerView
        RecyclerView rvNotify = findViewById(R.id.rvProductLike);
        rvNotify.setLayoutManager(layoutManager);
        rvNotify.setAdapter(adapter);
    }

    private void initData(){
//        productList.add(new Product(1,"Táo đỏ 1","Táo đỏ trung quốc",R.drawable.avata_image,false,11));
//        productList.add(new Product(1,"Táo đỏ 2","Táo đỏ trung quốc",R.drawable.avata_image,false,11));
//        productList.add(new Product(1,"Táo đỏ 3","Táo đỏ trung quốc",R.drawable.avata_image,false,11));
     }
}