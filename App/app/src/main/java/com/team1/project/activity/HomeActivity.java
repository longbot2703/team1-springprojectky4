package com.team1.project.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.team1.project.R;
import com.team1.project.adapter.CategoryAdapter;
import com.team1.project.adapter.NotifyAdapter;
import com.team1.project.model.Category;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class HomeActivity extends AppCompatActivity {
    private List<Category> listCate = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        // navigate in home
        BottomNavigationView bottomNavigationView = findViewById(R.id.tab_food_home);

        bottomNavigationView.setSelectedItemId(R.id.home);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull @NotNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.cart:
                        startActivity(new Intent(getApplicationContext(), CartActivity.class));
                        overridePendingTransition(0,0);
                        return true;
                    case R.id.order:
                        startActivity(new Intent(getApplicationContext(), OrderActivity.class));
                        overridePendingTransition(0,0);
                        return true;
                    case R.id.like:
                        startActivity(new Intent(getApplicationContext(), LikeActivity.class));
                        overridePendingTransition(0,0);
                        return true;
                    case R.id.account:
                        startActivity(new Intent(getApplicationContext(), AccountActivity.class));
                        overridePendingTransition(0,0);
                        return true;

                }
                return false;
            }
        });

        // B1:Data Source
        initData();

        //B2 : Adapter
        CategoryAdapter adapter =  new CategoryAdapter(this,listCate);

        //B3 : Layout Manager
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this,2);

        //B4 : RecyclerView
        RecyclerView rvCategory = findViewById(R.id.rvCategory);
        rvCategory.setLayoutManager(layoutManager);
        rvCategory.setAdapter(adapter);
    }

    private void initData() {
        listCate.add(new Category("Danh mục 1"));
        listCate.add(new Category("Danh mục 2"));
        listCate.add(new Category("Danh mục 3"));
        listCate.add(new Category("Danh mục 4"));
    }
}