package com.team1.project.activity.account;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.team1.project.R;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {
    EditText ed_username, ed_password, ed_check_pass;
    Button btn_register;
    TextView tv_gologin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        ed_username = findViewById(R.id.ed_username);
        ed_password = findViewById(R.id.ed_password);
        ed_check_pass = findViewById(R.id.ed_check_password);
        btn_register = findViewById(R.id.btn_register);
        tv_gologin = findViewById(R.id.tv_goLogin);

        btn_register.setOnClickListener(this);
        tv_gologin.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_login:
                break;
            case R.id.tv_goLogin:
                break;
            default:
                break;
        }

    }
}