package com.team1.project.model;

public class Product {
    private int id;
    private String name;
    private String description;
    private String imageUrl;
    private int image;
    private String unit;
    private boolean like;
    private float quantity;
    private long price;
    private int discount;
    private int status;
    private int sellerby;

    public Product() {
    }

    public Product(int id, String name, String description, int image, boolean like, float quantity, long price, int discount, int status) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.image = image;
        this.like = like;
        this.quantity = quantity;
        this.price = price;
        this.discount = discount;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public boolean isLike() {
        return like;
    }

    public void setLike(boolean like) {
        this.like = like;
    }

    public float getQuantity() {
        return quantity;
    }

    public void setQuantity(float quantity) {
        this.quantity = quantity;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getSellerby() {
        return sellerby;
    }

    public void setSellerby(int sellerby) {
        this.sellerby = sellerby;
    }
}
