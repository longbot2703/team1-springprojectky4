package com.team1.project.adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.team1.project.R;
import com.team1.project.model.Notify;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class NotifyAdapter  extends RecyclerView.Adapter {

    Activity activity;
    List<Notify> notifyList;

    public NotifyAdapter(Activity activity, List<Notify> notifyList) {
        this.activity = activity;
        this.notifyList = notifyList;
    }

    @NonNull
    @NotNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View itemView = activity.getLayoutInflater().inflate(R.layout.notify_item,parent,false);
        NotifyViewHolder holder = new NotifyViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull RecyclerView.ViewHolder holder, int position) {
        NotifyViewHolder vh = (NotifyViewHolder) holder;
        Notify data = notifyList.get(position);
        vh.tvTitle.setText(data.getTitle());
        vh.tvDescription.setText(data.getDescription());
        vh.tvDatetime.setText(data.getDate());
    }

    @Override
    public int getItemCount() {
        return notifyList.size();
    }

    public class NotifyViewHolder extends RecyclerView.ViewHolder{
        TextView tvTitle, tvDescription, tvDatetime;
        ImageView imvImage;

        public NotifyViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tv_notify_title);
            tvDescription = itemView.findViewById(R.id.tv_notify_description);
            tvDatetime = itemView.findViewById(R.id.tv_notify_datetime);
            imvImage = itemView.findViewById(R.id.imv_notifyImage);
        }
    }
}
