package com.team1.project.activity.account;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.team1.project.config.Constant;
import com.team1.project.R;

import java.util.regex.Matcher;

public class ForgotPasswordActivity extends AppCompatActivity implements View.OnClickListener {
    EditText ed_email;
    Button btn_confirm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        ed_email = findViewById(R.id.ed_email);
        btn_confirm = findViewById(R.id.btn_confirm);

        btn_confirm.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_confirm:
                String email = ed_email.getText().toString();
                String mess = "Hãy nhập đầy đủ và đúng định dạng email";
                if(!email.isEmpty()){
                    Matcher matcher = Constant.VALID_EMAIL_ADDRESS_REGEX.matcher(email);
                    if(matcher.find()){
                        mess="Vui lòng kiểm tra lại email";
                    }
                }
                Toast.makeText(ForgotPasswordActivity.this,mess,Toast.LENGTH_LONG).show();
                break;
            default:
                break;
        }
    }
}