package com.team1.project.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.team1.project.R;
import com.team1.project.config.Constant;

import org.jetbrains.annotations.NotNull;

public class CartActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);

        // navigate in home
        BottomNavigationView bottomNavigationView = findViewById(R.id.btn_navigate_cart);

        bottomNavigationView.setSelectedItemId(R.id.cart);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull @NotNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.home:
                        startActivity(new Intent(getApplicationContext(), HomeActivity.class));
                        overridePendingTransition(0,0);
                        return true;
                        
                    case R.id.order:
                        if(getStateLogin()){
                            startActivity(new Intent(getApplicationContext(), OrderActivity.class));
                            overridePendingTransition(0,0);
                            return true;
                        }else {
                            Toast.makeText(CartActivity.this,"Đăng nhập để sử dụng chức năng này",Toast.LENGTH_LONG).show();
                            return false;
                        }

                    case R.id.like:
                        startActivity(new Intent(getApplicationContext(), LikeActivity.class));
                        overridePendingTransition(0,0);
                        return true;

                    case R.id.account:
                        startActivity(new Intent(getApplicationContext(), AccountActivity.class));
                        overridePendingTransition(0,0);
                        return true;

                }
                return false;
            }
        });
    }

    public boolean getStateLogin(){
        SharedPreferences setting =getSharedPreferences(Constant.File_name,MODE_PRIVATE);
        // getdata , mặc định là false
        boolean isLogin = setting.getBoolean(Constant.Is_login, false);
        return isLogin;
    }
}