package com.team1.project.adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.team1.project.R;
import com.team1.project.model.Category;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class CategoryAdapter extends RecyclerView.Adapter{

    Activity activity;
    List<Category> listCate;

    public CategoryAdapter(Activity activity, List<Category> listCate) {
        this.activity = activity;
        this.listCate = listCate;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = activity.getLayoutInflater().inflate(R.layout.category_item,parent,false);
        CategoryViewHolder holder =  new CategoryViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        CategoryViewHolder vh = (CategoryViewHolder) holder;
        Category data = listCate.get(position);
        vh.tvName.setText(data.getName());
    }

    @Override
    public int getItemCount() {
        return listCate.size();
    }

    public class CategoryViewHolder extends RecyclerView.ViewHolder{
        TextView tvName, tvDescription, tvDatetime;
        ImageView imvImage;

        public CategoryViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tv_cateName);
//            tvDescription = itemView.findViewById(R.id.tv_notify_description);
//            tvDatetime = itemView.findViewById(R.id.tv_notify_datetime);
//            imvImage = itemView.findViewById(R.id.imv_notifyImage);
        }
    }
}
