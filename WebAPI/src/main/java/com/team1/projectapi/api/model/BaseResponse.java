package com.team1.projectapi.api.model;

public class BaseResponse<T> {
    public int status = 0;
    public int code = 400;
    public String message = "Error";
    public T data;

    public BaseResponse() {
    }

    public BaseResponse(int status, int code, String message, T data) {
        this.status = status;
        this.code = code;
        this.message = message;
        this.data = data;
    }
}
