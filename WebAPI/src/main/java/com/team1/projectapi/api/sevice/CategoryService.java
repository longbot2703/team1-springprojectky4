package com.team1.projectapi.api.sevice;

import com.team1.projectapi.api.dto.CategoryDto;
import com.team1.projectapi.api.entity.CategoryEntity;

import java.util.List;

public interface CategoryService {
    List<CategoryEntity> listCategory(String searchName);
    CategoryEntity getCateDetail(int cateid);
    CategoryDto convertEntityToDto(CategoryEntity cateEntity);
}
