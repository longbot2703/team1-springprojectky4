package com.team1.projectapi.api.sevice;

import com.team1.projectapi.api.config.Constants;
import com.team1.projectapi.api.dto.ProductDto;
import com.team1.projectapi.api.entity.ProductEntity;
import com.team1.projectapi.api.repository.ProductRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductServiceImpl implements ProductService{

    @Autowired
    ProductRepo productRepo;

    @Override
    public List<ProductDto> convertListEntityToDto(List<ProductEntity> listProductEntity) {
        if(listProductEntity != null){
            List<ProductDto> listProductDto = new ArrayList<>();
            for (ProductEntity prEntity : listProductEntity){
                ProductDto prDto = this.convertEntityToDto(prEntity);
                if (prDto != null){
                    listProductDto.add(prDto);
                }
            }
            return listProductDto;
        }
        return null;
    }

    @Override
    public ProductDto convertEntityToDto(ProductEntity productEntity) {
        ProductDto prDto = new ProductDto();
        if(productEntity != null){
            prDto.setId(productEntity.getId());
            prDto.setName(productEntity.getName());
            prDto.setDescription(productEntity.getDescription());
            prDto.setImage(prDto.getImage() != null ? Constants.DOMAIN + Constants.FILEPATH_IMAGE + productEntity.getImage() : null);
            prDto.setDiscount(productEntity.getDiscount());
            prDto.setPrice(productEntity.getPrice());
            prDto.setImagedetail(productEntity.getImagedetail());
            prDto.setQuantity(productEntity.getQuantity());
            prDto.setSellerby(productEntity.getSellerby());
            prDto.setStatus(productEntity.getStatus());
            prDto.setUnit(productEntity.getUnit());
            return prDto;
        }
        return null;
    }

    @Override
    public List<ProductEntity> getListByCateId(int cateid, String search) {
        if(cateid > 0){
            return productRepo.getListByCateidAndSearchName(cateid,search);
        }
        return null;
    }

    @Override
    public List<ProductEntity> listAllProduct(String search) {
        return productRepo.getListProduct(search);
    }

    @Override
    public ProductEntity getProductDetail(int id) {
        return productRepo.getProductDetail(id);
    }
}
