package com.team1.projectapi.api.repository;

import com.team1.projectapi.api.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepo extends JpaRepository<UserEntity,Integer> {
    UserEntity findByUsernameAndIsactive(String username, int isactive);
    UserEntity findByEmailAndIsactive(String email, int isactive);
}
