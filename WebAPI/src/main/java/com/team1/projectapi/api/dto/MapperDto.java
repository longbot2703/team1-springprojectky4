package com.team1.projectapi.api.dto;

import com.team1.projectapi.api.entity.CategoryEntity;
import com.team1.projectapi.api.entity.UserEntity;
import org.mapstruct.Mapper;
import org.mapstruct.NullValueCheckStrategy;

import java.util.List;

@Mapper(componentModel = "spring", nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface MapperDto {
//    UserEntity convertRegisterDtoToUserEntity(RegisterDto registerDto);
    CategoryEntity convertToCategoryEntity(CategoryDto categoryDto);
    CategoryDto convertToCategoryDto(CategoryEntity categoryEntity);
    List<CategoryDto> convertToAllCateDto(List<CategoryEntity> listCateEntity);
}
