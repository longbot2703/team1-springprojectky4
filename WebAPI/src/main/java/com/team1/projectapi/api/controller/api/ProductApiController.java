package com.team1.projectapi.api.controller.api;

import com.team1.projectapi.api.config.Constants;
import com.team1.projectapi.api.dto.CategoryDto;
import com.team1.projectapi.api.dto.ProductDto;
import com.team1.projectapi.api.dto.SearchDto;
import com.team1.projectapi.api.entity.CategoryEntity;
import com.team1.projectapi.api.entity.ProductEntity;
import com.team1.projectapi.api.model.BaseResponse;
import com.team1.projectapi.api.sevice.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/public/product")
public class ProductApiController {
    @Autowired
    ProductService productService;

    /**
     * Danh sách sản phẩm
     * @param input
     * @return
     */
    @PostMapping("")
    public ResponseEntity listProduct(@RequestBody SearchDto input) {
        BaseResponse res = new BaseResponse();
        List<ProductEntity> listEntity = new ArrayList<>();
        if(input.getId() > 0){
            listEntity = productService.getListByCateId(input.getId(),input.getSearch());
        }else {
            listEntity = productService.listAllProduct(input.getSearch());
        }

        List<ProductDto> listDto = productService.convertListEntityToDto(listEntity);

        res.status = 1;
        res.code = 200;
        res.message = "Thành công";
        res.data= listDto;
        return ResponseEntity.ok(res);
    }

    /**
     * chi tiết sản phẩm
     * @param input
     * @return
     */
    @PostMapping({"/detail"})
    public ResponseEntity productDetail(@RequestBody SearchDto input){
        BaseResponse res = new BaseResponse();
        if(input.getId() > 0){
            ProductDto productDto = productService.convertEntityToDto(productService.getProductDetail(input.getId()));
            res.status = 1;
            res.code = 200;
            res.message = "Thành công ";
            res.data = productDto;
        }else {
            res.status = 0;
            res.code = 502;
            res.message = "Thất bại";
        }
        return ResponseEntity.ok(res);
    }

}
