package com.team1.projectapi.api.dto;

import lombok.Data;

@Data
public class UserDto {
    private int id;
    private String username;
    private String password;
    private String email;
    private String token;
    private String deviceid;
}
