package com.team1.projectapi.api.controller.api;

import com.team1.projectapi.api.config.Constants;
import com.team1.projectapi.api.dto.CategoryDto;
import com.team1.projectapi.api.dto.MapperDto;
import com.team1.projectapi.api.dto.ProductDto;
import com.team1.projectapi.api.dto.SearchDto;
import com.team1.projectapi.api.entity.CategoryEntity;
import com.team1.projectapi.api.entity.ProductEntity;
import com.team1.projectapi.api.model.BaseResponse;
import com.team1.projectapi.api.sevice.CategoryService;
import com.team1.projectapi.api.sevice.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/public/category")
public class CategoryApiController {
    @Autowired
    CategoryService categoryService;

    @Autowired
    ProductService productService;

    @Autowired
    MapperDto mapperDto;


    @PostMapping("")
    public ResponseEntity listCate( @RequestBody SearchDto input) {
        BaseResponse res = new BaseResponse();
        List<CategoryEntity> listEntity = categoryService.listCategory(input.getSearch());
//        List<CategoryDto> listDto = mapperDto.convertToAllCateDto(listEntity);
        List<CategoryDto> listDto = new ArrayList<>();

        for (CategoryEntity cate : listEntity){
            if(cate != null){
                CategoryDto cateDto = new CategoryDto();
                cateDto.setId(cate.getId());
                cateDto.setName(cate.getName());
                cateDto.setImage(Constants.DOMAIN + Constants.FILEPATH_IMAGE + cate.getImage());
                cateDto.setDescription(cate.getDescription());

                listDto.add(cateDto);
            }
        }

        res.status = 1;
        res.code = 200;
        res.message = "Thành công";
        res.data= listDto;
        return ResponseEntity.ok(res);
    }

    @PostMapping("/detail")
    public ResponseEntity cateDetail( @RequestBody SearchDto input) {
        BaseResponse res = new BaseResponse();
        if(input.getId() > 0){
            CategoryEntity cateEntity = categoryService.getCateDetail(input.getId());
            CategoryDto cateDto = categoryService.convertEntityToDto(cateEntity);
            if(cateDto != null){
//                List<ProductEntity> listPro = productService.getListByCateId(input.getId(),input.getSearch());
                cateDto.setListProduct(productService.convertListEntityToDto(productService.getListByCateId(input.getId(),input.getSearch())));

                res.status = 1;
                res.code = 200;
                res.message = "Thành công";
                res.data = cateDto;
            }else {
                res.code = 400;
                res.message = "Không tồn tại dữ liệu";
            }
        }else {
            res.code = 500;
            res.message = "Danh mục không tồn tại";
        }
        return ResponseEntity.ok(res);
    }

}
