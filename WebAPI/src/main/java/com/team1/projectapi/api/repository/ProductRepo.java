package com.team1.projectapi.api.repository;

import com.team1.projectapi.api.entity.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ProductRepo extends JpaRepository<ProductEntity,Integer> {
    @Query("SELECT p from ProductEntity p where p.isactive = 1 and p.cateid = :cateid " +
            "and p.name like concat('%',:productName,'%') order by p.id desc ")
    List<ProductEntity> getListByCateidAndSearchName(@Param("cateid") int cateid, @Param("productName") String productName);

    @Query("SELECT p from ProductEntity p where p.isactive = 1 " +
            "and p.name like concat('%',:productName,'%') order by p.id desc")
    List<ProductEntity> getListProduct(@Param("productName") String productName);

    @Query("SELECT p from ProductEntity p where p.isactive = 1 and  p.id = :id")
    ProductEntity getProductDetail(@Param("id") int id);
}
