package com.team1.projectapi.api.sevice;

import com.team1.projectapi.api.dto.ProductDto;
import com.team1.projectapi.api.entity.ProductEntity;

import java.util.List;

public interface ProductService {
    List<ProductDto> convertListEntityToDto(List<ProductEntity> listProductEntity);

    ProductDto convertEntityToDto(ProductEntity productEntity);

    List<ProductEntity> getListByCateId(int cateid ,String search);

    List<ProductEntity> listAllProduct(String search);

    ProductEntity getProductDetail(int id);
}
