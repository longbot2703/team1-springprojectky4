package com.team1.projectapi.api.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "cart")
public class CartEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "productid",nullable = false)
    private int productid;

    @Column(name = "userid",nullable = false)
    private int userid;

    @ColumnDefault("0")
    @Column(name = "quantity",nullable = false)
    private int quantity;

    @JsonIgnore
    @ColumnDefault("1")
    @Column(name = "isactive",nullable = false)
    private int isactive;

    @JsonIgnore
    @Column(name = "createat")
    private Timestamp createat;

    @JsonIgnore
    @Column(name = "updateat")
    private Timestamp updateat;


    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY) //EAGER
    @JoinColumn(name = "userid", insertable = false, updatable = false)
    private UserEntity cartUser;

    @JsonIgnore
    @ManyToOne( fetch = FetchType.LAZY) //EAGER
    @JoinColumn(name = "productid", insertable = false, updatable = false)
    private ProductEntity cartProduct;


    public CartEntity() {
    }

    public CartEntity(int productid, int userid, int quantity, int isactive) {
        this.productid = productid;
        this.userid = userid;
        this.quantity = quantity;
        this.isactive = isactive;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getProductid() {
        return productid;
    }

    public void setProductid(int productid) {
        this.productid = productid;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getIsactive() {
        return isactive;
    }

    public void setIsactive(int isactive) {
        this.isactive = isactive;
    }

    public Timestamp getCreateat() {
        return createat;
    }

    public void setCreateat(Timestamp createat) {
        this.createat = createat;
    }

    public Timestamp getUpdateat() {
        return updateat;
    }

    public void setUpdateat(Timestamp updateat) {
        this.updateat = updateat;
    }

    public UserEntity getCartUser() {
        return cartUser;
    }

    public void setCartUser(UserEntity cartUser) {
        this.cartUser = cartUser;
    }

    public ProductEntity getCartProduct() {
        return cartProduct;
    }

    public void setCartProduct(ProductEntity cartProduct) {
        this.cartProduct = cartProduct;
    }
}
