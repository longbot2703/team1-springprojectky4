package com.team1.projectapi.api.config;

public class Constants {
    public static final int PAGE_SIZE_WEB = 8;
    public static final int PAGE_SIZE_APP = 12;
    public static final int ACTIVE = 1;
    public static final int NO_ACTIVE = 0;
    public static final int MALE = 0; // nam
    public static final int FEMALE = 1; // nữ

    public static final String DOMAIN = "http://nongnghiepsach.cf";
    public static final String FILEPATH_IMAGE = "/images/";
//    http://172.17.32.1:8001/


    // config table role

    public static final String ROLE_SUPERADMIN= "SuperAdmin";
    public static final String ROLE_SUPERADMIN_ID= "1";
    public static final String ROLE_ADMIN= "Admin";
    public static final String ROLE_SELLER= "Seller";
    public static final int ROLE_SELLER_ID= 3;
    public static final String ROLE_BUYER= "Buyer";

    // config table order
    public static final int ORDER_TYPE = 1;
    public static final int CARD_TYPE = 2;

    public static final int ORDER_WAIT_CONFIRM= 0; // đơn hàng chờ xác nhận
    public static final int ORDER_CONFIRM = 1; // đơn hàng đã xác nhận
    public static final int ORDER_FINISH = 2; // đơn hàng đã hoàn thành
    public static final int ORDER_CANCEL = -1; // đơn hàng đã hủy
    public static final int ORDER_REFUSE = -2; // đơn hàng đã từ chối
    public static final String ORDER_CREATE_SESSION = "ojhfie"; // đơn hàng đã hủy



    // config chi tiết đơn hàng


    // config News
    public static final int NEWS_TYPE = 1;
    public static final int BANNER_TYPE = 2;

    // config sản phẩm
    public static final int PRODUCT_OUT_OF_STOCK = 0; // sản phẩm hết hàng
    public static final int PRODUCT_AVAILABLE = 1; // sản phẩm còn hàng
    public static final int PRODUCT_NOT_AVAILABLE = 99; // sản phẩm ngừng kinh doanh

    // Replace with your email here:
    public static final String SYSTEM_EMAIL = "sannongnghiepsach@gmail.com";

    // Replace password!!
    public static final String SYSTEM_PASSWORD = "hsobgfnqokhwlixz";

    // And receiver!
    public static final String ADMIN_EMAIL = "nguyenminhan696@gmail.com";

    // Folder Anh
    public static final String IMAGE_FOLDER = "./img";
    public static final String IMAGE_FOLDER_PRODUCT = "./img/product/";
}
