package com.team1.projectapi.api.sevice;

import com.team1.projectapi.api.entity.UserEntity;

public interface UserService {
    UserEntity findUserByToken(String token);
    boolean checkEmail(String email);
    boolean checkUsername(String username);
    UserEntity createBuyer(String username, String email, String password);
}
