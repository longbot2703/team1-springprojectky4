package com.team1.projectapi.api.controller.api;

import com.team1.projectapi.api.dto.RegisterDto;
import com.team1.projectapi.api.dto.UserDto;
import com.team1.projectapi.api.model.BaseResponse;
import com.team1.projectapi.api.repository.RoleRepo;
import com.team1.projectapi.api.repository.UserRepo;
import com.team1.projectapi.api.security.jwt.JwtUtil;
import com.team1.projectapi.api.security.service.UserDetailImpl;
import com.team1.projectapi.api.sevice.UserService;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api/public")
public class AuthApiController {
    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserRepo userRepository;

    @Autowired
    RoleRepo roleRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtUtil jwtUtil;

    @Autowired
    UserService userService;


    @PostMapping("/login")
    public ResponseEntity login(@RequestBody UserDto input){
        BaseResponse res = new BaseResponse();
        if(!input.getUsername().isEmpty() && !input.getPassword().isEmpty()){
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(input.getUsername(),input.getPassword())
            );

            SecurityContextHolder.getContext().setAuthentication(authentication);
            String jwt = jwtUtil.generateJwtToken(authentication);

            UserDetailImpl userDetails = (UserDetailImpl) authentication.getPrincipal();

            userDetails.setToken(jwt);

            res.status = 1;
            res.code = 200;
            res.message = "Success ";
            res.data= jwt;
        }
        return ResponseEntity.ok(res);
    }

    @PostMapping("/register")
    public ResponseEntity register(@RequestBody RegisterDto input){
        BaseResponse res = new BaseResponse();
        if(!input.getUsername().isEmpty() && !input.getPassword().isEmpty() && !input.getEmail().isEmpty()){
            if(userService.checkEmail(input.getEmail())){
                res.code = 503;
                res.message = "Email đã tồn tại trong hệ thống! Vui lòng thử lại";
            }
            if(userService.checkUsername(input.getUsername())){
                res.code = 504;
                res.message = "Tài khoản đã tồn tại trong hệ thống! Vui lòng thử lại";
            }


        }

        return ResponseEntity.ok(res);
    }
}
