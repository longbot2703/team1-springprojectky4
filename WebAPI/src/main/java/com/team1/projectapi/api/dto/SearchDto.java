package com.team1.projectapi.api.dto;

import lombok.Data;

@Data
public class SearchDto {
    private int id;
    private String search;
}
