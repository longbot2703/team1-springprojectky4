package com.team1.projectapi.api.sevice;

import com.team1.projectapi.api.entity.RoleEntity;
import com.team1.projectapi.api.repository.RoleRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleServiceImpl implements RoleService{
    @Autowired
    RoleRepo roleRepo;

    @Override
    public int getRoleID(String roleName) {
        if(!roleName.isEmpty()){
            RoleEntity role = roleRepo.getRoleByName(roleName);
            if(role != null){
                return  role.getId();
            }
        }
        return 4;
    }
}
