package com.team1.projectapi.api.sevice;

import com.team1.projectapi.api.config.Constants;
import com.team1.projectapi.api.dto.CategoryDto;
import com.team1.projectapi.api.entity.CategoryEntity;
import com.team1.projectapi.api.repository.CategoryRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService{
    @Autowired
    CategoryRepo categoryRepo;

    @Override
    public List<CategoryEntity> listCategory(String searchName) {
//        return categoryRepo.findAllByNameContainingAndIsactiveOrderByIdDesc(searchName, Constants.ACTIVE);
        return categoryRepo.findListCateByName(searchName);
    }

    @Override
    public CategoryEntity getCateDetail(int cateid) {
        return categoryRepo.getCateDetailByID(cateid);
    }

    @Override
    public CategoryDto convertEntityToDto(CategoryEntity cateEntity) {
        if(cateEntity == null){
            return null;
        }
        CategoryDto cateDto = new CategoryDto();
        cateDto.setId(cateEntity.getId());
        cateDto.setName(cateEntity.getName());
        cateDto.setDescription(cateEntity.getDescription());
        cateDto.setImage(cateDto.getImage());

        return cateDto;
    }
}
