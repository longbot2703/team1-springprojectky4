package com.team1.projectapi.api.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Table(name = "orders")
public class OrderEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "code")
    private String code;

    @Column(name = "note")
    private String note;

    @Column(name = "fullname")
    private String fullname;

    @Column(name = "address")
    private String address;

    @Column(name = "phone")
    private String phone;

    @Column(name = "email")
    private String email;

    @Column(name = "totalprice",nullable = false)
    private long totalprice;

    @Column(name = "total_price_payment",nullable = false)
    private long totalPricePayment;

    @Column(name = "discount",nullable = false)
    @ColumnDefault("0")
    private float discount;

    @Column(name = "ship_fee",nullable = false)
    @ColumnDefault("0")
    private int ship_fee;

    @Column(name = "status",nullable = false)
    @ColumnDefault("0")
    private int status;

    @Column(name = "paypal",nullable = false)
    @ColumnDefault("false")
    private boolean paypal; // check người dùng thanh toán paypal hay thanh toán trực tiếp : true : thanh toán  paypal

    @Column(name = "status_payment",nullable = false)
    @ColumnDefault("false")
    private boolean status_payment; // true đã thanh toán thành công , false chưa thanh toán

    @Column(name = "buyerid", nullable = false) // id user nguoi mua
    private int buyerid;

    @JsonIgnore
    @Column(name = "isactive",nullable = false)
    @ColumnDefault("1")
    private int isactive;

    @JsonIgnore
    @Column(name = "createat")
    private Timestamp createat;

    @JsonIgnore
    @Column(name = "updateat")
    private Timestamp updateat;


    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY) //EAGER
    @JoinColumn(name = "buyerid", insertable = false, updatable = false)
    private UserEntity buyer;

    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY,mappedBy = "orderDetail")
    private List<OrderDetailEntity> orderDetail;

    public OrderEntity() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getTotalprice() {
        return totalprice;
    }

    public void setTotalprice(long totalprice) {
        this.totalprice = totalprice;
    }

    public long getTotalPricePayment() {
        return totalPricePayment;
    }

    public void setTotalPricePayment(long totalPricePayment) {
        this.totalPricePayment = totalPricePayment;
    }


    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getBuyerid() {
        return buyerid;
    }

    public void setBuyerid(int buyerid) {
        this.buyerid = buyerid;
    }

    public int getIsactive() {
        return isactive;
    }

    public void setIsactive(int isactive) {
        this.isactive = isactive;
    }

    public Timestamp getCreateat() {
        return createat;
    }

    public void setCreateat(Timestamp createat) {
        this.createat = createat;
    }

    public Timestamp getUpdateat() {
        return updateat;
    }

    public void setUpdateat(Timestamp updateat) {
        this.updateat = updateat;
    }

    public UserEntity getBuyer() {
        return buyer;
    }

    public void setBuyer(UserEntity buyer) {
        this.buyer = buyer;
    }

    public List<OrderDetailEntity> getOrderDetail() {
        return orderDetail;
    }

    public void setOrderDetail(List<OrderDetailEntity> orderDetail) {
        this.orderDetail = orderDetail;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public float getDiscount() {
        return discount;
    }

    public void setDiscount(float discount) {
        this.discount = discount;
    }

    public int getShip_fee() {
        return ship_fee;
    }

    public void setShip_fee(int ship_fee) {
        this.ship_fee = ship_fee;
    }

    public boolean isPaypal() {
        return paypal;
    }

    public void setPaypal(boolean paypal) {
        this.paypal = paypal;
    }

    public boolean isStatus_payment() {
        return status_payment;
    }

    public void setStatus_payment(boolean status_payment) {
        this.status_payment = status_payment;
    }
}
