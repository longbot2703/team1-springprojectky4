package com.team1.projectapi.api.sevice;

import com.team1.projectapi.api.config.Constants;
import com.team1.projectapi.api.config.Util;
import com.team1.projectapi.api.entity.UserEntity;
import com.team1.projectapi.api.repository.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class UserServiceImpl implements UserService{
    @Autowired
    UserRepo userRepo;

    @Autowired
    RoleService roleService;

    Util util = new Util();

    @Override
    public UserEntity findUserByToken(String token) {
        return null;
    }

    @Override
    public boolean checkEmail(String email) {
        if (email != null){
            UserEntity user = userRepo.findByEmailAndIsactive(email, Constants.ACTIVE);
            if(user != null){
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean checkUsername(String username) {
        if(username != null){
            UserEntity user = userRepo.findByUsernameAndIsactive(username,Constants.ACTIVE);
            if(user != null){
                return true;
            }
        }
        return false;
    }

    @Override
    @Transactional
    public UserEntity createBuyer(String username, String email, String password) {
        if(!username.isEmpty() && !email.isEmpty() && !password.isEmpty()){
            UserEntity user = new UserEntity();
            user.setUsername(username);
            user.setEmail(email);
            user.setPassword(util.hashPass(password));
            user.setIsactive(Constants.ACTIVE);
            user.setCreateat(util.timestampNow());
            user.setRole(roleService.getRoleID(Constants.ROLE_BUYER));
        }
        return null;
    }
}
