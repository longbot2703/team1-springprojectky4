package com.team1.projectapi.api.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "freedback")
public class FeedbackEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "title")
    private String title;

    @Column(name = "description")
    private String description;

    @Column(name = "producid",nullable = false)
    private int productid;

    @Column(name = "status")
    @ColumnDefault("0") //  chua phan hoi
    private int status;

    @Column(name = "createby",nullable = false) // tạo bởi ai (user id )
    private int createby;

    @Column(name = "confirmby") // người phản hổi lại
    private int confirmby;

    @Column(name = "receiverby") // phản hồi ai (sản phẩm của ai)
    private int receiverby;

    @JsonIgnore
    @Column(name = "isactive",nullable = false)
    @ColumnDefault("1")
    private int isactive;

    @JsonIgnore
    @Column(name = "createat")
    private Timestamp createat;

    @JsonIgnore
    @Column(name = "updateat")
    private Timestamp updateat;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY) //EAGER
    @JoinColumn(name = "producid", insertable = false, updatable = false)
    private ProductEntity productFreedBack;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY) //EAGER
    @JoinColumn(name = "createby", insertable = false, updatable = false)
    private UserEntity createFreedBack;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY) //EAGER
    @JoinColumn(name = "confirmby", insertable = false, updatable = false)
    private UserEntity confirmFreedBack;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY) //EAGER
    @JoinColumn(name = "receiverby", insertable = false, updatable = false)
    private UserEntity receiverFreedBack;


    public FeedbackEntity() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getProductid() {
        return productid;
    }

    public void setProductid(int productid) {
        this.productid = productid;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getCreateby() {
        return createby;
    }

    public void setCreateby(int createby) {
        this.createby = createby;
    }

    public int getConfirmby() {
        return confirmby;
    }

    public void setConfirmby(int confirmby) {
        this.confirmby = confirmby;
    }

    public int getReceiverby() {
        return receiverby;
    }

    public void setReceiverby(int receiverby) {
        this.receiverby = receiverby;
    }

    public int getIsactive() {
        return isactive;
    }

    public void setIsactive(int isactive) {
        this.isactive = isactive;
    }

    public Timestamp getCreateat() {
        return createat;
    }

    public void setCreateat(Timestamp createat) {
        this.createat = createat;
    }

    public Timestamp getUpdateat() {
        return updateat;
    }

    public void setUpdateat(Timestamp updateat) {
        this.updateat = updateat;
    }

    public ProductEntity getProductFreedBack() {
        return productFreedBack;
    }

    public void setProductFreedBack(ProductEntity productFreedBack) {
        this.productFreedBack = productFreedBack;
    }

    public UserEntity getCreateFreedBack() {
        return createFreedBack;
    }

    public void setCreateFreedBack(UserEntity createFreedBack) {
        this.createFreedBack = createFreedBack;
    }

    public UserEntity getConfirmFreedBack() {
        return confirmFreedBack;
    }

    public void setConfirmFreedBack(UserEntity confirmFreedBack) {
        this.confirmFreedBack = confirmFreedBack;
    }

    public UserEntity getReceiverFreedBack() {
        return receiverFreedBack;
    }

    public void setReceiverFreedBack(UserEntity receiverFreedBack) {
        this.receiverFreedBack = receiverFreedBack;
    }
}
