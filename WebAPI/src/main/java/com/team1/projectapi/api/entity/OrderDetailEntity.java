package com.team1.projectapi.api.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "order_detail")
public class OrderDetailEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "orderid")
    private int orderid;

    @Column(name = "productid",nullable = false)
    private int productid;

    @Column(name = "sellerid",nullable = false)
    private int sellerid;

    @Column(name = "price",nullable = false)
    @ColumnDefault("0")
    private long price;

    @Column(name = "discount",nullable = false)
    @ColumnDefault("0")
    private int discount;

    @Column(name = "quantity",nullable = false)
    @ColumnDefault("1")
    private int quantity;

    @JsonIgnore
    @Column(name = "isactive",nullable = false)
    @ColumnDefault("1")
    private int isactive;

    @JsonIgnore
    @Column(name = "createat")
    private Timestamp createat;

    @JsonIgnore
    @Column(name = "updateat")
    private Timestamp updateat;

    @ManyToOne(fetch = FetchType.LAZY) //EAGER
    @JoinColumn(name = "sellerid", insertable = false, updatable = false)
    private UserEntity seller;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY) //EAGER
    @JoinColumn(name = "orderid", insertable = false, updatable = false)
    private OrderEntity orderDetail;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY) //EAGER
    @JoinColumn(name = "productid", insertable = false, updatable = false)
    private ProductEntity product_order;

    public OrderDetailEntity() {
    }

    public OrderDetailEntity( int productid, Timestamp createat) {
        this.productid = productid;
        this.createat = createat;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOrderid() {
        return orderid;
    }

    public void setOrderid(int orderid) {
        this.orderid = orderid;
    }

    public int getProductid() {
        return productid;
    }

    public void setProductid(int productid) {
        this.productid = productid;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getIsactive() {
        return isactive;
    }

    public void setIsactive(int isactive) {
        this.isactive = isactive;
    }

    public Timestamp getCreateat() {
        return createat;
    }

    public void setCreateat(Timestamp createat) {
        this.createat = createat;
    }

    public Timestamp getUpdateat() {
        return updateat;
    }

    public void setUpdateat(Timestamp updateat) {
        this.updateat = updateat;
    }

    public OrderEntity getOrderDetail() {
        return orderDetail;
    }

    public void setOrderDetail(OrderEntity orderDetail) {
        this.orderDetail = orderDetail;
    }

    public ProductEntity getProduct_order() {
        return product_order;
    }

    public void setProduct_order(ProductEntity product_order) {
        this.product_order = product_order;
    }

    public int getSellerid() {
        return sellerid;
    }

    public void setSellerid(int sellerid) {
        this.sellerid = sellerid;
    }

    public UserEntity getSeller() {
        return seller;
    }

    public void setSeller(UserEntity seller) {
        this.seller = seller;
    }
}
