package com.team1.projectapi.api.repository;

import com.team1.projectapi.api.entity.CategoryEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CategoryRepo extends JpaRepository<CategoryEntity ,Integer> {
    List<CategoryEntity> findAllByNameContainingAndIsactiveOrderByIdDesc(String name, int isactive);

    @Query("SELECT c from CategoryEntity c where c.isactive = 1 and c.name like concat('%',:name,'%') ORDER BY c.id DESC ")
    List<CategoryEntity> findListCateByName(@Param("name") String name);

    @Query("SELECT c from CategoryEntity c where c.isactive = 1 and c.id = :id")
    CategoryEntity getCateDetailByID(@Param("id") int id);
}
