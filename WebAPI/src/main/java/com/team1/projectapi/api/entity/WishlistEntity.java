package com.team1.projectapi.api.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "wishlist")
public class WishlistEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "userid")
    private int userid;

    @Column(name = "productid")
    private int producid;

    @JsonIgnore
    @Column(name = "isactive",nullable = false)
    @ColumnDefault("1")
    private int isactive;

    @JsonIgnore
    @Column(name = "createat")
    private Timestamp createat;

    @JsonIgnore
    @Column(name = "updateat")
    private Timestamp updateat;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY) //EAGER
    @JoinColumn(name = "userid", insertable = false, updatable = false)
    private UserEntity userWishlist;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY) //EAGER
    @JoinColumn(name = "productid", insertable = false, updatable = false)
    private ProductEntity productWishlist;


    public WishlistEntity() {
    }

    public WishlistEntity(int userid, int producid, int isactive, Timestamp createat) {
        this.userid = userid;
        this.producid = producid;
        this.isactive = isactive;
        this.createat = createat;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public int getProducid() {
        return producid;
    }

    public void setProducid(int producid) {
        this.producid = producid;
    }

    public int getIsactive() {
        return isactive;
    }

    public void setIsactive(int isactive) {
        this.isactive = isactive;
    }

    public Timestamp getCreateat() {
        return createat;
    }

    public void setCreateat(Timestamp createat) {
        this.createat = createat;
    }

    public Timestamp getUpdateat() {
        return updateat;
    }

    public void setUpdateat(Timestamp updateat) {
        this.updateat = updateat;
    }

    public UserEntity getUserWishlist() {
        return userWishlist;
    }

    public void setUserWishlist(UserEntity userWishlist) {
        this.userWishlist = userWishlist;
    }

    public ProductEntity getProductWishlist() {
        return productWishlist;
    }

    public void setProductWishlist(ProductEntity productWishlist) {
        this.productWishlist = productWishlist;
    }
}
