package com.team1.projectapi.api.config;

import org.springframework.security.crypto.bcrypt.BCrypt;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Consumer;

public class Util {


    private static Random generator = new Random();
    private static final String alpha = "abcdefghijklmnopqrstuvwxyz"; // a-z
    private static final String alphaUpperCase = alpha.toUpperCase(); // A-Z
    private static final String digits = "0123456789"; // 0-9
    private static final String specials = "~=+%^*/()[]{}/!@#$?|";
    private static final String ALPHA_NUMERIC = alpha + alphaUpperCase + digits;
    private static final String ALL = alpha + alphaUpperCase + digits + specials;
    /**
     * Create MD5 in datetime
     * @return
     */
    public  String createMD5() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
        String datestr =  dateFormat.format(Calendar.getInstance().getTime());

        byte[] bytesOfMessage = datestr.getBytes(StandardCharsets.UTF_8);

        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        byte[] thedigest = md.digest(bytesOfMessage);

        return thedigest.toString();
    }

    public String createToken(){
        return  UUID.randomUUID().toString()+System.currentTimeMillis();
    }

    /**
     * Convert Datetime to string
     * @param Datetime
     * @return
     */
    public String convertDatetime(Date Datetime){
        DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
        return dateFormat.format(Datetime);
    }

    /**
     * Convert String to time stamp
     * @param timeStr
     * @return
     */
    public Timestamp convertDatetime(String timeStr){
        try{
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date parsedDate = dateFormat.parse(timeStr);
            Timestamp timestamp = new Timestamp(parsedDate.getTime());
            return timestamp;
        }catch (Exception e){
            return null;
        }
    }

    /**
     * Create hash password to string with BCrypt
     * @param pass
     * @return
     */
    public String hashPass(String pass){
        return BCrypt.hashpw(pass, BCrypt.gensalt());
    }

    /**
     * Check String and hashPass return true or false
     * @param pass
     * @param hashPass
     * @return
     */
    public boolean checkPass(String pass ,String hashPass){
        return BCrypt.checkpw(pass,hashPass);
    }

    /**
     * Check email validate
     * @param email
     * @return
     */
    public boolean isValidEmail(String email) {
        String regex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        return email.matches(regex);
    }

    /**
     * Create random string from char and number
     * @return
     */
    public  String randomStr(){
        int leftLimit = 48; // numeral '0'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 10;
        Random random = new Random();

        String generatedString = random.ints(leftLimit, rightLimit + 1)
                .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97))
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
        return generatedString;
    }

    /**
     * Get timstampNow
     * @return
     */
    public Timestamp timestampNow(){
        Date utilDate = new Date();
        return new Timestamp(utilDate.getTime());
    }

    /**
     * Random string with a-zA-Z0-9, not included special characters
     * @param numberOfCharactor
     * @return
     */
    public String randomAlphaNumeric(int numberOfCharactor) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < numberOfCharactor; i++) {
            int number = randomNumber(0, ALPHA_NUMERIC.length() - 1);
            char ch = ALPHA_NUMERIC.charAt(number);
            sb.append(ch);
        }
        return sb.toString();
    }

    /**
     * Random string password with at least 1 digit and 1 special character
     * @param numberOfCharactor
     * @return
     */
    public String randomPassword(int numberOfCharactor) {
        List<String> result = new ArrayList<>();
        Consumer<String> appendChar = s -> {
            int number = randomNumber(0, s.length() - 1);
            result.add("" + s.charAt(number));
        };         appendChar.accept(digits);
        appendChar.accept(specials);
        while (result.size() < numberOfCharactor) {
            appendChar.accept(ALL);
        }
        Collections.shuffle(result, generator);
        return String.join("", result);
    }

    public static int randomNumber(int min, int max) {
        return generator.nextInt((max - min) + 1) + min;
    }

}


