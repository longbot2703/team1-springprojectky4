package com.team1.projectapi.api.security.service;

import com.team1.projectapi.api.config.Constants;
import com.team1.projectapi.api.entity.UserEntity;
import com.team1.projectapi.api.repository.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    UserRepo userRepo;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserEntity userEntity = userRepo.findByUsernameAndIsactive(username, Constants.ACTIVE);
        if (userEntity == null) {
            throw new UsernameNotFoundException("User Not Found with username: " + username);
        }
        return UserDetailImpl.build(userEntity);
    }
}
