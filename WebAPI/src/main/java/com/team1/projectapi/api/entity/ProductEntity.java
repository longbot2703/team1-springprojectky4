package com.team1.projectapi.api.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Table(name = "product")
public class ProductEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name",nullable = false)
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "image")
    private String image;

    @Column(name = "image1")
    private String image1;

    @Column(name = "image2")
    private String image2;

    @Column(name = "image3")
    private String image3;

    @Column(name = "image4")
    private String image4;

    @Column(name = "imagedetail")
    private String imagedetail;

    @Column(name = "unit") // đơn vị đo
    private String unit;

    @Column(name = "quantity", nullable = false)
    @ColumnDefault("1")
    private float quantity;

    @Column(name = "price", nullable = false)
    @ColumnDefault("1")
    private long price;

    @Column(name = "discount", nullable = false)
    @ColumnDefault("0")
    private int discount;

    @Column(name = "status",nullable = false)
    @ColumnDefault("1")// tình trạng còn or hết hàng
    private int status;

    @Column(name = "sellerby", nullable = false) // user id  người bán
    private int sellerby;

    @Column(name = "cateid",nullable = false)
    private int cateid;

    @Column(name = "detail")
    private String detail;

    @JsonIgnore
    @Column(name = "isactive",nullable = false)
    @ColumnDefault("1")
    private int isactive;

    @JsonIgnore
    @Column(name = "createat")
    private Timestamp createat;

    @JsonIgnore
    @Column(name = "updateat")
    private Timestamp updateat;

    @JsonIgnore
    @Column(name = "deleteat")
    private Timestamp deleteat;

    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "product_order")
    private List<OrderDetailEntity> oderDetail;

    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY,  mappedBy = "productWishlist")
    private List<WishlistEntity> productWishlist;

    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "productFreedBack")
    private List<FeedbackEntity> productFreedBack;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY) //EAGER
    @JoinColumn(name = "cateid", insertable = false, updatable = false)
    private CategoryEntity category;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sellerby", insertable = false, updatable = false)
    private BusinessEntity business;

    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "cartProduct")
    private List<CartEntity> cartProduct;

    @Transient
    public String getProductImage() {
        if (image == null) return null;
        return "/img/product/" + id + "/" + image;
    }

    @Transient
    public String getProductImage1() {
        if (image1 == null) return null;
        return "/img/product/" + id + "/" + image1;
    }

    @Transient
    public String getProductImage2() {
        if (image2 == null) return null;
        return "/img/product/" + id + "/" + image2;
    }

    @Transient
    public String getProductImage3() {
        if (image3 == null) return null;
        return "/img/product/" + id + "/" + image3;
    }

    @Transient
    public String getProductImage4() {
        if (image4 == null) return null;
        return "/img/product/" + id + "/" + image4;
    }

    @Transient
    public String getProductDetailImage() {
        if (imagedetail == null) return null;
        return "/img/product/" + id + "/" + imagedetail;
    }

    public ProductEntity() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImagedetail() {
        return imagedetail;
    }

    public void setImagedetail(String imagedetail) {
        this.imagedetail = imagedetail;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public float getQuantity() {
        return quantity;
    }

    public void setQuantity(float quantity) {
        this.quantity = quantity;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getSellerby() {
        return sellerby;
    }

    public void setSellerby(int sellerby) {
        this.sellerby = sellerby;
    }

    public int getCateid() {
        return cateid;
    }

    public void setCateid(int cateid) {
        this.cateid = cateid;
    }

    public int getIsactive() {
        return isactive;
    }

    public void setIsactive(int isactive) {
        this.isactive = isactive;
    }

    public Timestamp getCreateat() {
        return createat;
    }

    public void setCreateat(Timestamp createat) {
        this.createat = createat;
    }

    public Timestamp getUpdateat() {
        return updateat;
    }

    public void setUpdateat(Timestamp updateat) {
        this.updateat = updateat;
    }

    public Timestamp getDeleteat() {
        return deleteat;
    }

    public void setDeleteat(Timestamp deleteat) {
        this.deleteat = deleteat;
    }

    public List<OrderDetailEntity> getOderDetail() {
        return oderDetail;
    }

    public void setOderDetail(List<OrderDetailEntity> oderDetail) {
        this.oderDetail = oderDetail;
    }

    public List<WishlistEntity> getProductWishlist() {
        return productWishlist;
    }

    public void setProductWishlist(List<WishlistEntity> productWishlist) {
        this.productWishlist = productWishlist;
    }

    public List<FeedbackEntity> getProductFreedBack() {
        return productFreedBack;
    }

    public void setProductFreedBack(List<FeedbackEntity> productFreedBack) {
        this.productFreedBack = productFreedBack;
    }

    public CategoryEntity getCategory() {
        return category;
    }

    public void setCategory(CategoryEntity category) {
        this.category = category;
    }

    public BusinessEntity getBusiness() {
        return business;
    }

    public void setBusiness(BusinessEntity business) {
        this.business = business;
    }

    public List<CartEntity> getCartProduct() {
        return cartProduct;
    }

    public void setCartProduct(List<CartEntity> cartProduct) {
        this.cartProduct = cartProduct;
    }
}
