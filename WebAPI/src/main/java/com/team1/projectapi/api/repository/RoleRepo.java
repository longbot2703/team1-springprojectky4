package com.team1.projectapi.api.repository;

import com.team1.projectapi.api.entity.RoleEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface RoleRepo extends JpaRepository<RoleEntity,Integer> {
    @Query("SELECT r from RoleEntity r where r.name=:name")
    RoleEntity getRoleByName(@Param("name")String name);
}
