package com.team1.projectapi.api.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Table(name = "user")
public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "username",nullable = false)
    private String username;

    @JsonIgnore
    @Column(name = "password",nullable = false)
    private String password;

    @Column(name = "token")
    private String token;

    @Column(name = "fullname")
    private String fullname;

    @Column(name = "email")
    private String email;

    @Column(name = "deviceid")
    private String deviceid;

    @Column(name = "sex", nullable = false)
    @ColumnDefault("0")
    private int sex;

    @JsonIgnore
    @Column(name = "isactive",nullable = false)
    @ColumnDefault("1")
    private int isactive;

    @JsonIgnore
    @Column(name = "createat")
    private Timestamp createat;

    @JsonIgnore
    @Column(name = "updateat")
    private Timestamp updateat;

    @Column(name = "roleid")
    private int role;

    @Column(name = "address")
    private String address;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY) //EAGER
    @JoinColumn(name = "roleid", insertable = false, updatable = false)
    private RoleEntity roleUser;

    @JsonIgnore
    @OneToOne(fetch = FetchType.LAZY,mappedBy = "user")
    private BusinessEntity business;

    @JsonIgnore
    @OneToOne(fetch = FetchType.LAZY, mappedBy="userCustomer",cascade = CascadeType.ALL)
    private CustomerEntity customer;

    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "buyer")
    private List<OrderEntity> Order_Buyer;

    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "userWishlist")
    private List<WishlistEntity> userWishlist;

    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "createFreedBack")
    private List<FeedbackEntity> createFreedBack;

    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "confirmFreedBack")
    private List<FeedbackEntity> confirmFreedBack;

    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "receiverFreedBack")
    private List<FeedbackEntity> receiverFreedBack;

    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY,mappedBy = "cartUser")
    private List<CartEntity> carts;

    public UserEntity() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDeviceid() {
        return deviceid;
    }

    public void setDeviceid(String deviceid) {
        this.deviceid = deviceid;
    }

    public int getIsactive() {
        return isactive;
    }

    public void setIsactive(int isactive) {
        this.isactive = isactive;
    }

    public Timestamp getCreateat() {
        return createat;
    }

    public void setCreateat(Timestamp createat) {
        this.createat = createat;
    }

    public Timestamp getUpdateat() {
        return updateat;
    }

    public void setUpdateat(Timestamp updateat) {
        this.updateat = updateat;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public RoleEntity getRoleUser() {
        return roleUser;
    }

    public void setRoleUser(RoleEntity roleUser) {
        this.roleUser = roleUser;
    }

    public BusinessEntity getBusiness() {
        return business;
    }

    public void setBusiness(BusinessEntity business) {
        this.business = business;
    }

    public CustomerEntity getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerEntity customer) {
        this.customer = customer;
    }

    public List<OrderEntity> getOrder_Buyer() {
        return Order_Buyer;
    }

    public void setOrder_Buyer(List<OrderEntity> order_Buyer) {
        Order_Buyer = order_Buyer;
    }

    public List<WishlistEntity> getUserWishlist() {
        return userWishlist;
    }

    public void setUserWishlist(List<WishlistEntity> userWishlist) {
        this.userWishlist = userWishlist;
    }

    public List<FeedbackEntity> getCreateFreedBack() {
        return createFreedBack;
    }

    public void setCreateFreedBack(List<FeedbackEntity> createFreedBack) {
        this.createFreedBack = createFreedBack;
    }

    public List<FeedbackEntity> getConfirmFreedBack() {
        return confirmFreedBack;
    }

    public void setConfirmFreedBack(List<FeedbackEntity> confirmFreedBack) {
        this.confirmFreedBack = confirmFreedBack;
    }

    public List<FeedbackEntity> getReceiverFreedBack() {
        return receiverFreedBack;
    }

    public void setReceiverFreedBack(List<FeedbackEntity> receiverFreedBack) {
        this.receiverFreedBack = receiverFreedBack;
    }

    public List<CartEntity> getCarts() {
        return carts;
    }

    public void setCarts(List<CartEntity> carts) {
        this.carts = carts;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

}
