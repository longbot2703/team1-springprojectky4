package com.team1.project.controller.Admin;

import com.team1.project.config.Constants;
import com.team1.project.config.FileUploadUtil;
import com.team1.project.dto.LayoutConfig;
import com.team1.project.dto.NewsDto;
import com.team1.project.entity.CategoryEntity;
import com.team1.project.entity.NewEntity;
import com.team1.project.model.BaseResponse;
import com.team1.project.service.NewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@Controller
public class NewController {

    @Autowired
    NewService newService;

    public NewController(NewService newService) {
        this.newService = newService;
    }

    @GetMapping({"/admin/news"})
    public String findPaginatedNew(@RequestParam( required = false, defaultValue = "1",value = "page") int pageNo, Model model){
        int pageSize = Constants.PAGE_SIZE_WEB;
        Page<NewEntity> page = newService.findPaginated(pageNo,pageSize);
        List<NewEntity> listNewPage = page.getContent();

        LayoutConfig layoutConfig = new LayoutConfig();
        layoutConfig.ActiveNew =true;
        layoutConfig.title="Tin Tức";
        model.addAttribute("config", layoutConfig);

        model.addAttribute("currentPage",pageNo);
        model.addAttribute("totalPages",page.getTotalPages());
        model.addAttribute("totalItems",page.getTotalElements());
        model.addAttribute("listNews",listNewPage);

        return "admin/news/list_news";
    }

    @GetMapping({"/admin/news/create"})
    public String createNews(Model model) {
        LayoutConfig layoutConfig = new LayoutConfig();
        layoutConfig.ActiveNew =true;
        layoutConfig.title="Tạo tin tức, sự kiện ";
        model.addAttribute("config", layoutConfig);

        NewEntity news = new NewEntity();
        model.addAttribute("news", news);

        return "admin/news/create_news";
    }

    @PostMapping({"/admin/news/create"})
    public BaseResponse createNewsPost(@RequestBody NewEntity input)
    {
        BaseResponse res = new BaseResponse();
        try{
            newService.addNews(input);
            res.status = 1;
            res.message = "";
            return  res;
        }
        catch (Exception ex){
            res.message = "Có lỗi cảy ra với mã lỗi: " + ex.toString();
            return res;
        }
    }

    @GetMapping("/admin/news/edit/{id}")
    public String editNews (@PathVariable("id") Integer id, Model model) {
        LayoutConfig layoutConfig = new LayoutConfig();
        layoutConfig.ActiveNew =true;
        layoutConfig.title="New";
        model.addAttribute("config", layoutConfig);
        NewEntity news = newService.getNewsById(id);
        model.addAttribute("news", news);
        return "admin/news/edit_news";
    }

    @PostMapping("/admin/news/edit")
    public BaseResponse editNewsPost(@RequestBody NewEntity input)
    {
        BaseResponse res = new BaseResponse();
        try{
            newService.editNews(input);
            res.status = 1;
            res.message = "";
            return res;
        }
        catch (Exception ex){
            res.message = "Có lỗi cảy ra với mã lỗi: " + ex.toString();
            return res;
        }
    }

    @PostMapping("/admin/news/delete")
    public @ResponseBody ResponseEntity deleteNews(@RequestBody NewsDto input){
        BaseResponse res = new BaseResponse();

        if(input.getId() > 0){
            newService.deleteNews(input.getId());

            res.status = 1;
            res.code = 200;
            res.message = "Xóa tin thành công ";
        } else {
            res.status = 0;
            res.code = 200;
            res.message = "Tin không tồn tại trong hệ thống !";
        }
        return ResponseEntity.ok(res);
    }
    @PostMapping("/admin/uploadimgnews")
    public @ResponseBody
    BaseResponse UploadImg(@RequestParam(value = "img", required = false) MultipartFile file) throws IOException {
        BaseResponse res = new BaseResponse();
        try{
            String fileName = file.getOriginalFilename();
            String uploadDir = Constants.IMAGE_FOLDER_NEWS;
            FileUploadUtil.saveFile(uploadDir, fileName, file, null);
            res.message = "";
            res.status = 1;
            return res;
        }catch (Exception ex){
            res.message = "Có lỗi cảy ra với mã lỗi: " + ex.toString();
            return res;
        }
    }
}
