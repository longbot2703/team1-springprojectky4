package com.team1.project.controller.Public;

import com.team1.project.config.Constants;
import com.team1.project.config.Util;
import com.team1.project.dto.ContactDto;
import com.team1.project.dto.FontEndConfig;
import com.team1.project.dto.OrderDetailDto;
import com.team1.project.dto.OrderDto;
import com.team1.project.entity.OrderDetailEntity;
import com.team1.project.entity.OrderEntity;
import com.team1.project.entity.UserEntity;
import com.team1.project.model.BaseResponse;
import com.team1.project.service.CartService;
import com.team1.project.service.OrderDetailService;
import com.team1.project.service.OrderService;
import com.team1.project.service.UserService;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/customer/order")
public class PublicOrderController {
    @Autowired
    UserService userService;

    @Autowired
    OrderService orderService;

    @Autowired
    OrderDetailService orderDetailService;

    @Autowired
    CartService cartService;

    Util util = new Util();

    @GetMapping({"","/","/index"})
    public String getCustomerOrders(@RequestParam( required = false, defaultValue = "100",value = "status") int status,
            @RequestParam( required = false, defaultValue = "1",value = "page") int pageNo, Model model){
        if(status != Constants.ORDER_ALL && status != Constants.ORDER_REFUSE &&
                status != Constants.ORDER_CANCEL && status != Constants.ORDER_WAIT_CONFIRM && status != Constants.ORDER_CONFIRM &&
                status != Constants.ORDER_FINISH && status != Constants.ORDER_SHIP && status != Constants.ORDER_SUCCESS ){
            status = 100;
        }
        int userid = userService.findUseridBySession();
        if(userid > 0){
            int pageSize = Constants.PAGE_SIZE_WEB;
            int totalPage = 0;
            long totalElement = 0;
            List<OrderEntity> listOrders = new ArrayList<OrderEntity>();

            Pageable pageable = PageRequest.of(pageNo - 1, pageSize);
            Page<OrderEntity> page = orderService.listOrderByBuyeridAndStatus(userid, status, pageable);
            if(page != null){
                listOrders = page.getContent();
                totalPage = page.getTotalPages();
                totalElement = page.getTotalElements();
            }

            FontEndConfig config = new FontEndConfig();
            config.ActiveAccount = true;
            config.countCart = cartService.CountCart();
            model.addAttribute("config",config);
            model.addAttribute("status",status);

            model.addAttribute("currentPage",pageNo);
            model.addAttribute("totalPages",totalPage);
            model.addAttribute("totalItems",totalElement);

            model.addAttribute("listOrder",listOrders);

            return "public/orders";
        }
        return "redirect:/login";
    }

    @GetMapping({"/detail"})
    public String getOrderDetail(@RequestParam( required = false, defaultValue = "0",value = "code") String code, Model model){
        int userid = userService.findUseridBySession();
        if(userid > 0){
            OrderEntity order = new OrderEntity();
            if(!code.isEmpty()){
                order = orderService.findOrderByCode(code);
                if(order != null){
                    List<OrderDetailEntity> listOrderDetail = orderDetailService.listByOrderid(order.getId());

                    model.addAttribute("order",order);
                    model.addAttribute("listOrderDetail",listOrderDetail);
                }

                FontEndConfig config = new FontEndConfig();
                config.ActiveAccount = true;
                config.countCart = cartService.CountCart();
                model.addAttribute("config",config);
            }
            return "public/orderdetail";
        }
        return "redirect:/login";
    }

    /**
     * Nguoi mua huy don hang khi cho xac nhan
     * @param input
     * @return
     */
    @PostMapping({"/cancel"})
    public @ResponseBody ResponseEntity deleteOrder(@RequestBody OrderDto input){
        BaseResponse res = new BaseResponse();
        int userid = userService.findUseridBySession();
        if(userid > 0){
            OrderEntity order = orderService.findOrderById(input.getId());
            if(order != null && order.getBuyerid() == userid ){
                if(order.getStatus() == Constants.ORDER_WAIT_CONFIRM){
                    order.setStatus(Constants.ORDER_CANCEL);
                    order.setUpdateat(util.timestampNow());

                    if(orderService.updateOrder(order) != null){
                        List<OrderDetailEntity> listDetail = orderDetailService.listByOrderid(order.getId());
                        for (OrderDetailEntity ord : listDetail){
                            ord.setStatus(Constants.ORDER_DETAIL_CANCEL);
                            ord.setUpdateat(util.timestampNow());
                        }
                        orderDetailService.updateList(listDetail);
                        res.status = 1;
                        res.code = 200;
                        res.message = "Hủy đơn hàng thành thành công !";
                    }else {
                        res.message = "Hủy đơn hàng thất bại !";
                    }

                }
            }else {
                res.message = "Không tồn tại dữ liệu ";
            }
        }else {
            res.status = 404;
            res.message = "Đăng nhập để thực hiện chức năng này";
        }

        return ResponseEntity.ok(res);
    }

    @PostMapping({"/update"})
    public @ResponseBody ResponseEntity updateOrder(@RequestBody OrderDetailDto input){
        BaseResponse res = new BaseResponse();
        int userid = userService.findUseridBySession();
        if(userid > 0){
            OrderDetailEntity orderDetail = orderDetailService.findById(input.getId());
            OrderEntity order = orderService.findOrderById(orderDetail.getOrderid());
            if(orderDetail != null && order.getBuyerid() == userid ){
                orderDetail.setStatus(Constants.ORDER_DETAIL_FINISH);
                orderDetail.setUpdateat(util.timestampNow());
                orderDetailService.updateOrderDetail(orderDetail);

                List<OrderDetailEntity> listDetail = orderDetailService.listByOrderidAndStatusNot(order.getId(),Constants.ORDER_DETAIL_FINISH);
                if (listDetail.size() == 0){
                    order.setStatus(Constants.ORDER_SUCCESS);
                    order.setUpdateat(util.timestampNow());
                    orderService.updateOrder(order);
                }
                res.status = 1;
                res.code = 200;
                res.message = "Đơn hàng đã được cập nhật thành công !";
            }else {
                res.message = "Cập nhập đơn hàng thất bại !";
            }
        }else {
            res.status = 404;
            res.message = "Đăng nhập để thực hiện chức năng này";
        }

        return ResponseEntity.ok(res);
    }

    private boolean updateOrder(int orderid, int userid ,OrderDto input, int type){
        try{
            OrderEntity orderDetail = orderService.getDetail(orderid,userid);
            if(orderDetail != null){
                // xóa đơn hàng
                if(type == -1){
                    if(orderDetail.getStatus() == Constants.ORDER_WAIT_CONFIRM){
                        orderDetail.setStatus(Constants.ORDER_CANCEL);
                        orderDetail.setUpdateat(util.timestampNow());

                        if(orderService.updateOrder(orderDetail) != null){
                            OrderEntity checkDelete = orderService.updateOrder(orderDetail);
                            if(checkDelete != null){
                                List<OrderDetailEntity> listOrderDetail = orderDetailService.listByOrderid(orderid);
                                if(listOrderDetail != null){
                                    for(OrderDetailEntity orderDetailEntity : listOrderDetail){
                                        orderDetailEntity.setIsactive(Constants.NO_ACTIVE);
                                        orderDetailEntity.setUpdateat(util.timestampNow());
                                    }
                                    orderDetailService.updateList(listOrderDetail);
                                }
                            }
                            return true;
                        }
                    }
                    return false;
                }

                // cập nhập đơn hàng
                if(type == 1){
                    if(orderDetail.getStatus() == Constants.ORDER_WAIT_CONFIRM){
                        orderDetail.setFullname(input.getFullname());
                        orderDetail.setPhone(input.getPhone());
                        orderDetail.setAddress(input.getAddress());
                        orderDetail.setEmail(input.getEmail());
                        orderDetail.setNote(input.getNote());
                        orderDetail.setUpdateat(util.timestampNow());
                        if(orderService.updateOrder(orderDetail) != null){
                            return true;
                        }
                    }
                    return false;
                }
                if(type == 2){

                }
            }
            return false;
        }catch (Exception e){
            System.out.println(e.toString());
            return false;
        }

    }

}
