package com.team1.project.controller;

import com.team1.project.config.Constants;
import com.team1.project.dto.*;
import com.team1.project.entity.*;
import com.team1.project.repository.OrderDetailRepo;
import com.team1.project.repository.OrderRepo;
import com.team1.project.repository.UserRepo;
import com.team1.project.model.BaseResponse;
import com.team1.project.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Controller
public class WebController {

    @Autowired
    NewService newService;
    UserService userService;

    @Autowired
    CategoryService categoryService;
    UserRepo userRepo;
    OrderRepo orderRepo;
    OrderDetailRepo orderDetailRepo;

    @Autowired
    ProductService productService;

    @Autowired
    CartService cartService;

    @Autowired
    MapperDto mapperDto;

    public WebController(UserRepo userRepo, OrderRepo orderRepo,UserService userService, OrderDetailRepo orderDetailRepo) {
        this.userRepo = userRepo;
        this.orderRepo = orderRepo;
        this.userService = userService;
        this.orderDetailRepo = orderDetailRepo;
    }

    @GetMapping({"/","/index"})
    private String getIndex(Model model){

        FontEndConfig config = new FontEndConfig();
        config.ActiveHome = true;
        config.countCart = cartService.CountCart();
        model.addAttribute("config",config);

        List<NewEntity> listBanner = newService.listNews(Constants.BANNER_TYPE);

        List<CategoryDto> listCateDto = mapperDto.convertToAllCateDto(categoryService.getAllCategory());
        List<ProductDto> listProductDto = mapperDto.convertToAllProductDto(productService.findNewProduct(""));


        int slideCate = 0;
        if(listCateDto!= null ){
            int checkSlide = Integer.divideUnsigned(listCateDto.size(), 8);
            if(checkSlide >= 1){
                slideCate = checkSlide + 1;
            }
        }

        model.addAttribute("listBanner",listBanner);
        model.addAttribute("listCate",listCateDto);
        model.addAttribute("listProduct",listProductDto);
        model.addAttribute("countCate",listCateDto.size());
        model.addAttribute("slideCate",slideCate);

        return "public/index";
    }

    @GetMapping({"/admin/index"})
    private String getAdmin(Model model){
        LayoutConfig layoutConfig = new LayoutConfig();
        layoutConfig.ActiveDashboard = true;
        layoutConfig.title = "Dashboard";

        int userid = userService.findUseridBySession();
        UserEntity user = userService.findUserByID(userid);
        String role = user.getRoleUser().getName();

        List<UserEntity> listBuyer = userRepo.findAllByRoleAndIsactive(Constants.ROLE_BUYER_ID,Constants.ACTIVE);
        Integer countBuyer = listBuyer.size();

        List<UserEntity> listSeller = userRepo.findAllByRoleAndIsactive(Constants.ROLE_SELLER_ID,Constants.ACTIVE);
        Integer countSeller = listSeller.size();

        Map<String, Integer> dataPieChart = new LinkedHashMap<String, Integer>();
        if(user.getRole() == Constants.ROLE_SELLER_ID) {
            List<OrderDetailEntity> listOrder = orderDetailRepo.findAllByIsactiveAndStatusAndSellerid(Constants.ACTIVE,Constants.ORDER_FINISH,userid);
            Integer countOrder = listOrder.size();
            List<OrderDetailEntity> listOrderCancel = orderDetailRepo.findAllBySelleridAndIsactiveAndStatus(user.getId(),Constants.ACTIVE,Constants.ORDER_CANCEL);
            List<OrderDetailEntity> listOrderRefuse = orderDetailRepo.findAllBySelleridAndIsactiveAndStatus(user.getId(),Constants.ACTIVE,Constants.ORDER_REFUSE);
            Integer countOrderCancel = listOrderCancel.size();
            Integer countOrderRefuse = listOrderRefuse.size();
            Integer countOrderNotFinish = countOrderCancel + countOrderRefuse;
            dataPieChart.put("Đơn hàng thành công", countOrder);
            dataPieChart.put("Đơn hàng không thành công", countOrderNotFinish);
            model.addAttribute("countOrder", countOrder);
            model.addAttribute("countOrderCancel", countOrderCancel);
            if (orderDetailRepo.sumTotalPrice(userid) != null) {
                Integer totalPrice = orderDetailRepo.sumTotalPrice(userid);
                double trueTotalPrice = totalPrice * 0.3;
                model.addAttribute("totalPrice", totalPrice);
                model.addAttribute("trueTotalPrice", trueTotalPrice);
            }
        } else {
            List<OrderEntity> listOrder = orderRepo.findAllByStatusAndIsactive(Constants.ORDER_FINISH,Constants.ACTIVE);
            Integer countOrder = listOrder.size();
            List<OrderEntity> listOrderCancel = orderRepo.findAllByStatusAndIsactive(Constants.ORDER_CANCEL,Constants.ACTIVE);
            List<OrderEntity> listOrderRefuse = orderRepo.findAllByStatusAndIsactive(Constants.ORDER_REFUSE,Constants.ACTIVE);
            Integer countOrderCancel = listOrderCancel.size();
            Integer countOrderRefuse = listOrderRefuse.size();
            Integer countOrderNotFinish = countOrderCancel + countOrderRefuse;
            dataPieChart.put("Đơn hàng thành công", countOrder);
            dataPieChart.put("Đơn hàng không thành công", countOrderNotFinish);
            if (orderRepo.sumTotalPricePayment() != null) {
                Integer totalPrice = orderRepo.sumTotalPricePayment();
                double trueTotalPrice = totalPrice * 0.3;
                model.addAttribute("totalPrice", totalPrice);
                model.addAttribute("trueTotalPrice", trueTotalPrice);
                model.addAttribute("countOrder", countOrder);
            }
        }

        Map<String, Integer> dataLineChart = new LinkedHashMap<String, Integer>();
        for (int i=1; i<=12; i++) {
            if (user.getRoleUser().getName().equals("Seller")) {
                if(orderDetailRepo.sumSellerTotalPricePaymentByMonth(i, user.getId()) == null){
                    dataLineChart.put("Tháng " + i, 0);
                } else {
                    dataLineChart.put("Tháng " + i, orderDetailRepo.sumSellerTotalPricePaymentByMonth(i, user.getId()));
                }
            } else {
                if(orderRepo.sumTotalPricePaymentByMonth(i) == null){
                    dataLineChart.put("Tháng " + i, 0);
                } else {
                    dataLineChart.put("Tháng " + i, orderRepo.sumTotalPricePaymentByMonth(i));
                }
            }
        }

        model.addAttribute("config", layoutConfig);
        model.addAttribute("role", role);
        model.addAttribute("countBuyer", countBuyer);
        model.addAttribute("countSeller", countSeller);
        model.addAttribute("dataLineChart", dataLineChart);
        model.addAttribute("dataPieChart", dataPieChart);

        return "admin/index";
    }

    @GetMapping({"/error"})
    private String error(Model model){
        return "error";
    }

    @GetMapping("/test")
    private  @ResponseBody  ResponseEntity test(){
        BaseResponse res = new BaseResponse();
        double a = 65 * (1.0 * 15/100) * 1;
        res.data = a;

        return ResponseEntity.ok(res);
    }
}
