package com.team1.project.controller.Public;

import com.paypal.api.payments.Links;
import com.paypal.api.payments.Payment;
import com.paypal.base.rest.PayPalRESTException;
import com.team1.project.config.Constants;
import com.team1.project.config.PaypalPaymentIntent;
import com.team1.project.config.PaypalPaymentMethod;
import com.team1.project.config.Util;
import com.team1.project.dto.CustomerDTO;
import com.team1.project.dto.FontEndConfig;
import com.team1.project.dto.OrderDto;
import com.team1.project.entity.*;
import com.team1.project.model.BaseResponse;
import com.team1.project.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

@Controller
@RequestMapping("/customer/checkout")
public class CheckoutController {

    public static final String URL_PAYPAL_SUCCESS = "customer/checkout/pay/success";
    public static final String URL_PAYPAL_CANCEL = "customer/cart";
    private Logger log = (Logger) LoggerFactory.getLogger(getClass());

    @Autowired
    UserService userService;

    @Autowired
    OrderService orderService;

    @Autowired
    OrderDetailService orderDetailService;

    @Autowired
    CartService cartService;

    @Autowired
    private PaypalService paypalService;

    @Autowired
    ProductService productService;

    @Autowired
    CustomerService customerService;

    Util util = new Util();


    @GetMapping({"","/","/index"})
    public String getOrder(Model model, HttpSession session){
        int userid = userService.findUseridBySession();
        if(userid == 0){
            return "redirect:/login";
        }
        FontEndConfig config = new FontEndConfig();
        config.ActiveCart = true;
        config.countCart = cartService.CountCart();
        model.addAttribute("config",config);

        OrderDto orderDto = (OrderDto) session.getAttribute(Constants.ORDER_CREATE_SESSION);
        if(orderDto == null){
            orderDto = new OrderDto();
        }
        UserEntity user = userService.findUserByID(userid);

        model.addAttribute("orderDto",orderDto);
        model.addAttribute("user",user);

        return "public/checkout";
    }

    @PostMapping({"/create"})
    public @ResponseBody ResponseEntity createOrder(@RequestBody OrderDto input, HttpServletRequest request){
        BaseResponse res = new BaseResponse();

        int userid = userService.findUseridBySession();

        if(userid == 0){
                res.status = 0;
                res.code = 404;
                res.message = "Bạn cần đăng nhập để thực hiện chức năng này ";
        }else {
            input.setBuyerid(userid);
            UserEntity user = userService.findUserByID(userid);
            user.setFullname(input.getFullname());
            user.setAddress(input.getAddress());
            user.setUpdateat(util.timestampNow());
            userService.updateBuyer(user);

            CustomerEntity cus = user.getCustomer();
            cus.setPhone(input.getPhone());
            cus.setUpdateat(util.timestampNow());
            customerService.update(cus);
            List<CartEntity> carts = cartService.getListByUserid(userid);
            if(carts != null) {
                input.setListCart(carts);
                if (input.getCheck_paypal() == 2) {
                    // lưu tạm database đơn hàng nhưng chưa active
                    OrderEntity order = orderService.createOrder(input,Constants.NO_ACTIVE);
                    if(order != null){
                        // create order detail
                        if(orderDetailService.createOrderDetail(input,order.getId(),Constants.NO_ACTIVE)){
                            res.status = 1;
                            res.code = 507;
                            res.message = "Tạo đơn hàng thành công !";
                        }
                        // đoạn code này để tạo 1 thanh toán gửi tới paypal
                        String cancelUrl = Util.getBaseURL(request) + "/" + URL_PAYPAL_CANCEL; // cancel rederect to checkout
                        String successUrl = Util.getBaseURL(request) + "/" + URL_PAYPAL_SUCCESS +"?code=" + order.getCode();
                        try {
                            double total = (1.0 * (input.getTotalPrice() + input.getShip_fee() - input.getDiscount())/25000);
                            if( total <= 1 ) total = 1;
                            Payment payment = paypalService.createPayment(
                                    total,
                                    "USD",
                                    PaypalPaymentMethod.paypal,
                                    PaypalPaymentIntent.sale,
                                    "thanh toan don hang " + order.getCode(),
                                    cancelUrl,
                                    successUrl);
                            for (Links links : payment.getLinks()) {
                                if (links.getRel().equals("approval_url")) {
                                    res.status = 1;
                                    res.code = 555;
                                    res.message = "Chuyển qua đăng nhập paypal";
                                    res.data = links.getHref();
                                }
                            }
                        } catch (PayPalRESTException e) {
                            log.error(e.getMessage());
                            res.status = 0;
                            res.code = 506;
                            res.message = "Tạo đơn hàng thất bại !";
                        }
                    }else {
                        res.status = 0;
                        res.code = 506;
                        res.message = "Tạo đơn hàng thất bại !";
                    }

                }else {
                    OrderEntity order = orderService.createOrder(input,Constants.ACTIVE);
                    if(order != null){
                        // create order detail
                        if(orderDetailService.createOrderDetail(input,order.getId(),Constants.ACTIVE)){
                            res.status = 1;
                            res.code = 507;
                            res.message = "Tạo đơn hàng thành công !";
                        }

                    }else {
                        res.status = 0;
                        res.code = 506;
                        res.message = "Tạo đơn hàng thất bại !";
                    }
                }
            }else {
                res.status = 0;
                res.code = 506;
                res.message = "Tạo đơn hàng thất bại !";
            }
        }
        return ResponseEntity.ok(res);
    }


    // url sau khi thanh toán thành công
    @GetMapping("/pay/success")
    public String successPay(@RequestParam("code") String orderCode,
                             @RequestParam("paymentId") String paymentId,
                             @RequestParam("PayerID") String payerId){
        try {
            Payment payment = paypalService.executePayment(paymentId, payerId);
            if(payment.getState().equals("approved")){
                int userid = userService.findUseridBySession();
                if(userid > 0 && !orderCode.isEmpty()){
                    OrderEntity orderUpdate = orderService.finAllbyOderCoder(orderCode);
                    if(orderUpdate != null && orderUpdate.getBuyerid() == userid){
                        // cập nhập lại thông tin lai đơn  hàng sau khhi thanh toán thành công
                        orderUpdate.setPaypal(true);
                        orderUpdate.setStatus_payment(true);
                        orderUpdate.setUpdateat(util.timestampNow());
                        orderUpdate.setIsactive(Constants.ACTIVE);

                        orderService.updateOrder(orderUpdate);

                        List<OrderDetailEntity> listOrderDetail = orderDetailService.listAllByOrderid(orderUpdate.getId());
                        List<ProductEntity> lisPro = new ArrayList<>();
                        if(listOrderDetail != null){
                            for (OrderDetailEntity orderDetail: listOrderDetail){
                                orderDetail.setIsactive(Constants.ACTIVE);
                                ProductEntity pro = productService.findByID(orderDetail.getProductid());
                                if(pro != null){
                                    pro.setUpdateat(util.timestampNow());
                                    float quantity = pro.getQuantity() - orderDetail.getQuantity();
                                    if(quantity < 0) quantity = 0;
                                    pro.setQuantity(quantity);
                                    lisPro.add(pro);
                                }
                            }

                            if(lisPro != null){
                                productService.saveAll(lisPro);
                            }
                            orderDetailService.updateList(listOrderDetail);
                        }

                        List<CartEntity> listCart = cartService.getListByUserid(userid);
                        if(listCart != null){
                            for (CartEntity cart : listCart){
                                cart.setIsactive(Constants.NO_ACTIVE);
                                cart.setUpdateat(util.timestampNow());
                            }
                            cartService.saveAll(listCart);
                        }
                    }
                }

                return "redirect:/customer/order";
            }
        } catch (PayPalRESTException e) {
            log.error(e.getMessage());
        }
        return "redirect:/";
    }

    @GetMapping("/shipping")
    public String changeShippingAddress(Model model){
        int userid = userService.findUseridBySession();
        if(userid == 0){
            return "redirect:/login";
        }
        FontEndConfig config = new FontEndConfig();
        config.ActiveShippingAdress = true;
        model.addAttribute("config",config);
        UserEntity userEdit = userService.findUserByID(userid);
        String userPhone = userEdit.getCustomer().getPhone();
        String userAddress = userEdit.getAddress();
        String city="";
        String district="";
        String addressDetail="";
        if (userAddress!=null && userAddress.length()>0){
            String[] lsaddress=userAddress.split(",");
            if(lsaddress.length>=3){
                city=lsaddress[lsaddress.length-1].trim();
                district=lsaddress[lsaddress.length-2].trim();
                addressDetail="";
                for (int i=0;i<lsaddress.length-2;i++){
                    if (i==0){
                        addressDetail+=lsaddress[i];
                    }
                    else {
                        addressDetail+=","+lsaddress[i];
                    }
                }
            }

            model.addAttribute("city",city);
            model.addAttribute("district",district);
            model.addAttribute("addressDetail",addressDetail.trim());
            model.addAttribute("phone",userPhone);
        }
        return "public/changeshippingaddress";
    }
    @PostMapping({"/updateaddress"})
    public @ResponseBody ResponseEntity Updateaddress(@RequestBody CustomerDTO data){
        BaseResponse res = new BaseResponse();
        int userid = userService.findUseridBySession();
        if(userid > 0){
            boolean check=userService.updateAddressUser(userid,data.getAddress(), data.getPhone());
            if(check){
                res.status = 1;
                res.code = 200;
                res.message = "Cập nhập thông tin thành công !";
            }
            else {
                res.message="cập nhật thất bại";
            }
        }else {
            res.code= 404;
        }
        return ResponseEntity.ok(res);
    }
}
