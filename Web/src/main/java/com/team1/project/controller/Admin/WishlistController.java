package com.team1.project.controller.Admin;

import com.team1.project.config.Constants;
import com.team1.project.dto.LayoutConfig;
import com.team1.project.entity.WishlistEntity;
import com.team1.project.service.WishlistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class WishlistController {

    @Autowired
    WishlistService wishlistService;

    @GetMapping({"/admin/wishlist"})
    public String index(@RequestParam( required = false, defaultValue = "1",value = "page") int pageNo, Model model){
        int pageSize = Constants.PAGE_SIZE_WEB;
        Page<WishlistEntity> page = wishlistService.findPaginated(pageNo,pageSize);
        List<WishlistEntity> allWishlist = page.getContent();

        LayoutConfig layoutConfig = new LayoutConfig();
        layoutConfig.ActiveWishlist =true;
        layoutConfig.title="Wishlist";
        model.addAttribute("config", layoutConfig);

        model.addAttribute("currentPage",pageNo);
        model.addAttribute("totalPages",page.getTotalPages());
        model.addAttribute("totalItems",page.getTotalElements());
        model.addAttribute("allWishlist",allWishlist);

        return "admin/wish_list/index";
    }
}
