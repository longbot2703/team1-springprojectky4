package com.team1.project.controller.Admin;

import com.team1.project.config.Constants;
import com.team1.project.config.FileUploadUtil;
import com.team1.project.dto.LayoutConfig;
import com.team1.project.dto.ProductDto;
import com.team1.project.entity.CategoryEntity;
import com.team1.project.entity.ProductEntity;
import com.team1.project.entity.UserEntity;
import com.team1.project.model.BaseResponse;
import com.team1.project.service.CategoryService;
import com.team1.project.service.ProductService;
import com.team1.project.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.IOException;

import javax.validation.Valid;
import java.util.List;

@Controller
public class ProductController {

    @Autowired
    ProductService productService;
    CategoryService categoryService;
    UserService userService;

    public ProductController(ProductService productService, CategoryService categoryService, UserService userService) {
        this.productService = productService;
        this.categoryService = categoryService;
        this.userService = userService;
    }

    @GetMapping({"/admin/product"})
    public String findPaginatedNew(@RequestParam( required = false, defaultValue = "1",value = "page") int pageNo, Model model){
        int pageSize = Constants.PAGE_SIZE_WEB;
        int userid = userService.findUseridBySession();
        UserEntity user = userService.findUserByID(userid);
        Page<ProductEntity> page;
        if(user.getRole() == Constants.ROLE_SELLER_ID) {
            page = productService.findPaginated(pageNo,pageSize,user.getBusiness().getId());
        } else {
            page = productService.findPaginated(pageNo,pageSize,0);
        }
        List<ProductEntity> listProduct = page.getContent();

        LayoutConfig layoutConfig = new LayoutConfig();
        layoutConfig.ActiveProduct =true;
        layoutConfig.title="Danh Sách Sản Phẩm";
        model.addAttribute("config", layoutConfig);

        model.addAttribute("currentPage",pageNo);
        model.addAttribute("totalPages",page.getTotalPages());
        model.addAttribute("totalItems",page.getTotalElements());
        model.addAttribute("listProduct",listProduct);

        return "admin/product/list_product";
    }

    @GetMapping({"/admin/product/create"})
    public String createProduct(Model model) {
        List<CategoryEntity> listCategory = categoryService.fillAll();
        List<UserEntity> listSeller = userService.findAllSeller();
        LayoutConfig layoutConfig = new LayoutConfig();
        layoutConfig.ActiveProduct =true;
        layoutConfig.title="Tạo mới sản phẩm";
        int userid = userService.findUseridBySession();
        UserEntity user = userService.findUserByID(userid);
        model.addAttribute("config", layoutConfig);
        ProductEntity product = new ProductEntity();
        model.addAttribute("product", product);
        model.addAttribute("role", user.getRole());
        model.addAttribute("listCategory",listCategory);
        model.addAttribute("listSeller", listSeller);


        return "admin/product/create_product";
    }

    @PostMapping({"/admin/product/create"})
    public String createProductPost(@Valid @ModelAttribute("product") ProductEntity product,
                          RedirectAttributes ra,
                          BindingResult bindingResult,
                          @RequestParam("inputProductImage1") MultipartFile multipartFile1,
                          @RequestParam("inputProductImage2") MultipartFile multipartFile2,
                          @RequestParam("inputProductImage3") MultipartFile multipartFile3,
                          @RequestParam("inputProductImage4") MultipartFile multipartFile4,
                          @RequestParam("inputProductDetailImage") MultipartFile detailImage) throws IOException {
        if (bindingResult.hasErrors()) {
            return "admin/product/create_product";
        }
        String productImage1 = StringUtils.cleanPath(multipartFile1.getOriginalFilename());
        String productImage2 = StringUtils.cleanPath(multipartFile2.getOriginalFilename());
        String productImage3 = StringUtils.cleanPath(multipartFile3.getOriginalFilename());
        String productImage4 = StringUtils.cleanPath(multipartFile4.getOriginalFilename());
        String productDetailImage = StringUtils.cleanPath(detailImage.getOriginalFilename());
        product.setImage1(productImage1);
        product.setImage2(productImage2);
        product.setImage3(productImage3);
        product.setImage4(productImage4);
        product.setImagedetail(productDetailImage);
        int userid = userService.findUseridBySession();
        UserEntity user = userService.findUserByID(userid);
        ProductEntity newProduct;
        if (user.getRole() == Constants.ROLE_ADMIN_ID){
            newProduct = productService.addProduct(product,0);
        } else {
            newProduct = productService.addProduct(product,user.getBusiness().getId());
        }
        // save in local host
        String uploadDir = Constants.IMAGE_FOLDER_PRODUCT + newProduct.getId();
        // save in vps
//        String uploadDir = Constants.IMAGE_FOLDER_PRODUCT_SERVER + newProduct.getId();
        if (!detailImage.isEmpty()) FileUploadUtil.saveFile(uploadDir, productDetailImage, detailImage, null);
        if (!productImage1.isEmpty()) FileUploadUtil.saveFile(uploadDir, productImage1, multipartFile1, null);
        if (!productImage2.isEmpty()) FileUploadUtil.saveFile(uploadDir, productImage2, multipartFile2, null);
        if (!productImage3.isEmpty()) FileUploadUtil.saveFile(uploadDir, productImage3, multipartFile3, null);
        if (!productImage4.isEmpty()) FileUploadUtil.saveFile(uploadDir, productImage4, multipartFile4, null);


        return "redirect:/admin/product";
    }

    @GetMapping("/admin/product/edit/{id}")
    public String editProduct (@PathVariable("id") Integer id, Model model) {
        List<CategoryEntity> listCategory = categoryService.fillAll();
        List<UserEntity> listSeller = userService.findAllSeller();
        LayoutConfig layoutConfig = new LayoutConfig();
        layoutConfig.ActiveProduct =true;
        layoutConfig.title="Sửa sản phẩm";
        model.addAttribute("config", layoutConfig);
        ProductEntity product = productService.findByID(id);
        model.addAttribute("product", product);
        int userid = userService.findUseridBySession();
        UserEntity user = userService.findUserByID(userid);
        model.addAttribute("role", user.getRole());
        model.addAttribute("listCategory",listCategory);
        model.addAttribute("listSeller", listSeller);
        return "admin/product/edit_product";
    }

    @PostMapping("/admin/product/edit/{id}")
    public String editProductPost(@PathVariable("id") Integer id,
                               @Valid @ModelAttribute("product") ProductEntity product,
                               BindingResult bindingResult,
                              @RequestParam("inputProductImage1") MultipartFile multipartFile1,
                              @RequestParam("inputProductImage2") MultipartFile multipartFile2,
                              @RequestParam("inputProductImage3") MultipartFile multipartFile3,
                              @RequestParam("inputProductImage4") MultipartFile multipartFile4,
                              @RequestParam("inputProductDetailImage") MultipartFile detailImage) throws IOException {
        if (bindingResult.hasErrors()) {
            product.setId(id);
            return "admin/product/edit_product";
        }
        String oldProductName1 = productService.findByID(id).getImage1();
        String oldProductName2 = productService.findByID(id).getImage2();
        String oldProductName3 = productService.findByID(id).getImage3();
        String oldProductName4 = productService.findByID(id).getImage4();
        String oldProductDetailName = productService.findByID(id).getImagedetail();
        String productImage1 = StringUtils.cleanPath(multipartFile1.getOriginalFilename());
        String productImage2 = StringUtils.cleanPath(multipartFile2.getOriginalFilename());
        String productImage3 = StringUtils.cleanPath(multipartFile3.getOriginalFilename());
        String productImage4 = StringUtils.cleanPath(multipartFile4.getOriginalFilename());
        String productDetailImage = StringUtils.cleanPath(detailImage.getOriginalFilename());
        product.setImage1(productImage1);
        product.setImage2(productImage2);
        product.setImage3(productImage3);
        product.setImage4(productImage4);
        product.setImagedetail(productDetailImage);
        ProductEntity editProduct = productService.editProduct(product);
        // update in local
        String uploadDir = Constants.IMAGE_FOLDER + "/product" + editProduct.getId();
        // update in vps
//        String uploadDir = Constants.IMAGE_FOLDER_PRODUCT_SERVER + editProduct.getId();

        if (!detailImage.isEmpty()) FileUploadUtil.saveFile(uploadDir, productDetailImage, detailImage, oldProductDetailName);
        if (!productImage1.isEmpty()) FileUploadUtil.saveFile(uploadDir, productImage1, multipartFile1, oldProductName1);
        if (!productImage2.isEmpty()) FileUploadUtil.saveFile(uploadDir, productImage2, multipartFile2, oldProductName2);
        if (!productImage3.isEmpty()) FileUploadUtil.saveFile(uploadDir, productImage3, multipartFile3, oldProductName3);
        if (!productImage4.isEmpty()) FileUploadUtil.saveFile(uploadDir, productImage4, multipartFile4, oldProductName4);

        return "redirect:/admin/product";
    }

    @PostMapping("/admin/product/delete")
    private @ResponseBody ResponseEntity deleteProduct(@RequestBody ProductDto input){
        BaseResponse res = new BaseResponse();

        if(input.getId() > 0){
            productService.deleteProduct(input.getId());

            res.status = 1;
            res.code = 200;
            res.message = "Xóa sản phẩm thành công ";
        } else {
            res.status = 0;
            res.code = 200;
            res.message = "Sản phẩm không tồn tại trong hệ thống !";
        }
        return ResponseEntity.ok(res);
    }
}
