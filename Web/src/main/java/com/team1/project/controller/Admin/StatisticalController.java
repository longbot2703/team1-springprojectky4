package com.team1.project.controller.Admin;

import com.team1.project.config.Util;
import com.team1.project.dto.LayoutConfig;
import com.team1.project.dto.StatisticalDto;
import com.team1.project.service.CategoryServiceImpl;
import com.team1.project.service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;


@Controller
public class StatisticalController {
    @Autowired
    UserServiceImpl userService;
    Util util = new Util();

    @GetMapping({"/admin/statistical"})
    public String category(Model model) {
        LayoutConfig layoutConfig = new LayoutConfig();
        List<StatisticalDto> data =  userService.statisticalSeller();
        layoutConfig.ActiveStaticstical =true;
        layoutConfig.title="Thống kê doanh thu";
        model.addAttribute("config", layoutConfig);
        model.addAttribute("data", data);
        return "admin/statistical";
    }
}
