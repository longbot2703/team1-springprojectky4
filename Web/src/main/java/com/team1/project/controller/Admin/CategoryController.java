package com.team1.project.controller.Admin;

import com.team1.project.config.Constants;
import com.team1.project.config.FileUploadUtil;
import com.team1.project.config.Util;
import com.team1.project.dto.LayoutConfig;
import com.team1.project.entity.CategoryEntity;
import com.team1.project.model.BaseResponse;
import com.team1.project.service.CategoryServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;


@Controller
public class CategoryController {
    @Autowired
    CategoryServiceImpl categoryServiceImpl;
    Util util = new Util();

    @GetMapping({"/admin/category"})
    public String category(Model model) {
        LayoutConfig layoutConfig = new LayoutConfig();
        List<CategoryEntity> data = categoryServiceImpl.GetAll();
        layoutConfig.ActiveCategory = true;
        layoutConfig.title = "Danh mục";
        model.addAttribute("config", layoutConfig);
        model.addAttribute("data", data);
        return "admin/category";
    }

    @GetMapping({"/admin/searchCate"})
    public String getCategoryByName(@RequestParam String name, Model model){
        List<CategoryEntity> data = categoryServiceImpl.findAllByNameAndIsactive(name, Constants.ACTIVE);
        model.addAttribute("data", data);
        return "admin/_tableCategory";
    }

    @PostMapping("/admin/category/createCate")
    public @ResponseBody
    BaseResponse CreateCategory(@RequestBody CategoryEntity input) {
        input.setCreateat(util.timestampNow());
        input.setIsactive(1);
        return categoryServiceImpl.CreateCategory(input);
    }

    @PostMapping("/admin/category/update")
    public @ResponseBody
    BaseResponse UpdateCategory(@RequestBody CategoryEntity input) {
        return categoryServiceImpl.UpdateCategory(input);
    }


    @PostMapping("/admin/category/delete")
    public @ResponseBody
    BaseResponse DelCategory(@RequestBody CategoryEntity input) {
        return categoryServiceImpl.Delete(input);
    }

    @PostMapping("/admin/uploadimg")
    public @ResponseBody
    BaseResponse UploadImg(@RequestParam(value = "img", required = false) MultipartFile file) throws IOException {
        BaseResponse res = new BaseResponse();
        try{
            String fileName = file.getOriginalFilename();
            String uploadDir = Constants.IMAGE_FOLDER_CATEGORY;
            FileUploadUtil.saveFile(uploadDir, fileName, file, null);
            res.message = "";
            res.status = 1;
            return res;
        }catch (Exception ex){
            res.message = "Có lỗi cảy ra với mã lỗi: " + ex.toString();
            return res;
        }
    }
}
