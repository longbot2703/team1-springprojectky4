package com.team1.project.controller.Admin;

import com.team1.project.config.Constants;
import com.team1.project.config.Util;
import com.team1.project.dto.LayoutConfig;
import com.team1.project.dto.OrderDetailDto;
import com.team1.project.dto.OrderDto;
import com.team1.project.dto.ProductDto;
import com.team1.project.entity.NewEntity;
import com.team1.project.entity.OrderDetailEntity;
import com.team1.project.entity.OrderEntity;
import com.team1.project.entity.UserEntity;
import com.team1.project.model.BaseResponse;
import com.team1.project.service.OrderDetailService;
import com.team1.project.service.OrderService;
import com.team1.project.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/admin/business/order")
public class OrderController {
    @Autowired
    UserService userService;

    @Autowired
    OrderService orderService;

    @Autowired
    OrderDetailService orderDetailService;

    Util util = new Util();

    @GetMapping({"","/index"})
    public String findPaginatedListOrder(@RequestParam( required = false, defaultValue = "100",value = "status") int status,
            @RequestParam( required = false, defaultValue = "1",value = "page") int pageNo, Model model){
        if(status != Constants.ORDER_ALL && status != Constants.ORDER_REFUSE &&
            status != Constants.ORDER_CANCEL && status != Constants.ORDER_WAIT_CONFIRM && status != Constants.ORDER_CONFIRM &&
            status != Constants.ORDER_FINISH && status != Constants.ORDER_SHIP && status != Constants.ORDER_SUCCESS ){
            status = 100;
        }
        int userid = userService.findUseridBySession();
        UserEntity user = userService.findUserByID(userid);
        if(userid > 0){
            if (user.getRole() == Constants.ROLE_SELLER_ID) {
                int pageSize = Constants.PAGE_SIZE_WEB;

                List<OrderEntity> listOrder = new ArrayList<>();
                int totalPage = 0;
                long totalElement = 0;

                Pageable pageable = PageRequest.of(pageNo-1,pageSize);

                List<Integer> list = orderDetailService.listOrderidBysellerid(userid);
                if(list != null){
                    Page<OrderEntity> page = orderService.listPageByArrID(list,status,pageable);

                    if(page != null){
                        listOrder = page.getContent();
                        totalPage = page.getTotalPages();
                        totalElement = page.getTotalElements();
                    }

                }


                LayoutConfig layoutConfig = new LayoutConfig();
                layoutConfig.ActiveOrder = true;
                layoutConfig.title = "Danh sách đơn hàng";
                model.addAttribute("config", layoutConfig);
                model.addAttribute("status", status);

                model.addAttribute("listOrder",listOrder);

                model.addAttribute("currentPage",pageNo);
                model.addAttribute("totalPages",totalPage);
                model.addAttribute("totalItems",totalElement);

                return "admin/order/index";
            } else {
                int pageSize = Constants.PAGE_SIZE_WEB;

                List<OrderEntity> listOrder = new ArrayList<>();
                int totalPage = 0;
                long totalElement = 0;

                Pageable pageable = PageRequest.of(pageNo-1,pageSize);

                List<Integer> list = orderDetailService.listAll();
                if(list != null){
                    Page<OrderEntity> page = orderService.listPageByArrID(list,status,pageable);

                    if(page != null){
                        listOrder = page.getContent();
                        totalPage = page.getTotalPages();
                        totalElement = page.getTotalElements();
                    }

                }


                LayoutConfig layoutConfig = new LayoutConfig();
                layoutConfig.ActiveOrder = true;
                layoutConfig.title = "Danh sách đơn hàng";
                model.addAttribute("config", layoutConfig);
                model.addAttribute("status", status);

                model.addAttribute("listOrder",listOrder);

                model.addAttribute("currentPage",pageNo);
                model.addAttribute("totalPages",totalPage);
                model.addAttribute("totalItems",totalElement);

                return "admin/order/index";
            }
        }else {
            return "redirect:/admin/login";
        }
    }


    @GetMapping({"/detail"})
    public String viewDetail(@RequestParam(required = false, defaultValue = "0",value = "code") String code, Model model){

        int sellerid = userService.findUseridBySession();
        UserEntity user = userService.findUserByID(sellerid);
        if(sellerid > 0 && !code.isEmpty()) {
            if (user.getRole() == Constants.ROLE_SELLER_ID) {
                OrderEntity order = orderService.findOrderByCode(code);
                List<OrderDetailEntity> listOrderDetail = new ArrayList<OrderDetailEntity>();
                long total = 0;
                if (order != null) {
                    listOrderDetail = orderDetailService.listBySellerIdAndOrderid(sellerid, order.getId());
                    for (OrderDetailEntity ord : listOrderDetail) {
                        total += ord.getPrice() * (1 - 1.0 * ord.getDiscount() / 100) * ord.getQuantity();
                    }
                }

                LayoutConfig layoutConfig = new LayoutConfig();
                layoutConfig.ActiveOrder = true;
                layoutConfig.title = "Chi tiết đơn hàng";
                model.addAttribute("config", layoutConfig);

                model.addAttribute("order", order);
                model.addAttribute("listOrderDetail", listOrderDetail);
                model.addAttribute("total", total);
                model.addAttribute("role", user.getRole());

                return "admin/order/detail";
            } else{
                OrderEntity order = orderService.findOrderByCode(code);
                List<OrderDetailEntity> listOrderDetail = new ArrayList<OrderDetailEntity>();
                long total = 0;
                if (order != null) {
                    listOrderDetail = orderDetailService.listByOrderid(order.getId());
                    for (OrderDetailEntity ord : listOrderDetail) {
                        total += ord.getPrice() * (1 - 1.0 * ord.getDiscount() / 100) * ord.getQuantity();
                    }
                }

                LayoutConfig layoutConfig = new LayoutConfig();
                layoutConfig.ActiveOrder = true;
                layoutConfig.title = "Chi tiết đơn hàng";
                model.addAttribute("config", layoutConfig);

                model.addAttribute("order", order);
                model.addAttribute("listOrderDetail", listOrderDetail);
                model.addAttribute("total", total);
                model.addAttribute("role", user.getRole());

                return "admin/order/detail";
            }
        }
        return "redirect:/admin/login";
    }

    @PostMapping("/update")
    private @ResponseBody ResponseEntity updateOrder(@RequestBody OrderDetailDto input){
        BaseResponse res = new BaseResponse();
        res.message = "Cập nhập thất bại !";

        int sellerid = userService.findUseridBySession();

        if (sellerid > 0 && input.getId() > 0) {
            OrderDetailEntity orderDetail = orderDetailService.findById(input.getId());
            OrderEntity order = orderService.findOrderById(orderDetail.getOrderid());
            if(orderDetail != null && orderDetail.getSellerid() == sellerid ){
                orderDetail.setStatus(Constants.ORDER_DETAIL_CONFIRM);
                orderDetail.setUpdateat(util.timestampNow());
                orderDetailService.updateOrderDetail(orderDetail);

                List<OrderDetailEntity> listDetail = orderDetailService.listByOrderidAndStatusNot(order.getId(),Constants.ORDER_DETAIL_CONFIRM);
                if (listDetail.size() == 0){
                    order.setStatus(2);
                    order.setUpdateat(util.timestampNow());
                    orderService.updateOrder(order);
                } else {
                    order.setStatus(Constants.ORDER_CONFIRM);
                    order.setUpdateat(util.timestampNow());
                    orderService.updateOrder(order);
                }
                res.status = 1;
                res.code = 200;
                res.message = "Đơn hàng đã được cập nhật thành công !";
                res.data = order.getCode();
            }else {
                res.message = "Cập nhập đơn hàng thất bại !";
            }
        }
        return ResponseEntity.ok(res);
    }

    @PostMapping("/delete")
    private @ResponseBody ResponseEntity deleteOrder(@RequestBody OrderDto input){
        BaseResponse res = new BaseResponse();

        res.message = "Cập nhập thất bại !";
        int sellerid = userService.findUseridBySession();

        if (sellerid > 0 && input.getId() > 0) {
            OrderEntity order = orderService.findOrderById(input.getId());
            if(order != null){
                List<OrderDetailEntity> listDetail = orderDetailService.listBySellerIdAndOrderid(sellerid,order.getId());
                if(listDetail != null){
                    long total = 0;
                    float discount = 0;
                    for (OrderDetailEntity ord : listDetail){
                        ord.setStatus(Constants.ORDER_DETAIL_REFUSE);
                        ord.setUpdateat(util.timestampNow());
                        total += ord.getPrice() * ord.getQuantity();
                        discount += ord.getPrice() * (1.0 * ord.getDiscount() / 100 ) * ord.getQuantity();
                    }
                    if(orderDetailService.updateList(listDetail)){
                        // update order
                        order.setTotalprice(order.getTotalprice() - total);
                        order.setDiscount(order.getDiscount() - discount);
                        order.setTotalPricePayment((long) (order.getTotalPricePayment() - (total + discount)));

                        List<OrderDetailEntity> listDetail2 = orderDetailService.listByOrderid(order.getId());
                        if(listDetail.size() == listDetail2.size() ){
                            order.setStatus(Constants.ORDER_REFUSE);
                            order.setTotalPricePayment(0);
                            order.setShip_fee(0);
                        }else {
                            // update khi tất cả status != 0
                            List<OrderDetailEntity> check = orderDetailService.listByOrderidandStatus(order.getId(), Constants.ORDER_DETAIL_WAIT_CONFIRM);
                            if(check.size() == 0){
                                // tất cả các đơn hàng của người bán trong order đó đều được confirm
                                order.setStatus(Constants.ORDER_FINISH);
                            }else {
                                order.setStatus(Constants.ORDER_CONFIRM);
                            }
                        }

                        order.setUpdateat(util.timestampNow());
                        orderService.updateOrder(order);

                        res.status = 1;
                        res.code = 200;
                        res.message = "Từ chối đơn hàng thành công ";
                    }
                }
            }
        }

        return ResponseEntity.ok(res);
    }

}
