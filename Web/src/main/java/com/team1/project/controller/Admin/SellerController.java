package com.team1.project.controller.Admin;

import com.team1.project.config.Constants;
import com.team1.project.config.Util;
import com.team1.project.dto.LayoutConfig;
import com.team1.project.dto.SellerDto;
import com.team1.project.entity.UserEntity;
import com.team1.project.model.BaseResponse;
import com.team1.project.service.EmailService;
import com.team1.project.service.RoleService;
import com.team1.project.service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class SellerController {
    @Autowired
    UserServiceImpl userServiceImpl;

    @Autowired
    RoleService roleService;

    @Autowired
    EmailService emailService;

    @Autowired
    PasswordEncoder passwordEncoder;

    Util util = new Util();

    @GetMapping({"/admin/seller"})
    public String findPaginatedUser(@RequestParam( required = false, defaultValue = "1",value = "page") int pageNo, Model model){
        Page<UserEntity> sellerPage= userServiceImpl.findPaginatedSeller(pageNo,Constants.PAGE_SIZE_WEB , "");
        List<UserEntity> listSellerPage= sellerPage.getContent();

        LayoutConfig layoutConfig = new LayoutConfig();
        layoutConfig.ActiveSeller = true;
        layoutConfig.title="Danh sách nhà cung cấp";
        model.addAttribute("config", layoutConfig);

        model.addAttribute("currentPage",pageNo);
        model.addAttribute("totalPages",sellerPage.getTotalPages());
        model.addAttribute("totalItems",sellerPage.getTotalElements());
        model.addAttribute("listSeller",listSellerPage);
        model.addAttribute("sellerPage",sellerPage);

        return "admin/seller";
    }

    /**
     * Admin create buyer
     * @param input
     * @return
     */
    @PostMapping({"/admin/seller/create"})
    private @ResponseBody  ResponseEntity createSeller(@RequestBody SellerDto input) {
        BaseResponse res = new BaseResponse();
        if (!input.getEmail().isEmpty() && util.isValidEmail(input.getEmail())) {
            UserEntity userByEmail = userServiceImpl.findUserByEmail(input.getEmail());
            UserEntity userByUsername = userServiceImpl.findSellerByUsername(input.getUsername());
            if(userByEmail != null){
                res.message = "Email đã tồn tại trong hệ thống";
            }else if(userByUsername != null){
                res.message = "Tài khoản đã tồn tại trong hệ thống";
            } else {
                UserEntity user = new UserEntity();
                user.setUsername(input.getUsername());
                user.setFullname(input.getFullname());
                user.setEmail(input.getEmail());
                user.setPassword(passwordEncoder.encode(input.getPassword()));
                user.setRole(Constants.ROLE_SELLER_ID);
                user.setCreateat(util.timestampNow());
                user.setIsactive(Constants.ACTIVE);

//                emailService.sendSimpleMessage(input.getEmail(),"Nông nghiệp sạch","Mật khẩu của quý khách là : " + input.getPassword());

                userServiceImpl.createSeller(user);

                res.status = 1;
                res.message = "Tạo tài khoản cho nhà cung cấp thành công!";

            }
        }else{
            res.status = 0;
            res.message = "Email không đúng định dạng !";
        }
        return ResponseEntity.ok(res);
    }

    /**
     * Admin delete seller
     * @param input
     * @return
     */
    @PostMapping({"/admin/seller/delete"})
    private @ResponseBody ResponseEntity deleteSeller(@RequestBody SellerDto input){
        BaseResponse res = new BaseResponse();
        res.status = 0;
        res.code = 200;
        res.message = "Tài khoản không tồn tại trong hệ thống !";

        if(input.getId() > 0){
            boolean checkSeller = userServiceImpl.findSellerByID(input.getId());
            if(checkSeller){
               userServiceImpl.deleteSeller(input.getId());

                res.status = 1;
                res.code = 200;
                res.message = "Xóa tài khoản nhà cung cấp thành công ";
            }
        }
        return ResponseEntity.ok(res);
    }

    /**
     * admin  update seller
     * @param input
     * @return
     */
    @PostMapping({"/admin/seller/update"})
    private @ResponseBody ResponseEntity updateSeller(@RequestBody SellerDto input){
        BaseResponse res = new BaseResponse();

        if(input != null && input.getId() > 0){
            UserEntity check_user_email =  userServiceImpl.findUserByEmail(input.getEmail());
            if(check_user_email != null && check_user_email.getId() != input.getId()){
                res.message = "Email đã tồn tạ trong hệ thống";
            }else{
                UserEntity userupdate = userServiceImpl.findUserByID(input.getId());
                if(userupdate != null ){
                    userupdate.setFullname(input.getFullname());
                    userupdate.setEmail(input.getEmail());

                    UserEntity u =  userServiceImpl.updateSeller(userupdate);
                    if(u != null){
                        res.status = 1;
                        res.message = "Cập nhập thành công ";
                    }else {
                        res.message = "Cập nhập thất bại ";
                    }
                }

            }
        }
        return ResponseEntity.ok(res);
    }

}
