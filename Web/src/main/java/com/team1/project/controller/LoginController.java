package com.team1.project.controller;

import com.team1.project.dto.RegisterDto;
import com.team1.project.entity.UserEntity;
import com.team1.project.model.BaseResponse;
import com.team1.project.security.service.SecurityService;
import com.team1.project.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class LoginController {
    @Autowired
    private SecurityService securityService;

    @Autowired
    UserService userService;

    @GetMapping({"/admin/login","/admin/login/"})
    private String adminLogin(@RequestParam(required = false, defaultValue = "false",value = "error") boolean err, Model model){
        boolean checkadminlogin = false;
        if(err){
            checkadminlogin = true;
        }
        model.addAttribute("checklogin",checkadminlogin);
        return "admin/login";
    }

    @GetMapping("/login")
    private String userLogin(@RequestParam(required = false, defaultValue = "false",value = "error") boolean err, Model model){
        boolean checklogin = false;
        if(err){
            checklogin = true;
        }
        model.addAttribute("checklogin",checklogin);
        return "public/login";
    }

    /**
     * Nguoi mua dang ký tài khoản
     * @return
     */
    @GetMapping({"/register","/register/"})
    private String userRegister(){
        return "public/register";
    }

    @PostMapping({"/register"})
    private @ResponseBody ResponseEntity register(@RequestBody RegisterDto input){
        BaseResponse res = new BaseResponse();

        UserEntity checkUsername = userService.findUserByUsername(input.getUsername());
        if(checkUsername != null){
            res.message = "Tài khoản đã tồn tại trong hệ thống !";
        }else if(userService.findUserByEmail(input.getEmail()) != null){
            res.message = "Email đã tồn tại trong hệ thống !";
        }else {
            UserEntity user = userService.createBuyer(input);
            if(user != null){
                securityService.autoLogin(input.getUsername(),input.getPassword());

                res.status = 1;
                res.code = 200;
                res.message = "Đăng ký thành công";
            }
        }
        return ResponseEntity.ok(res);
    }
}
