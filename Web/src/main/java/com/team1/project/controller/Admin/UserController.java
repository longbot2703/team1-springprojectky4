package com.team1.project.controller.Admin;

import com.team1.project.config.Constants;
import com.team1.project.dto.LayoutConfig;
import com.team1.project.entity.UserEntity;
import com.team1.project.service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;


@Controller
public class UserController {
    @Autowired
    UserServiceImpl userServiceImpl;

    @GetMapping({"/admin/user"})
    public String findPaginatedUser(@RequestParam( required = false, defaultValue = "1",value = "page") int pageNo, Model model){
        int pageSize = Constants.PAGE_SIZE_WEB;
        Page<UserEntity> page= userServiceImpl.findPaginated(pageNo,pageSize);
        List<UserEntity> listUserPage= page.getContent();

        LayoutConfig layoutConfig = new LayoutConfig();
        layoutConfig.ActiveUser =true;
        layoutConfig.title="User";
        model.addAttribute("config", layoutConfig);

        model.addAttribute("currentPage",pageNo);
        model.addAttribute("totalPages",page.getTotalPages());
        model.addAttribute("totalItems",page.getTotalElements());
        model.addAttribute("listuser",listUserPage);

        return "admin/user";
    }

    //Hàm điều hướng URL theo các loại tài khoản sau khi login
    @GetMapping("/defaulturl")
    public  String setUrlDefault(){
        UserDetails data = userServiceImpl.getCurentUser();
        String role = data.getAuthorities().toArray()[0].toString();
if(role.equals(Constants.ROLE_BUYER))
    return "redirect:/index";
        return "redirect:/admin/index";
//        switch (role){
//            case Constants.ROLE_BUYER:
//                return "public/index";
//                break;
//            case Constants.ROLE_ADMIN:
//                return "admin/user";
//            default:
//                break;
//        }
    }

    //Đây chỉ là hàm viết mẫu lấy userInfo từ session nha ae, cần userRole,userName thì tham khảo nhé
    @GetMapping("admin/getUserInfo")
    public void test(){
        try{
            UserDetails data = userServiceImpl.getCurentUser();
            String name = data.getUsername();
            String role = data.getAuthorities().toArray()[0].toString();
            UserEntity user = userServiceImpl.findUserByUsername(name, role);
        }catch (Exception e){
            e.toString();
        }
    }
}
