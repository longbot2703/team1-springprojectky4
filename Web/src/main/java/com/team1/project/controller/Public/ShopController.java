package com.team1.project.controller.Public;

import com.team1.project.dto.FontEndConfig;
import com.team1.project.entity.CategoryEntity;
import com.team1.project.entity.ProductEntity;
import com.team1.project.entity.UserEntity;
import com.team1.project.service.CartService;
import com.team1.project.service.CategoryService;
import com.team1.project.service.ProductService;
import com.team1.project.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

@Controller
public class ShopController {

    @Autowired
    ProductService productService;

    @Autowired
    CategoryService categoryService;

    @Autowired
    CartService cartService;

    @Autowired
    UserService userService;


    @GetMapping({"/shop"})
    private String index( @RequestParam( required = false, defaultValue = "0",value = "cate") int cateid,
                          @RequestParam( required = false, defaultValue = "",value = "search") String search,
                          @RequestParam( required = false, defaultValue = "1",value = "page") int pageNo, Model model){

        Page<ProductEntity> page = productService.findAllPageProduct(cateid,search,pageNo);
        List<ProductEntity> listProduct = page.getContent();

        List<CategoryEntity> listCate = categoryService.fillAll();
        model.addAttribute("searchCate",cateid);
        model.addAttribute("searchPr",search);


        FontEndConfig config = new FontEndConfig();
        config.ActiveShop = true;
        config.countCart = cartService.CountCart();
        model.addAttribute("config",config);

        model.addAttribute("currentPage",pageNo);
        model.addAttribute("totalPages",page.getTotalPages());
        model.addAttribute("totalItems",page.getTotalElements());
        model.addAttribute("listProduct",listProduct);
        model.addAttribute("listCategory",listCate);
        return "public/shop";
    }


    @GetMapping({"/shop/detail"})
    private String productDetail( @RequestParam( required = false, defaultValue = "0",value = "product") int productid, Model model){

        ProductEntity product = new ProductEntity();
        List<ProductEntity> listPr = new ArrayList<>();
        if(productid > 0){
            product = productService.findByID(productid);
            String userName = userService.findUserByID(product.getSellerby()).getFullname();
            model.addAttribute("userName",userName);
            if(product != null){
                listPr = productService.findTop8ByCategory(product.getCateid(),productid);
            }
        }

        FontEndConfig config = new FontEndConfig();
        config.ActiveShop = true;
        config.countCart = cartService.CountCart();
        model.addAttribute("config",config);

        model.addAttribute("product",product);
        model.addAttribute("listProductBySeller",listPr);

        return "public/productdetail";
    }

}
