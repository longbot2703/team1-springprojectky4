package com.team1.project.controller.Public;

import com.team1.project.config.Constants;
import com.team1.project.dto.CartDto;
import com.team1.project.dto.FontEndConfig;
import com.team1.project.dto.OrderDto;
import com.team1.project.dto.WishlistDto;
import com.team1.project.entity.CartEntity;
import com.team1.project.entity.OrderEntity;
import com.team1.project.entity.UserEntity;
import com.team1.project.entity.WishlistEntity;
import com.team1.project.model.BaseResponse;
import com.team1.project.service.CartService;
import com.team1.project.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/customer/cart")
public class CartController {
    @Autowired
    CartService cartService;

    @Autowired
    UserService userService;


    @GetMapping({"","/"})
    public String getCart(Model model){
        int userid = userService.findUseridBySession();
        if(userid > 0) {
            FontEndConfig config = new FontEndConfig();
            config.ActiveCart = true;
            config.countCart = cartService.CountCart();
            model.addAttribute("config",config);

            UserEntity user=userService.findUserByID(userid);
            if(user!=null){
                String address=user.getAddress();
                String city="";
                String district="";
                String addressDetail="";
                if (address!=null && address.length()>0){
                    String[] lsaddress=address.split(",");
                    if(lsaddress.length>=3){
                        city=lsaddress[lsaddress.length-1].trim();
                        district=lsaddress[lsaddress.length-2].trim();
                        addressDetail="";
                        for (int i=0;i<lsaddress.length-2;i++){
                            if (i==0){
                                addressDetail+=lsaddress[i];
                            }
                            else {
                                addressDetail+=","+lsaddress[i];
                            }
                        }
                    }

                    model.addAttribute("city",city);
                    model.addAttribute("district",district);
                    model.addAttribute("addressDetail",addressDetail.trim());
                }
            }

            List<CartEntity> listCart = cartService.getListByUserid(userid);
            if(listCart != null){
                long totalPrice = 0;
                float discount = 0;
                for (CartEntity cart : listCart) {
                    totalPrice += cart.getCartProduct().getPrice() * cart.getQuantity();
                    if(cart.getCartProduct().getDiscount() > 0){
                        discount +=  cart.getQuantity() *  cart.getCartProduct().getPrice() * ( 1.0 * cart.getCartProduct().getDiscount() / 100);
                    }
                }
                model.addAttribute("totalPrice", totalPrice);
                model.addAttribute("discount", discount);
            }
            model.addAttribute("listCart", listCart);

            return "public/cart";
        }
        return "redirect:/logout";
    }

    @PostMapping({"/add"})
    public @ResponseBody ResponseEntity createCart(@RequestBody CartDto input){
        BaseResponse res = new BaseResponse();
        res.message = "Thêm vào giỏ hàng thất bại ";

        int userid = userService.findUseridBySession();
        if (userid > 0){
            input.setUserid(userid);
            CartEntity data = cartService.createCart(userid, input.getProductid(), input.getQuantity());
            if(data != null){
                res.status = 1;
                res.code = 200;
                res.message = "Thêm vào giỏ hàng thành công ";
            }
        }
        return ResponseEntity.ok(res);
    }

    @PostMapping({"/delete"})
    public @ResponseBody ResponseEntity deleteCart(@RequestBody CartDto input){
        BaseResponse res = new BaseResponse();
        res.message = "Xóa giỏ hàng thất bại !";

        int userid = userService.findUseridBySession();
        if(userid > 0){
            CartEntity data = cartService.deleteCart(userid,input.getId());
            if(data != null){
                res.status = 1;
                res.code = 200;
                res.message = "Xóa thành công !";
            }
        }
        return ResponseEntity.ok(res);
    }

    @PostMapping({"/createOrder"})
    public @ResponseBody ResponseEntity createOrder(@RequestBody OrderDto input, HttpServletRequest request){
        BaseResponse res = new BaseResponse();
        OrderDto orderDto = (OrderDto) request.getSession().getAttribute(Constants.ORDER_CREATE_SESSION);

        if (orderDto == null) {
            orderDto = new OrderDto();
            request.getSession().setAttribute(Constants.ORDER_CREATE_SESSION, orderDto);
        }
        orderDto.setDiscount(input.getDiscount());
        orderDto.setDelivery(input.getDelivery());
        orderDto.setTotalPrice(input.getTotalPrice());

        request.getSession().setAttribute(Constants.ORDER_CREATE_SESSION, orderDto);
        res.status = 1;
        res.code = 200;
        res.data = "/customer/order";
        return ResponseEntity.ok(res);
    }

}
