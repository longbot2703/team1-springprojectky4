package com.team1.project.controller.Admin;

import com.team1.project.config.Constants;
import com.team1.project.dto.CustomerDTO;
import com.team1.project.dto.LayoutConfig;
import com.team1.project.dto.NewsDto;
import com.team1.project.entity.CustomerEntity;
import com.team1.project.entity.NewEntity;
import com.team1.project.entity.UserEntity;
import com.team1.project.model.BaseResponse;
import com.team1.project.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
public class CustomerController {
@Autowired
    CustomerService customerService;

@GetMapping({"/admin/listcustomer"})
    public String findPaginatedCus(@RequestParam( required = false, defaultValue = "1",value = "page") int pageNo, Model model){
    int pageSize = Constants.PAGE_SIZE_WEB;
    Page<UserEntity> page = customerService.findPaginated(pageNo,pageSize);
    List<UserEntity> listCus = page.getContent();

    LayoutConfig layoutConfig = new LayoutConfig();
    layoutConfig.ActiveCustomer =true;
    layoutConfig.title="Danh Sách Khách Hàng";
    model.addAttribute("config", layoutConfig);

    model.addAttribute("currentPage",pageNo);
    model.addAttribute("totalPages",page.getTotalPages());
    model.addAttribute("totalItems",page.getTotalElements());
    model.addAttribute("listCus",listCus);
    return "admin/customer/listcustomer";
}

    @GetMapping({"/admin/customer/create"})
    public String createCus(Model model) {
        LayoutConfig layoutConfig = new LayoutConfig();
        layoutConfig.ActiveCustomer =true;
        layoutConfig.title="Customer";
        model.addAttribute("config", layoutConfig);
        CustomerEntity cus = new CustomerEntity();
        model.addAttribute("cus", cus);
        return "admin/customer/createcustomer";
    }

    @PostMapping({"/admin/customer/create"})
    public String createCusPost(@Valid @ModelAttribute("cus") CustomerEntity cus,
                                 BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "admin/customer/createcustomer";
        }
        customerService.addCus(cus);
        return "redirect:/admin/listcustomer";
    }

    @GetMapping("/admin/customer/edit/{id}")
    public String editCus (@PathVariable("id") Integer id, Model model) {
        LayoutConfig layoutConfig = new LayoutConfig();
        layoutConfig.ActiveCustomer =true;
        layoutConfig.title="Customer";
        model.addAttribute("config", layoutConfig);
        CustomerEntity cus = customerService.getCusById(id);
        model.addAttribute("cus", cus);
        return "admin/customer/editcustomer";
    }

    @PostMapping("/admin/customer/edit/{id}")
    public String editNewsPost(@PathVariable("id") Integer id, @Valid @ModelAttribute("cus") CustomerEntity cus, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            cus.setId(id);
            return "admin/customer/editcustomer";
        }
        customerService.editCus(cus);
        return "redirect:/admin/listcustomer";
    }

    @PostMapping("/admin/customer/delete")
    private @ResponseBody
    ResponseEntity deleteCus(@RequestBody CustomerDTO input){
        BaseResponse res = new BaseResponse();

        if(input.getId() > 0){
            customerService.deleteCus(input.getId());

            res.status = 1;
            res.code = 200;
            res.message = "Xóa khách hàng thành công ";
        } else {
            res.status = 0;
            res.code = 200;
            res.message = "khách hàng không tồn tại trong hệ thống !";
        }
        return ResponseEntity.ok(res);
    }
}
