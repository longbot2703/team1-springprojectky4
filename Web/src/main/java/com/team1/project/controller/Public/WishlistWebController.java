package com.team1.project.controller.Public;

import com.team1.project.config.Constants;
import com.team1.project.dto.FontEndConfig;
import com.team1.project.dto.WishlistDto;
import com.team1.project.entity.UserEntity;
import com.team1.project.entity.WishlistEntity;
import com.team1.project.model.BaseResponse;
import com.team1.project.service.CartService;
import com.team1.project.service.UserService;
import com.team1.project.service.UserServiceImpl;
import com.team1.project.service.WishlistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/customer/wishlist")
public class WishlistWebController {
    @Autowired
    UserServiceImpl userServiceImpl;

    @Autowired
    UserService userService;

    @Autowired
    CartService cartService;

    @Autowired
    WishlistService wishlistService;

    @GetMapping({""})
    private String Wishlist(@RequestParam( required = false, defaultValue = "1",value = "page") int pageNo, Model model ){
        int totalPages = 0 ;
        long totalItem = 0;

        int userid =  userService.findUseridBySession();

        if(userid > 0 ){
            Page<WishlistEntity> listPages = wishlistService.getListWishlist("",userid, pageNo, Constants.PAGE_SIZE_WEB);
            totalPages = listPages.getTotalPages();
            totalItem = listPages.getTotalElements();
            model.addAttribute("listWishlist",listPages);
        }else {
            return "redirect:/login";
        }

        FontEndConfig config = new FontEndConfig();
        config.ActiveShop = true;
        config.countCart = cartService.CountCart();
        model.addAttribute("config",config);

        model.addAttribute("currentPage",pageNo);
        model.addAttribute("totalPages", totalPages);
        model.addAttribute("totalItems",totalItem);

        return "public/wishlist";
    }

    @PostMapping({"/update"})
    private @ResponseBody ResponseEntity updateWishlist(@RequestBody  WishlistDto input){

        BaseResponse res = new BaseResponse();
        int userid =  userService.findUseridBySession();
        if(userid > 0){
            input.setUserid(userid);
            WishlistEntity data = wishlistService.updateWishlist(input);
            if(data != null){
                res.status = 1;
                res.code = 200;
                if(data.getIsactive() == Constants.ACTIVE){
                    res.message = "Thêm sản phẩm vào danh mục ưa thích thành công";

                }else {
                    res.message = "Xóa sản phẩm khỏi danh mục ưa thích thành công";

                }
            }else {
                res.message = "Thất bại";
            }
        }else {
            res.code = 404;
            res.message= "Đăng nhập để thực hiện chức năng này";
        }
        return ResponseEntity.ok(res);
    }


}
