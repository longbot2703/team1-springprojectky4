package com.team1.project.controller.Admin;

import com.team1.project.config.Constants;
import com.team1.project.dto.LayoutConfig;
import com.team1.project.dto.ShippingDto;
import com.team1.project.entity.ShippingEntity;
import com.team1.project.model.BaseResponse;
import com.team1.project.service.ShippingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
public class ShippingController {
@Autowired
    ShippingService shippingService;

@GetMapping("admin/listshipping")
public String findPaginate(@RequestParam( required = false, defaultValue = "1",value = "page") int pageNo, Model model){
    int pageSize = Constants.PAGE_SIZE_WEB;
    Page<ShippingEntity> page = shippingService.findPaginated(pageNo,pageSize);
    List<ShippingEntity> listShipping = page.getContent();

    LayoutConfig layoutConfig = new LayoutConfig();
    layoutConfig.ActiveShipping =true;
    layoutConfig.title="Danh Sách Shipper";
    model.addAttribute("config", layoutConfig);

    model.addAttribute("currentPage",pageNo);
    model.addAttribute("totalPages",page.getTotalPages());
    model.addAttribute("totalItems",page.getTotalElements());
    model.addAttribute("listShipping",listShipping);

    return "admin/shipping/listshipper";
}

@GetMapping({"/admin/edit/shipper/{id}"})
public String editShipper(@PathVariable("id") Integer id,Model model){
    LayoutConfig layoutConfig = new LayoutConfig();
    layoutConfig.ActiveShipping =true;
    layoutConfig.title="Chỉnh Sửa Shipper";
    model.addAttribute("config", layoutConfig);
    ShippingEntity shipper = shippingService.getshipById(id);
    model.addAttribute("shipper", shipper);
    return "admin/shipping/editshipper";
}

@PostMapping({"/admin/edit/shipper/{id}"})
public String editShipperPost(@PathVariable("id") Integer id, @Valid @ModelAttribute("shipper") ShippingEntity shipper, BindingResult bindingResult){
if(bindingResult.hasErrors()){
shipper.setId(id);
return "admin/shipping/editshipper";
}
shippingService.editShipping(shipper);
return "redirect:/admin/listshipping";
}

@PostMapping({"/admin/delete/shipper"})
private @ResponseBody ResponseEntity deleteNews(@RequestBody ShippingDto input){
    BaseResponse res = new BaseResponse();
    res.status = 0;
    res.code = 200;
    res.message = "Shipper không tồn tại trong hệ thống !";

    if(input.getId() > 0){
        shippingService.deleteShipping(input.getId());
        res.status = 1;
        res.code = 200;
        res.message = "Xóa Shipper thành công ";
    }
    return ResponseEntity.ok(res);
}


@GetMapping({"/admin/shipping/create"})
    public String createShip(Model model){
    LayoutConfig layoutConfig = new LayoutConfig();
    layoutConfig.ActiveShipping =true;
    layoutConfig.title="Tạo Mới Shipper";
    model.addAttribute("config", layoutConfig);
    ShippingEntity shipper = new ShippingEntity();
    model.addAttribute("shipper", shipper);
    return "admin/shipping/createshipper";
}

    @PostMapping({"/admin/shipping/create"})
    public String createNewsPost(@Valid @ModelAttribute("shipper") ShippingEntity shipper,
                                 BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "admin/shipping/createshipper";
        }
        shippingService.addShipping(shipper);
        return "redirect:/admin/listshipping";
    }

}
