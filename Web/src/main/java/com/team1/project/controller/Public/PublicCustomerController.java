package com.team1.project.controller.Public;

import com.team1.project.dto.ContactDto;
import com.team1.project.dto.FontEndConfig;
import com.team1.project.dto.UserDto;
import com.team1.project.entity.CartEntity;
import com.team1.project.entity.OrderEntity;
import com.team1.project.entity.UserEntity;
import com.team1.project.model.BaseResponse;
import com.team1.project.service.CartService;
import com.team1.project.service.CustomerService;
import com.team1.project.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/customer/detail")
public class PublicCustomerController {
    @Autowired
    UserService userService;

    @Autowired
    CustomerService customerService;

    @Autowired
    CartService cartService;

    
    @GetMapping({""})
    public String getCustomerDetail(Model model){
        int userid = userService.findUseridBySession();
        if(userid > 0){
            UserEntity user = userService.findUserByID(userid);
            user.setCustomer(customerService.getCusByUserID(user.getId()));

            FontEndConfig config = new FontEndConfig();
            config.ActiveAccount = true;
            config.countCart = cartService.CountCart();
            model.addAttribute("config",config);

            model.addAttribute("customerDetail",user);
            return "public/user";
        }
        return "redirect:/login";
    }

    @PostMapping({"/update"})
    private @ResponseBody ResponseEntity sendContact(@RequestBody UserDto input){
        BaseResponse res = new BaseResponse();
        int userid = userService.findUseridBySession();
        if(userid > 0){
            // check email
            UserEntity checkEmail = userService.findUserByEmail(input.getEmail());

            if(checkEmail.getId() != userid){
                res.message = "Email đã tồn tại trong hệ thống !";
            }else if( !input.getCustomer().getPhone().isEmpty() &&  customerService.checkPhone(userid,input.getCustomer().getPhone())){
                res.message = "Số điện thoại đã tồn tại trong hệ thống !";
            }else{
                input.setId(userid);
                if(userService.updateCustomer(input)){
                    res.status = 1;
                    res.code = 200;
                    res.message = "Cập nhập thông tin thành công !";
                }
            }
        }else {
            res.code= 404;
        }
        return ResponseEntity.ok(res);
    }
}
