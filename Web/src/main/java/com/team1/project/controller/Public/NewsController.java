package com.team1.project.controller.Public;

import com.team1.project.config.Constants;
import com.team1.project.dto.CategoryDto;
import com.team1.project.dto.FontEndConfig;
import com.team1.project.dto.MapperDto;
import com.team1.project.dto.NewsDto;
import com.team1.project.entity.CategoryEntity;
import com.team1.project.entity.NewEntity;
import com.team1.project.entity.ProductEntity;
import com.team1.project.service.CartService;
import com.team1.project.service.CategoryService;
import com.team1.project.service.NewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/news")
public class NewsController {
    @Autowired
    NewService newService;

    @Autowired
    CategoryService categoryService;

    @Autowired
    CartService cartService;

    @Autowired
    MapperDto mapperDto;

    private List<CategoryDto> listCateDto;

    private void getListCate(){
        List<CategoryEntity> listCateEntity = categoryService.getAllCategory();
        this.listCateDto = mapperDto.convertToAllCateDto(listCateEntity);
    }

    @GetMapping("")
    private String getNews(@RequestParam( required = false, defaultValue = "1",value = "page") int pageNo,
                           @RequestParam( required = false, defaultValue = "",value = "search") String search,
                           Model model){
        FontEndConfig config = new FontEndConfig();
        config.ActiveNews = true;
        config.countCart = cartService.CountCart();
        model.addAttribute("config",config);

        int pageSize = Constants.PAGE_SIZE_WEB;
        List<NewsDto> listNewsDto = new ArrayList<>();
        int totalPages =0;
        long totalItems = 0;

        this.getListCate();

        Page<NewEntity> page = newService.getListNews(search,pageNo,pageSize);
        List<NewEntity> listNews = page.getContent();
        if (listNews != null){
            listNewsDto = mapperDto.convertToAllNewsDto(listNews);
            totalItems = page.getTotalElements();
            totalPages = page.getTotalPages();
        }


        model.addAttribute("currentPage",pageNo);
        model.addAttribute("totalPages",totalPages);
        model.addAttribute("totalItems",totalItems);
        model.addAttribute("listNews",listNewsDto);
        model.addAttribute("listCategory",listCateDto);
        return "public/news";
    }

    @GetMapping("/detail")
    private String getNewsDetail(@RequestParam( required = false, defaultValue = "0",value = "id") int newsId,
                                 Model model){
        FontEndConfig config = new FontEndConfig();
        config.ActiveNews = true;
        config.countCart = cartService.CountCart();
        model.addAttribute("config",config);

        NewsDto news = new NewsDto();
        if(newsId > 0){
            news = mapperDto.convertToNewsDto(newService.getNewsById(newsId));
        }

        model.addAttribute("news",news);
        model.addAttribute("listCategory",listCateDto);
        return "public/newsdetail";
    }


}
