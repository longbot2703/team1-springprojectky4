package com.team1.project.controller.Public;

import com.team1.project.dto.FontEndConfig;
import com.team1.project.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class AboutController {
    @Autowired
    CartService cartService;

    @GetMapping({"/about"})
    private String about(Model model){
        FontEndConfig config = new FontEndConfig();
        config.ActiveAbout = true;
        config.countCart = cartService.CountCart();
        model.addAttribute("config",config);

        return "public/about";
    }
}
