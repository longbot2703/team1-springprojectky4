package com.team1.project.controller.Public;

import com.team1.project.config.Constants;
import com.team1.project.dto.ContactDto;
import com.team1.project.dto.FontEndConfig;
import com.team1.project.model.BaseResponse;
import com.team1.project.service.CartService;
import com.team1.project.service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/contact")
public class ContactController {
    @Autowired
    EmailService emailService;

    @Autowired
    CartService cartService;

    @GetMapping("")
    private String contact(Model model){
        FontEndConfig config = new FontEndConfig();
        config.ActiveContact = true;
        config.countCart = cartService.CountCart();
        model.addAttribute("config",config);

        return "public/contact";
    }

    @PostMapping("/send")
    private @ResponseBody ResponseEntity sendContact(@RequestBody ContactDto contact){
        BaseResponse res = new BaseResponse();

        if(!contact.getEmail().isEmpty()){
            emailService.sendSimpleMessage(contact.getEmail(),"Nông nghiệp sạch","Cảm ơn bạn đã đóng góp ý kiến");

            emailService.sendSimpleMessage(Constants.ADMIN_EMAIL,"New Contact: "+contact.getSubject() + " from "+ contact.getName()+ " " + contact.getEmail(), contact.getMessage());

            res.status = 1;
            res.code = 200;
            res.message = "Thành công";

        }
        return ResponseEntity.ok(res);
    }
}
