package com.team1.project.model;

public class BaseResponse<T> {
    public int status = 0;
    public int code = 500;
    public String message="Server Error !";
    public T data;
}
