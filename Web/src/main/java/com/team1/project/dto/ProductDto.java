package com.team1.project.dto;

import lombok.Data;

import javax.validation.constraints.Min;

public class ProductDto {
    @Min(value = 1 ,message = "Id > 0")
    private int id;
    private String name;
    private String description;
    private String image;
    private String imagedetail;
    private String unit;
    private float quantity;
    private long price;
    private int discount;
    private int status;
    private int sellerby;

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getImage() {
        return image;
    }
    public void setImage(String image) {
        this.image = image;
    }
    public String getImagedetail() {
        return imagedetail;
    }
    public void setImagedetail(String imagedetail) {
        this.imagedetail = imagedetail;
    }
    public String getUnit() {
        return unit;
    }
    public void setUnit(String unit) {
        this.unit = unit;
    }
    public float getQuantity() {
        return quantity;
    }
    public void setQuantity(float quantity) {
        this.quantity = quantity;
    }
    public long getPrice() {
        return price;
    }
    public void setPrice(long price) {
        this.price = price;
    }
    public int getDiscount() {
        return discount;
    }
    public void setDiscount(int discount) {
        this.discount = discount;
    }
    public int getStatus() {
        return status;
    }
    public void setStatus(int status) {
        this.status = status;
    }
    public int getSellerby() {
        return sellerby;
    }
    public void setSellerby(int sellerby) {
        this.sellerby = sellerby;
    }

}
