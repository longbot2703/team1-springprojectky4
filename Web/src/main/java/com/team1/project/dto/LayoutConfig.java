package com.team1.project.dto;

public class LayoutConfig {
    public boolean ActiveDashboard = false;
    public boolean ActiveCategory = false;
    public boolean ActiveUser = false;
    public boolean ActiveNew = false;
    public boolean ActiveCustomer = false;
    public boolean ActiveRelation = false;
    public boolean ActiveNotify = false;
    public boolean ActiveShipping = false;
    public boolean ActiveBuyer = false;
    public boolean ActiveSeller = false;
    public boolean ActiveWishlist = false;
    public boolean ActiveProduct = false;
    public boolean ActiveOrder = false;
    public boolean ActiveStaticstical = false;
    public String title ="Dashboard";
}
