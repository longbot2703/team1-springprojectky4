package com.team1.project.dto;

import lombok.Data;

import javax.validation.constraints.Min;
@Data
public class ShippingDto {
    @Min(value=1,message="Id>0")
    private int id;
}
