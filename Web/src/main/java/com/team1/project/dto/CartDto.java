package com.team1.project.dto;

import lombok.Data;

@Data
public class CartDto {
    private int id;
    private int productid;
    private int userid;
    private int quantity;
}
