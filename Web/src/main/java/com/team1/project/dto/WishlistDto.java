package com.team1.project.dto;

import lombok.Data;

@Data
public class WishlistDto {
    private int userid;
    private int productid;
}
