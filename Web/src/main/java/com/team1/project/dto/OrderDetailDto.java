package com.team1.project.dto;

import com.team1.project.entity.CartEntity;
import lombok.Data;

import java.util.List;

@Data
public class OrderDetailDto {
    private int id ;
    private int orderid;
    private int productid;
    private int sellerid;
    private int status = 0;
    private long price = 0;
    private int quantity = 0;
    private int discount = 0;
}
