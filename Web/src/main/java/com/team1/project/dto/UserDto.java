package com.team1.project.dto;

import com.team1.project.config.Constants;
import lombok.Data;

@Data
public class UserDto {
    private int id;

    private String username;

    private String fullname;

    private String email;

    private String address;

    private int sex = Constants.MALE;

    private CustomerDTO customer;
}
