package com.team1.project.dto;

import lombok.Data;

import javax.validation.constraints.Min;

@Data
public class CustomerDTO {
    @Min(value = 1 ,message = "Id > 0")
    private int id;

    private String phone;

    private String job;

    private int userid;

    private String imgUrl;

    private String address;
}
