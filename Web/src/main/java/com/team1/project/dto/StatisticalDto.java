package com.team1.project.dto;



public class StatisticalDto {
    private  int stt;
    private  String username;
    private  long total;
    private  int complete;
    private  int processing;
    private  int reject;

    public StatisticalDto(int stt, String username, long total, int complete, int processing, int reject) {
        this.stt = stt;
        this.username = username;
        this.total = total;
        this.complete = complete;
        this.processing = processing;
        this.reject = reject;
    }

    public int getStt() {
        return stt;
    }

    public void setStt(int stt) {
        this.stt = stt;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public int getComplete() {
        return complete;
    }

    public void setComplete(int complete) {
        this.complete = complete;
    }

    public int getProcessing() {
        return processing;
    }

    public void setProcessing(int processing) {
        this.processing = processing;
    }

    public int getReject() {
        return reject;
    }

    public void setReject(int reject) {
        this.reject = reject;
    }
}
