package com.team1.project.dto;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
public class SellerDto {
    @Min(value = 1 ,message = "Id > 0")
    private int id;
    @NotNull(message = "Username not null")
    private String username;
    private String fullname;
    private String email;
    private String password;
}
