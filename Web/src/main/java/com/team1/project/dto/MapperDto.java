package com.team1.project.dto;

import com.team1.project.entity.CategoryEntity;
import com.team1.project.entity.NewEntity;
import com.team1.project.entity.ProductEntity;
import org.mapstruct.Mapper;
import org.mapstruct.NullValueCheckStrategy;

import java.util.List;

@Mapper(componentModel = "spring", nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface MapperDto {
    CategoryEntity convertToCategoryEntity(CategoryDto categoryDto);
    CategoryDto convertToCategoryDto(CategoryEntity categoryEntity);
    List<CategoryDto> convertToAllCateDto(List<CategoryEntity> listCateEntity);

    ProductEntity convertToProductEntity(ProductDto productDto);
    ProductDto convertToProductDto(ProductEntity productEntity);
    List<ProductDto> convertToAllProductDto(List<ProductEntity> productEntityList);

    List<NewsDto> convertToAllNewsDto(List<NewEntity> newEntityList);
    NewsDto convertToNewsDto(NewEntity newEntity);
}
