package com.team1.project.dto;

import com.team1.project.config.Constants;
import lombok.Data;

@Data
public class RequestPageDto {
    private int pageNo = 1;
    private int pageSize = Constants.PAGE_SIZE_APP;
    private String search = null;
}
