package com.team1.project.dto;

import javax.validation.constraints.Min;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public class NewsDto {
    @Min(value = 1 ,message = "Id > 0")
    private int id;
    private String title;
    private String description;
    private String content;
    private String imageurl;
    private Timestamp createdat;
    private Timestamp updatedat;
    private String timeStr;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImageurl() {
        return "/img/news/" + imageurl;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }

    public Timestamp getCreatedat() {
        return createdat;
    }

    public void setCreatedat(Timestamp createdat) {
        this.createdat = createdat;
    }

    public Timestamp getUpdatedat() {
        return updatedat;
    }

    public void setUpdatedat(Timestamp updatedat) {
        this.updatedat = updatedat;
    }

    public String getTimeStr() {
        if(updatedat != null){
            return new SimpleDateFormat("dd-MM-yyyy").format(updatedat);
        }else {
            return new SimpleDateFormat("dd-MM-yyyy").format(createdat);
        }
    }
}
