package com.team1.project.dto;

import com.team1.project.entity.CartEntity;
import lombok.Data;

import java.util.List;

@Data
public class OrderDto {
    private int buyerid;
    private float discount = 0;
    private float delivery = 0;
    private long totalPrice = 0;
    private int ship_fee = 25000;
    private String code;
    private String fullname;
    private String note;
    private String address;
    private String email;
    private String phone;
    private int sellerid;
    private List<Integer> listCartBySeller;
    private List<CartEntity> listCart;
    private int status = 0;
    private int id ;
    private int check_paypal = 1;

}
