package com.team1.project.dto;

import java.util.List;

public class CategoryDto {
    private int id;
    private String name;
    private String description;
    private String image;
    private int countProduct;

    private List<ProductDto> listProduct;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<ProductDto> getListProduct() {
        return listProduct;
    }

    public void setListProduct(List<ProductDto> listProduct) {
        this.listProduct = listProduct;
    }

    public int getCountProduct() {
        return this.listProduct.size();
    }

}
