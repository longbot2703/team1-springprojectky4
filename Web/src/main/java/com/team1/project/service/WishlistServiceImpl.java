package com.team1.project.service;

import com.team1.project.config.Constants;
import com.team1.project.config.Util;
import com.team1.project.dto.WishlistDto;
import com.team1.project.entity.WishlistEntity;
import com.team1.project.repository.WishlistRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class WishlistServiceImpl implements WishlistService{

    @Autowired
    WishlistRepository wishlistRepository;

    @Override
    public Page<WishlistEntity> findPaginated(int pageNo, int pageSize) {
        Pageable pageable = PageRequest.of(pageNo-1,pageSize);
        return wishlistRepository.findAllByIsactiveEquals(Constants.ACTIVE,pageable);
    }

    @Override
    public Page<WishlistEntity> getListWishlist(String search, int userid, int pageNo, int pageSize) {
        Pageable pageable = PageRequest.of(pageNo-1,pageSize);
        return wishlistRepository.findAllByUseridAndIsactive(userid ,Constants.ACTIVE, pageable);
    }

    @Override
    public WishlistEntity updateWishlist(WishlistDto input) {
        WishlistEntity check = wishlistRepository.findByUseridAndProducid(input.getUserid(),input.getProductid());
        if(check != null){
            if (check.getIsactive() == Constants.ACTIVE) {
                check.setIsactive(Constants.NO_ACTIVE);
            } else {
                check.setIsactive(Constants.ACTIVE);
            }
            return wishlistRepository.save(check);
        }else{
            return  wishlistRepository.save(new WishlistEntity(input.getUserid(),input.getProductid(),Constants.ACTIVE, new Util().timestampNow()));
        }
    }
}
