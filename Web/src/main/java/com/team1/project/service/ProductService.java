package com.team1.project.service;

import com.team1.project.entity.ProductEntity;
import org.springframework.data.domain.Page;

import java.util.List;

public interface ProductService {
    Page<ProductEntity> findPaginated(int pageNo, int pageSize, int userid);
    Page<ProductEntity> findAllPageProduct(int cateid, String search, int pageNo);
    ProductEntity findByID(int id);
    List<ProductEntity> findByCreateBy(int userID);
    ProductEntity addProduct(ProductEntity product, int businessID);
    ProductEntity editProduct(ProductEntity product);
    ProductEntity deleteProduct(int id);
    List<ProductEntity> findNewProduct(String name);
    List<ProductEntity> findTop8ByCategory(int cateid,int productid);

    List<ProductEntity> get12NewProduct(String name);
    List<ProductEntity> saveAll(List<ProductEntity> listPro);
}
