package com.team1.project.service;

import com.team1.project.entity.CategoryEntity;
import com.team1.project.model.BaseResponse;
import org.springframework.data.jpa.repository.Query;


import java.util.List;

public interface CategoryService {

    List<CategoryEntity> fillAll();
    List<CategoryEntity> getAllCategory();
    List<CategoryEntity>findAllByNameAndIsactive(String name, int isactive);

    List<CategoryEntity> GetAll();
    BaseResponse CreateCategory(CategoryEntity input);
    BaseResponse UpdateCategory(CategoryEntity input);
    BaseResponse Delete(CategoryEntity input);

}
