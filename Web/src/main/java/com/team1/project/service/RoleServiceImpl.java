package com.team1.project.service;

import com.team1.project.entity.RoleEntity;
import com.team1.project.repository.RoleRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class RoleServiceImpl implements RoleService{
    @Autowired
    RoleRepo roleRepo;

    @Override
    public int getRoleID(String roleName) {
        return roleRepo.findByName(roleName).getId();
    }

    @Override
    public RoleEntity getRoler(int RoleID) {
        if(RoleID > 0){
            return roleRepo.findById(RoleID).get();
        }
        return null;
    }
}
