package com.team1.project.service;

import com.team1.project.entity.ShippingEntity;
import com.team1.project.repository.ShippingRepo;
import org.springframework.data.domain.Page;

import java.util.List;

public interface ShippingService {
    
List<ShippingEntity> getAllShipping();
ShippingEntity editShipping(ShippingEntity shippingEntity);
ShippingEntity deleteShipping(int id);
ShippingEntity getshipById(int id);
ShippingEntity addShipping(ShippingEntity shippingEntity);
Page<ShippingEntity> findPaginated(int pageNo,int pageSize);
}
