package com.team1.project.service;

import com.team1.project.config.Constants;
import com.team1.project.config.Util;
import com.team1.project.entity.ShippingEntity;
import com.team1.project.repository.ShippingRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ShippingServiceImp implements ShippingService{
  @Autowired
    ShippingRepo shippingRepo;
    Constants constants;
    Util util=new Util();
    @Override
    public List<ShippingEntity> getAllShipping() {
        return shippingRepo.findAll();
    }

    @Override
    public ShippingEntity editShipping(ShippingEntity shippingEntity) {
            ShippingEntity shipper = getshipById(shippingEntity.getId());
            shipper.setDescription(shippingEntity.getDescription());
            shipper.setEmail(shippingEntity.getEmail());
            shipper.setLogo(shippingEntity.getLogo());
            shipper.setName(shippingEntity.getName());
            shipper.setPhone(shippingEntity.getPhone());
            shipper.setUpdatedat(util.timestampNow());
            return shippingRepo.save(shipper);
    }

    @Override
    public ShippingEntity deleteShipping(int id) {
        ShippingEntity sp = getshipById(id);
        sp.setIsactive(constants.NO_ACTIVE);
        sp.setDeletedat(util.timestampNow());
        return shippingRepo.save(sp);
    }

    @Override
    public ShippingEntity getshipById(int id) {
        return shippingRepo.findById(id).get();
    }

    @Override
    public ShippingEntity addShipping(ShippingEntity shippingEntity) {
        ShippingEntity shipper=new ShippingEntity();
        shipper.setDescription(shippingEntity.getDescription());
        shipper.setEmail(shippingEntity.getEmail());
        shipper.setLogo(shippingEntity.getLogo());
        shipper.setName(shippingEntity.getName());
        shipper.setPhone(shippingEntity.getPhone());
        shipper.setIsactive(constants.ACTIVE);
        shipper.setCreatedat(util.timestampNow());
        return shippingRepo.save(shipper);
    }

    @Override
    public Page<ShippingEntity> findPaginated(int pageNo, int pageSize) {
        Pageable pageable = PageRequest.of(pageNo-1,pageSize);
        return shippingRepo.findAllByIsactiveEquals(constants.ACTIVE,pageable);
    }
}
