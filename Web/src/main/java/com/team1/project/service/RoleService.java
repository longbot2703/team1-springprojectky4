package com.team1.project.service;

import com.team1.project.entity.RoleEntity;

public interface RoleService {
    int getRoleID(String roleName);
    RoleEntity getRoler(int RoleID);
}
