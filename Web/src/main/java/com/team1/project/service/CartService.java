package com.team1.project.service;

import com.team1.project.entity.CartEntity;
import com.team1.project.entity.OrderEntity;

import java.util.List;

public interface CartService {
    CartEntity createCart(int userid, int productid, int quantity);
    CartEntity deleteCart(int userid, int cartid);
    List<CartEntity> getListByUserid(int userid);
    List<CartEntity> getListByUseridAndSellerid(int userid,int sellerid);
    CartEntity findById(int id , int userid);
    int CountCart();
    List<CartEntity> saveAll(List<CartEntity> listCart);
}
