package com.team1.project.service;

import com.team1.project.dto.WishlistDto;
import com.team1.project.entity.WishlistEntity;
import org.springframework.data.domain.Page;

public interface WishlistService {
    Page<WishlistEntity> findPaginated(int pageNo, int pageSize);
    Page<WishlistEntity> getListWishlist(String search,int userid, int pageNo, int pageSize);
    WishlistEntity updateWishlist(WishlistDto input);
}
