package com.team1.project.service;

import com.team1.project.config.Constants;
import com.team1.project.config.Util;
import com.team1.project.entity.ProductEntity;
import com.team1.project.repository.ProductRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    ProductRepo productRepo;

    Util util = new Util();

    @Override
    public Page<ProductEntity> findPaginated(int pageNo, int pageSize, int userid) {
        Pageable pageable = PageRequest.of(pageNo - 1, pageSize);
        Page<ProductEntity> list = null;
        if (userid > 0) {
            list = productRepo.findAllByIsactiveAndSellerbyOrderByIdDesc(Constants.ACTIVE, userid, pageable);
        } else {
            list = productRepo.findAllByIsactiveOrderByIdDesc(Constants.ACTIVE, pageable);
        }
        return list;
    }

    @Override
    public Page<ProductEntity> findAllPageProduct(int cateid, String search, int pageNo) {
        Pageable pageable = PageRequest.of(pageNo - 1, Constants.PAGE_SIZE_WEB);
        if (cateid > 0) {
            return productRepo.findAllByNameContainingAndCateidAndIsactiveOrderByIdDesc(search, cateid, Constants.ACTIVE, pageable);
        } else {
            return productRepo.findAllByNameContainingAndIsactiveOrderByIdDesc(search, Constants.ACTIVE, pageable);
        }
    }

    @Override
    public ProductEntity findByID(int id) {
        return productRepo.findByIdAndIsactive(id, Constants.ACTIVE);
    }

    @Override
    public List<ProductEntity> findByCreateBy(int userID) {
        return productRepo.findAllBySellerbyAndIsactiveOrderByIdDesc(userID, Constants.ACTIVE);
    }

    @Override
    public ProductEntity addProduct(ProductEntity product, int businessID) {
        ProductEntity newProduct = new ProductEntity();

        newProduct.setName(product.getName());
        newProduct.setDescription(product.getDescription());
        newProduct.setQuantity(product.getQuantity());
        newProduct.setUnit(product.getUnit());
        newProduct.setPrice(product.getPrice());
        newProduct.setImage1(product.getImage1());
        newProduct.setImage2(product.getImage2());
        newProduct.setImage3(product.getImage3());
        newProduct.setImage4(product.getImage4());
        newProduct.setImage(product.getImage1());
        newProduct.setImagedetail(product.getImagedetail());
        newProduct.setIsactive(Constants.ACTIVE);
        newProduct.setCreateat(util.timestampNow());
        newProduct.setStatus(Constants.PRODUCT_AVAILABLE);
        newProduct.setCateid(product.getCateid());
        if (businessID > 0){
            newProduct.setSellerby(businessID);
        } else {
            newProduct.setSellerby(product.getSellerby());
        }
        newProduct.setDetail(product.getDetail());

        return productRepo.save(newProduct);
    }

    @Override
    public ProductEntity editProduct(ProductEntity product) {
        ProductEntity productUpdate = productRepo.getById(product.getId());;

        productUpdate.setName(product.getName());
        productUpdate.setDescription(product.getDescription());
        productUpdate.setQuantity(product.getQuantity());
        productUpdate.setUnit(product.getUnit());
        productUpdate.setPrice(product.getPrice());
        productUpdate.setImage1(product.getImage1());
        productUpdate.setImage2(product.getImage2());
        productUpdate.setImage3(product.getImage3());
        productUpdate.setImage4(product.getImage4());
        productUpdate.setImage(product.getImage1());
        productUpdate.setImagedetail(product.getImagedetail());
        productUpdate.setUpdateat(util.timestampNow());
        productUpdate.setCateid(product.getCateid());
        productUpdate.setDetail(product.getDetail());

        return productRepo.save(productUpdate);
    }

    @Override
    public ProductEntity deleteProduct(int id) {
        ProductEntity delProduct = productRepo.getById(id);
        delProduct.setIsactive(Constants.NO_ACTIVE);
        delProduct.setStatus(Constants.PRODUCT_NOT_AVAILABLE);
        delProduct.setDeleteat(util.timestampNow());

        return productRepo.save(delProduct);
    }

    @Override
    public List<ProductEntity> findNewProduct(String name) {
        return productRepo.findTop12ByIsactiveOrderByIdDesc(Constants.ACTIVE);
    }

    @Override
    public List<ProductEntity> findTop8ByCategory(int cateid, int productid) {
        return productRepo.findTop8ByCateidAndIsactiveAndIdNotOrderByIdDesc(cateid,Constants.ACTIVE,productid);
    }

    @Override
    public List<ProductEntity> get12NewProduct(String name) {
//        return productRepo.get12NewProductByName(Constants.ACTIVE, name);
        return null;
    }

    @Override
    public List<ProductEntity> saveAll(List<ProductEntity> listPro) {
        if(listPro != null){
            return productRepo.saveAll(listPro);
        }
        return null;
    }
}
