package com.team1.project.service;

import com.team1.project.config.Constants;
import com.team1.project.config.Util;
import com.team1.project.dto.OrderDto;
import com.team1.project.entity.OrderEntity;
import com.team1.project.entity.UserEntity;
import com.team1.project.repository.OrderRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.annotation.Transient;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {
    @Autowired
    UserService userService;

    @Autowired
    OrderRepo orderRepo;

    Util util = new Util();

    @Override
    @Transient
    public OrderEntity createOrder(OrderDto input, int active) {
        if (input.getBuyerid() > 0 ) {

            OrderEntity order = new OrderEntity();

            order.setFullname(input.getFullname());
            order.setPhone(input.getPhone());
            order.setAddress(input.getAddress());
            order.setNote(input.getNote());
            order.setEmail(input.getEmail());

            order.setCode("ORD"+ input.getBuyerid() + util.randomAlphaNumeric(6));

            order.setBuyerid(input.getBuyerid());

            order.setStatus(Constants.ORDER_WAIT_CONFIRM);
            order.setDiscount(input.getDiscount());
            order.setTotalprice(input.getTotalPrice());

            long totalpricePayment = (long) (input.getTotalPrice() + input.getShip_fee() - input.getDiscount());
            order.setTotalPricePayment(totalpricePayment);

            order.setShip_fee(input.getShip_fee());
            order.setPaypal(false);
            order.setStatus_payment(false);
            order.setIsactive(active);
            order.setCreateat(util.timestampNow());

            return orderRepo.save(order);

        }
        return null;
    }


    @Override
    public Page<OrderEntity> listOrderByBuyerid(int buyerid, Pageable pageable) {
        return orderRepo.findAllByBuyeridAndIsactiveOrderByIdDesc(buyerid, Constants.ACTIVE, pageable);
    }

    @Override
    public Page<OrderEntity> listOrderByBuyeridAndStatus(int buyerid, int status, Pageable pageable) {
        if(status == Constants.ORDER_ALL){
            return orderRepo.findAllByBuyeridAndIsactiveOrderByIdDesc(buyerid, Constants.ACTIVE, pageable);
        }else {
            return orderRepo.findAllByBuyeridAndStatusAndIsactiveOrderByIdDesc(buyerid, status, Constants.ACTIVE, pageable);
        }
    }

    @Override
    @Transient
    public OrderEntity updateOrder(OrderEntity input) {
        try{
            if(input != null){
                return orderRepo.save(input);
            }
        }catch (Exception e){
            return null;
        }
        return null;
    }

    @Override
    public OrderEntity findById(int id) {
        return orderRepo.findByIdAndIsactive(id,Constants.ACTIVE);
    }

    @Override
    public OrderEntity getDetail(int id, int buyerid) {
        if(id > 0 && buyerid > 0){
            return orderRepo.findByIdAndBuyeridAndIsactive(id, buyerid, Constants.ACTIVE);
        }
        return null;
    }

    @Override
    public OrderEntity findOrderById(int id) {
        if(id > 0){
            return orderRepo.findById(id).get();
        }
        return null;
    }

    @Override
    public OrderEntity findOrderByCode(String code) {
        if(!code.isEmpty()){
            return orderRepo.findByCodeAndIsactive(code,Constants.ACTIVE);
        }
        return null;
    }

    @Override
    public Page<OrderEntity> listPageByArrID(List<Integer> listId,int status, Pageable pageable) {
        if(listId != null){
            if(status == Constants.ORDER_ALL){
                return orderRepo.listOrderByArrayId(listId,pageable);
            }else {
                return orderRepo.listOrderByStatusAndArrayId(listId, status, pageable);
            }
        }
        return null;
    }

    @Override
    public OrderEntity finAllbyOderCoder(String code) {
        if(!code.isEmpty()){
            return orderRepo.findByCode(code);
        }
        return null;
    }

}