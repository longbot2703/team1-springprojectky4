package com.team1.project.service;

import com.team1.project.entity.BusinessEntity;

public interface BusinessService {
    BusinessEntity createBusiness(int userID);
    BusinessEntity findByUserID(int userID);
}
