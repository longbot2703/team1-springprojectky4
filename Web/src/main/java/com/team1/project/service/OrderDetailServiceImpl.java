package com.team1.project.service;

import com.team1.project.config.Constants;
import com.team1.project.config.Util;
import com.team1.project.dto.CartDto;
import com.team1.project.dto.OrderDto;
import com.team1.project.entity.CartEntity;
import com.team1.project.entity.OrderDetailEntity;
import com.team1.project.entity.OrderEntity;
import com.team1.project.entity.ProductEntity;
import com.team1.project.repository.CartRepo;
import com.team1.project.repository.OrderDetailRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class OrderDetailServiceImpl implements OrderDetailService{
    @Autowired
    CartService cartService;

    @Autowired
    OrderDetailRepo orderDetailRepo;

    @Autowired
    CartRepo cartRepo;

    @Autowired
    OrderService orderService;

    @Autowired
    ProductService productService;

    Util util = new Util();

    @Override
    public boolean createOrderDetail(OrderDto input, int orderid, int isacive) {
        if(input.getBuyerid() > 0 ){
            List<OrderDetailEntity> orderDetailServiceList = new ArrayList<>();
            List<ProductEntity> productEntityList = new ArrayList<>();
            List<CartEntity> cartEntityList = new ArrayList<>();

            for (CartEntity cart : input.getListCart()){
                if(cart != null){
                    OrderDetailEntity orderDetailEntity = new OrderDetailEntity();
                    orderDetailEntity.setOrderid(orderid);
                    orderDetailEntity.setProductid(cart.getProductid());
                    orderDetailEntity.setSellerid(cart.getCartProduct().getBusiness().getUserid());
                    orderDetailEntity.setQuantity(cart.getQuantity());
                    orderDetailEntity.setDiscount(cart.getCartProduct().getDiscount());
                    orderDetailEntity.setPrice(cart.getCartProduct().getPrice());
                    orderDetailEntity.setIsactive(isacive);
                    orderDetailEntity.setCreateat(util.timestampNow());

                    if (isacive == Constants.ACTIVE){
                        // mua hàng bình thường khong paypal => xóa sp khỏi giỏ hàng
                        cart.setIsactive(Constants.NO_ACTIVE);
                        cart.setUpdateat(util.timestampNow());
                        cartEntityList.add(cart);

                        // cập nhập số lượng sp
                        ProductEntity pro = productService.findByID(cart.getProductid());
                        float quantity = pro.getQuantity() - cart.getQuantity();
                        if(quantity < 0) quantity = 0;
                        pro.setQuantity(quantity);
                        pro.setUpdateat(util.timestampNow());
                        productEntityList.add(pro);
                    }

                    orderDetailServiceList.add(orderDetailEntity);
                }
            }

            if(orderDetailServiceList != null){
                orderDetailRepo.saveAll(orderDetailServiceList);
                if(productEntityList != null){
                    productService.saveAll(productEntityList);
                }
                if(cartEntityList != null){
                    cartService.saveAll(cartEntityList);
                }
                return true;
            }
        }
        return false;
    }

    @Override
    public List<OrderDetailEntity> listByOrderid(int orderid) {
        return orderDetailRepo.findAllByOrderidAndIsactiveOrderByIdDesc(orderid,Constants.ACTIVE);
    }

    @Override
    public List<OrderDetailEntity> listAllByOrderid(int orderid) {
        return orderDetailRepo.findAllByOrderidOrderByIdDesc(orderid);
    }

    @Override
    public boolean updateList(List<OrderDetailEntity> list) {
        try{
            if(list != null){
                List<OrderDetailEntity> check = orderDetailRepo.saveAll(list);
                if(check != null){
                    return true;
                }
            }
            return false;
        }catch (Exception e){
            return false;
        }
    }

    @Override
    public Page<OrderDetailEntity> listPageOrderDetail(int sellerid, Pageable pageable) {
        if(sellerid > 0){

        }
        return null;
    }

    @Override
    public List<Integer> listOrderidBysellerid(int sellerid) {
        if(sellerid > 0){
            return orderDetailRepo.findDistinctBySellerID(sellerid);
        }
        return null;
    }

    @Override
    public List<Integer> listAll() {
        return orderDetailRepo.findDistinct();
    }

    @Override
    public List<OrderDetailEntity> listByOrderidAndSellerIdAndStatus(int orderid,int sellerid,int status) {
        if(sellerid > 0){
            return orderDetailRepo.findAllByOrderidAndSelleridAndStatusAndIsactive(orderid,sellerid,status,Constants.ACTIVE);
        }
        return null;
    }

    @Override
    public List<OrderDetailEntity> listBySellerIdAndOrderid(int sellerid, int orderid) {
        if(sellerid > 0 && orderid > 0){
            return orderDetailRepo.findAllByOrderidAndSelleridAndIsactiveOrderByIdDesc(orderid,sellerid,Constants.ACTIVE);
        }
        return null;
    }

    @Override
    public List<OrderDetailEntity> listByOrderidAndStatusNot(int orderid, int status) {
        if( orderid > 0){
            return orderDetailRepo.findAllByOrderidAndStatusNotAndIsactive(orderid,status,Constants.ACTIVE);
        }
        return null;
    }

    @Override
    public List<OrderDetailEntity> listByOrderidandStatus(int orderid, int status) {
        if( orderid > 0){
            return orderDetailRepo.findAllByOrderidAndStatusAndIsactive(orderid,status,Constants.ACTIVE);
        }
        return null;
    }

    @Override
    public OrderDetailEntity findById(int id) {
        return orderDetailRepo.findByIdAndIsactive(id,Constants.ACTIVE);
    }

    @Override
    public OrderDetailEntity updateOrderDetail(OrderDetailEntity orderDetail) {
        try{
            if(orderDetail != null){
                return orderDetailRepo.save(orderDetail);
            }
            return null;
        }catch (Exception e){
            return null;
        }
    }
}
