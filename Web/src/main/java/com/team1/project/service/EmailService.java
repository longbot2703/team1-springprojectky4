package com.team1.project.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

@Component
public class EmailService {
    @Autowired
    private JavaMailSender emailSender;

    public boolean sendSimpleMessage(String toEmail, String subject, String text) {

        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("NongNghiepSach");
        message.setTo(toEmail);
        message.setSubject(subject);
        message.setText(text);

        emailSender.send(message);
        return true;
    }
}
