package com.team1.project.service;

import com.team1.project.dto.OrderDto;
import com.team1.project.entity.OrderDetailEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface OrderDetailService {
    boolean createOrderDetail(OrderDto input, int orderid,int isactive);
    List<OrderDetailEntity> listByOrderid(int orderid);
    List<OrderDetailEntity> listAllByOrderid(int orderid);
    boolean updateList(List<OrderDetailEntity> list);
    Page<OrderDetailEntity> listPageOrderDetail(int sellerid, Pageable pageable);
    List<Integer> listOrderidBysellerid(int sellerid);
    List<Integer> listAll();
    List<OrderDetailEntity> listByOrderidAndSellerIdAndStatus(int orderid,int sellerid,int status);
    List<OrderDetailEntity> listBySellerIdAndOrderid(int sellerid, int orderid);
    List<OrderDetailEntity> listByOrderidAndStatusNot(int orderid , int status);
    List<OrderDetailEntity> listByOrderidandStatus(int orderid , int status);
    OrderDetailEntity findById(int id);
    OrderDetailEntity updateOrderDetail(OrderDetailEntity orderDetail);
}
