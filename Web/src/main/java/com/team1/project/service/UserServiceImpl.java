package com.team1.project.service;

import com.team1.project.config.Constants;
import com.team1.project.config.Util;
import com.team1.project.dto.StatisticalDto;
import com.team1.project.dto.UserDto;
import com.team1.project.dto.RegisterDto;
import com.team1.project.entity.CustomerEntity;
import com.team1.project.entity.UserEntity;
import com.team1.project.repository.CustomerRepo;
import com.team1.project.repository.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class UserServiceImpl implements UserService {
    @Autowired
    UserRepo userRepo;

    @Autowired
    CustomerRepo customerRepo;

    @Autowired
    RoleService roleService;

    @Autowired
    BusinessService businessService;

    @Autowired
    CustomerService customerService;

    @Autowired
    PasswordEncoder passwordEncoder;


    Util util = new Util();


    public Page<UserEntity> findPaginated(int pageNo, int pageSize) {
        Pageable pageable = PageRequest.of(pageNo - 1, pageSize);
        return userRepo.findAll(pageable);
    }

    public Page<UserEntity> findPaginatedSeller(int pageNo, int pageSize, String username) {
        Pageable pageable = PageRequest.of(pageNo - 1, pageSize, Sort.by("createat"));
        if (username.isEmpty()) {
            return userRepo.findAllByRoleAndIsactive(Constants.ROLE_SELLER_ID, Constants.ACTIVE, pageable);
        } else {
            return userRepo.findAllByUsernameAndIsactiveAndRole(username, Constants.ACTIVE, Constants.ROLE_SELLER_ID, pageable);
        }
    }

    public UserEntity findUserByEmail(String email) {
        return userRepo.findByEmailAndIsactive(email, Constants.ACTIVE);
    }

    public UserEntity findSellerByUsername(String username) {
        return userRepo.findByEmailAndIsactive(username, Constants.ACTIVE);
    }

    public UserEntity createSeller(UserEntity seller) {
        UserEntity userEntity = userRepo.save(seller);
        if (userEntity != null) {
            businessService.createBusiness(userEntity.getId());
        }
        return userEntity;
    }

    @Override
    public UserEntity updateSeller(UserEntity seller) {
        businessService.findByUserID(seller.getId());
        return userRepo.save(seller);
    }

    @Override
    public UserEntity updateBuyer(UserEntity buyer) {
        return userRepo.save(buyer);
    }

    @Override
    public UserEntity deleteSeller(int id) {
        UserEntity userEntity = userRepo.findById(id).get();
        userEntity.setIsactive(Constants.NO_ACTIVE);
        userEntity.setUpdateat(util.timestampNow());
        return userRepo.save(userEntity);
    }

    public boolean findSellerByID(int id) {
        if (id > 0) {
            UserEntity user = userRepo.findByIdAndIsactive(id, Constants.ACTIVE);
            if (user != null) {
                return true;
            }
        }
        return false;
    }

    public UserEntity findUserByID(int id) {
        return userRepo.findByIdAndIsactive(id, Constants.ACTIVE);
    }

    public UserEntity findUserByUsername(String username, String roleName) {
        int roleid = roleService.getRoleID(roleName);
        return userRepo.findByUsernameAndRoleAndIsactive(username, roleid, Constants.ACTIVE);
    }

    @Override
    public UserDetails getCurentUser() {
        UserDetails auth = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return auth;
    }

    @Override
    public boolean updateCustomer(UserDto customer) {
        if (customer.getId() > 0) {
            UserEntity user = findUserByID(customer.getId());
            if (user != null) {
                CustomerEntity cus = customerService.getCusByUserID(user.getId());
                if (!customer.getCustomer().getJob().isEmpty()) cus.setJob(customer.getCustomer().getJob());
                if (!customer.getCustomer().getPhone().isEmpty()) cus.setPhone(customer.getCustomer().getPhone());

                customerService.update(cus);

                user.setSex(customer.getSex());
                if (!customer.getFullname().isEmpty()) user.setFullname(customer.getFullname());
                if (!customer.getEmail().isEmpty()) user.setEmail(customer.getEmail());
                if (!customer.getAddress().isEmpty()) user.setAddress(customer.getAddress());
                user.setUpdateat(util.timestampNow());

                userRepo.save(user);
                return true;
            }
        }
        return false;
    }

    @Override
    public UserEntity findUserByUsername(String username) {
        UserEntity user = userRepo.findByUsernameAndIsactive(username, Constants.ACTIVE);
        if(user!= null){
            user.setRoleUser(roleService.getRoler(user.getRole()));
        }
        return user;
    }

    @Override
    public UserEntity createBuyer(RegisterDto buyer) {

        UserEntity newbuyer = new UserEntity();

        newbuyer.setUsername(buyer.getUsername());
        newbuyer.setPassword(passwordEncoder.encode(buyer.getPassword()));
        newbuyer.setEmail(buyer.getEmail());
        newbuyer.setRole(roleService.getRoleID(Constants.ROLE_BUYER));
        newbuyer.setIsactive(Constants.ACTIVE);
        newbuyer.setCreateat(util.timestampNow());

        UserEntity user = userRepo.save(newbuyer);
        if (user.getId() > 0) {
            CustomerEntity cus = new CustomerEntity();
            cus.setUserid(user.getId());
            cus.setCreatedate(util.timestampNow());
            customerRepo.save(cus);
        }
        return user;
    }

    @Override
    public int findUseridBySession() {
        try{
            UserDetails data = this.getCurentUser();
            String name = data.getUsername();
            String role = data.getAuthorities().toArray()[0].toString();
            UserEntity user = this.findUserByUsername(name, role);
            if(user != null){
                return user.getId();
            }
            return 0;
        }catch (Exception e){
            return 0;
        }
    }

    @Override
    public boolean updateAddressUser(int id, String Address, String phone) {
        if(id>0){
            UserEntity user=findUserByID(id);
            if(user!=null){
                user.setAddress(Address);
                user.setUpdateat(util.timestampNow());
                userRepo.save(user);

                CustomerEntity customer = user.getCustomer();
                customer.setPhone(phone);
                customer.setUpdateat(util.timestampNow());
                customerRepo.save(customer);
                return true;
            }
        }
        return false;
    }

    @Override
    public List<StatisticalDto> statisticalSeller() {
        try{

            return userRepo.StatisticalBySeller();
        }catch (Exception ex){
            ex.toString();
            return null;
        }
    }

    @Override
    public List<UserEntity> findAllSeller() {
        return userRepo.findAllByRoleAndIsactive(Constants.ROLE_SELLER_ID,Constants.ACTIVE);
    }
}
