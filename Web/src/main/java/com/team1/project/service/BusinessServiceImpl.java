package com.team1.project.service;

import com.team1.project.entity.BusinessEntity;
import com.team1.project.repository.BusinessRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BusinessServiceImpl implements BusinessService{
    @Autowired
    BusinessRepo businessRepo;

    @Override
    public BusinessEntity createBusiness(int userID) {

        BusinessEntity newBusiness = new BusinessEntity();
        if(userID > 0) {
            newBusiness.setUserid(userID);
            BusinessEntity  business =   businessRepo.save(newBusiness);
            if(business.getId() >  0){
                return business;
            }
        }
        return newBusiness;
    }

    @Override
    public BusinessEntity findByUserID(int userID) {
        BusinessEntity businessEntity = businessRepo.findByUserid(userID);
        if(businessEntity != null){
            return  businessEntity;
        }else {
            BusinessEntity newBusiness = new BusinessEntity();
            newBusiness.setUserid(userID);
            return businessRepo.save(newBusiness);
        }
    }
}
