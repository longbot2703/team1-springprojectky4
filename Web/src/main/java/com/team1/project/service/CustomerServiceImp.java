package com.team1.project.service;

import com.team1.project.config.Constants;
import com.team1.project.config.Util;
import com.team1.project.entity.CustomerEntity;
import com.team1.project.entity.UserEntity;
import com.team1.project.repository.CustomerRepo;
import com.team1.project.repository.OrderDetailRepo;
import com.team1.project.repository.OrderRepo;
import com.team1.project.repository.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class CustomerServiceImp implements CustomerService {
@Autowired
    CustomerRepo customerRepo;
    UserRepo userRepo;
    Constants constants;
    Util util=new Util();

    public CustomerServiceImp(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    @Override
    public Page<UserEntity> findPaginated(int pageNo, int pageSize) {
        Pageable pageable = PageRequest.of(pageNo-1,pageSize);
        return userRepo.findAllByRoleAndIsactiveOrderByIdDesc(constants.ROLE_BUYER_ID, constants.ACTIVE,pageable);
    }

    @Override
    public CustomerEntity addCus(CustomerEntity cus) {
        CustomerEntity newCus=new CustomerEntity();
//        newCus.setName(cus.getName());
        newCus.setImgurl(cus.getImgurl());
//        newCus.setIsactive(1);
        newCus.setIsactive(Constants.ACTIVE);
        newCus.setCreatedate(util.timestampNow());
        newCus.setUserid(cus.getUserid());
        return customerRepo.save(newCus);
    }

    @Override
    public CustomerEntity editCus(CustomerEntity cus) {
        CustomerEntity customerEntity=getCusById(cus.getId());
//        customerEntity.setName(cus.getName());
        customerEntity.setImgurl(cus.getImgurl());
        customerEntity.setUpdateat(util.timestampNow());
        return customerRepo.save(customerEntity);
    }

    @Override
    public CustomerEntity deleteCus(int id) {
        CustomerEntity cus=getCusById(id);
        cus.setIsactive(Constants.NO_ACTIVE);
//        cus.setDeletedat(util.timestampNow());
        cus.setUpdateat(util.timestampNow());
        return customerRepo.save(cus);
    }

    @Override
    public CustomerEntity getCusById(int id) {
        return customerRepo.findById(id).get();
    }

    @Override
    public Page<CustomerEntity> getListCus(String search, int userid, int pageNo, int pageSize) {
        return null;
    }

    @Override
    public CustomerEntity getCusByUserID(int userid) {
        return customerRepo.findByUserid(userid);
    }

    @Override
    public CustomerEntity update(CustomerEntity cus) {
        cus.setUpdateat(util.timestampNow());
        return customerRepo.save(cus);
    }

    @Override
    public boolean checkPhone(int userid, String phone) {
        if(userid > 0){
            CustomerEntity cus = customerRepo.findByPhone(phone);
            if(cus != null && cus.getUserid() != userid){
                return true;
            }
        }
        return false;
    }
}
