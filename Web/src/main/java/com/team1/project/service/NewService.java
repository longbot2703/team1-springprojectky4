package com.team1.project.service;

import com.team1.project.entity.NewEntity;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;

public interface NewService {
    Page<NewEntity> findPaginated(int pageNo, int pageSize);
    NewEntity addNews(NewEntity news);
    NewEntity editNews(NewEntity news);
    NewEntity deleteNews(int id);
    NewEntity getNewsById(int id);
    List<NewEntity> listNews(int type);
    Page<NewEntity> getListNews(String searchTitle, int pageNo, int pageSize);

}
