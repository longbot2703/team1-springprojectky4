package com.team1.project.service;

import com.team1.project.entity.CustomerEntity;
import com.team1.project.entity.NewEntity;
import com.team1.project.entity.UserEntity;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

public interface CustomerService {
    Page<UserEntity> findPaginated(int pageNo, int pageSize);
    CustomerEntity addCus(CustomerEntity cus);
    CustomerEntity editCus(CustomerEntity cus);
    CustomerEntity deleteCus(int id);
    CustomerEntity getCusById(int id);
    Page<CustomerEntity> getListCus(String search,int userid, int pageNo, int pageSize);
    CustomerEntity getCusByUserID(int userid);
    CustomerEntity update (CustomerEntity cus);
    boolean checkPhone(int userid , String phone);
}
