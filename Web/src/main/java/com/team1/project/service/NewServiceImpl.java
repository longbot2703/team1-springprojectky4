package com.team1.project.service;

import com.team1.project.config.Constants;
import com.team1.project.config.Util;
import com.team1.project.entity.NewEntity;
import com.team1.project.repository.NewRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NewServiceImpl implements NewService{

    @Autowired
    NewRepository newRepository;
    Constants constants;

    Util util = new Util();

    @Override
    public Page<NewEntity> findPaginated(int pageNo, int pageSize) {
        Pageable pageable = PageRequest.of(pageNo-1,pageSize);
        return newRepository.findAllByIsactiveEquals(constants.ACTIVE,pageable);
    }

    @Override
    public NewEntity addNews(NewEntity news) {
        NewEntity newEntity = new NewEntity();
        newEntity.setTitle(news.getTitle());
        newEntity.setDescription(news.getDescription());
        newEntity.setContent(news.getContent());
        newEntity.setIsactive(1);
        newEntity.setCreatedat(util.timestampNow());
        newEntity.setType(news.getType());
        newEntity.setImageurl(news.getImageurl());
        return newRepository.save(newEntity);
    }

    @Override
    public NewEntity editNews(NewEntity news) {
        NewEntity newsUpdate = getNewsById(news.getId());
        newsUpdate.setTitle(news.getTitle());
        newsUpdate.setDescription(news.getDescription());
        newsUpdate.setContent(news.getContent());
        newsUpdate.setUpdatedat(util.timestampNow());
        newsUpdate.setType(news.getType());
        newsUpdate.setImageurl(news.getImageurl());
        return newRepository.save(newsUpdate);
    }

    @Override
    public NewEntity deleteNews(int id) {
        NewEntity newsDelete = getNewsById(id);
        newsDelete.setIsactive(0);
        newsDelete.setDeletedat(util.timestampNow());

        return newRepository.save(newsDelete);
    }

    @Override
    public NewEntity getNewsById(int id) {
        return newRepository.findById(id).get();
    }

    @Override
    public List<NewEntity> listNews(int type) {
        return newRepository.findAllByTypeAndIsactiveOrderByIdDesc(type, Constants.ACTIVE);
    }

    @Override
    public Page<NewEntity> getListNews(String searchTitle, int pageNo, int pageSize) {
        Pageable pageable = PageRequest.of(pageNo-1,pageSize);
        return newRepository.findAllByTitleContainingAndTypeAndIsactiveOrderByIdDesc(searchTitle,Constants.NEWS_TYPE,Constants.ACTIVE,pageable);
    }
}
