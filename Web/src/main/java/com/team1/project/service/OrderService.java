package com.team1.project.service;

import com.team1.project.dto.OrderDto;
import com.team1.project.entity.OrderEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface OrderService {
    OrderEntity createOrder(OrderDto input,int active);
    Page<OrderEntity> listOrderByBuyerid(int buyerid,Pageable pageable);
    Page<OrderEntity> listOrderByBuyeridAndStatus(int buyerid,int type,Pageable pageable);
    OrderEntity updateOrder(OrderEntity input);
    OrderEntity findById(int id);
    OrderEntity getDetail(int id, int buyerid);
    OrderEntity findOrderById(int id);
    OrderEntity findOrderByCode(String code);
    Page<OrderEntity> listPageByArrID(List<Integer> listId,int status,Pageable pageable);
    OrderEntity finAllbyOderCoder(String code);
}
