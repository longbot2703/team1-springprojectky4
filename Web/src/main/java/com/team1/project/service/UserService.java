package com.team1.project.service;


import com.team1.project.dto.StatisticalDto;
import com.team1.project.dto.UserDto;
import com.team1.project.dto.RegisterDto;
import com.team1.project.entity.UserEntity;
import org.springframework.data.domain.Page;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.List;

public interface UserService {
    Page<UserEntity> findPaginated(int pageNo, int pageSize);
    Page<UserEntity> findPaginatedSeller(int pageNo, int pageSize,String username);
    UserEntity findUserByEmail(String email);
    UserEntity findSellerByUsername(String username);
    UserEntity createSeller(UserEntity seller);
    UserEntity updateSeller(UserEntity seller);
    UserEntity updateBuyer(UserEntity buyer);
    UserEntity deleteSeller(int id);
    boolean findSellerByID(int id);
    UserEntity findUserByID(int id);
    UserEntity findUserByUsername(String username, String roleName);
    UserDetails getCurentUser();
    boolean updateCustomer(UserDto customer);
    UserEntity findUserByUsername(String username);
    UserEntity createBuyer(RegisterDto buyer);
    int findUseridBySession();
    boolean updateAddressUser(int id,String Addressm, String phone);
    List<StatisticalDto> statisticalSeller();
    List<UserEntity> findAllSeller();

}
