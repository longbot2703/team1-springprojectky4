package com.team1.project.service;

import com.team1.project.config.Constants;
import com.team1.project.entity.CategoryEntity;
import com.team1.project.model.BaseResponse;
import com.team1.project.repository.CategoryRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService{

    @Autowired
    CategoryRepo categoryRepo;


    @Override
    public List<CategoryEntity> fillAll() {
        return categoryRepo.findAllByIsactiveOrderByIdDesc(Constants.ACTIVE);

    }

    @Override
    public List<CategoryEntity> getAllCategory() {
        return categoryRepo.getAllCategory(Constants.ACTIVE);
    }

    @Override
    public List<CategoryEntity> findAllByNameAndIsactive(String name, int isactive) {
        return categoryRepo.findAllByNameAndIsactive(name, Constants.ACTIVE);
    }

    public List<CategoryEntity> GetAll() {
        return categoryRepo.findAll();
    }

    @Override
    public BaseResponse CreateCategory(CategoryEntity input) {
        BaseResponse res = new BaseResponse();
        try {
            categoryRepo.save(input);
            res.message = "Thêm mới thành công";
            res.status = 1;
            return res;
        } catch (Exception ex) {
            res.message = "Có lỗi cảy ra với mã lỗi: " + ex.toString();
            return res;
        }
    }

    @Override
    public BaseResponse UpdateCategory(CategoryEntity input) {
        BaseResponse res = new BaseResponse();
        try {
            input.setIsactive(1);
            categoryRepo.save(input);
            res.message = "Cập nhật thành công";
            res.status = 1;
            return res;
        } catch (Exception ex) {
            res.message = "Có lỗi cảy ra với mã lỗi: " + ex.toString();
            return res;
        }
    }

    @Override
    public BaseResponse Delete(CategoryEntity input) {
        BaseResponse res = new BaseResponse();
        try {
            categoryRepo.save(input);
            res.message = "Xóa thành công";
            res.status = 1;
            return res;
        } catch (Exception ex) {
            res.message = "Có lỗi cảy ra với mã lỗi: " + ex.toString();
            return res;
        }
    }
}
