package com.team1.project.service;

import com.team1.project.config.Constants;
import com.team1.project.config.Util;
import com.team1.project.entity.CartEntity;
import com.team1.project.entity.OrderDetailEntity;
import com.team1.project.entity.OrderEntity;
import com.team1.project.repository.CartRepo;
import com.team1.project.repository.OrderDetailRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CartServiceImpl implements CartService{
    @Autowired
    CartRepo cartRepo;

    @Autowired
    UserService userService;

    Util util = new Util();

    @Override
    public CartEntity createCart(int userid, int productid, int quantity) {
        CartEntity cart = cartRepo.findByUseridAndProductid(userid,productid);

        if(cart != null){
            if(cart.getIsactive() == Constants.ACTIVE){
                cart.setQuantity(cart.getQuantity() + quantity);
                cart.setUpdateat(util.timestampNow());
                return cartRepo.save(cart);
            }else {
                cart.setQuantity(quantity);
                cart.setUpdateat(util.timestampNow());
                cart.setIsactive(Constants.ACTIVE);
                return  cartRepo.save(cart);
            }
        }else {
           return cartRepo.save(new CartEntity(productid,userid,quantity,Constants.ACTIVE));
        }
    }

    @Override
    public CartEntity deleteCart(int userid, int cartid) {
        CartEntity cart = cartRepo.findByIdAndIsactive(cartid,Constants.ACTIVE);
        if(cart != null){
            if(cart.getUserid() == userid){
                cart.setIsactive(Constants.NO_ACTIVE);
                cart.setUpdateat(util.timestampNow());
                return cartRepo.save(cart);
            }
        }
        return null;
    }

    @Override
    public List<CartEntity> getListByUserid(int userid) {
        return cartRepo.findAllByUseridAndIsactiveOrderByIdDesc(userid,Constants.ACTIVE);
    }

    @Override
    public List<CartEntity> getListByUseridAndSellerid(int userid, int sellerid) {
        return null;
    }

    @Override
    public CartEntity findById(int id, int userid) {
        return cartRepo.findByIdAndUseridAndIsactive(id,userid,Constants.ACTIVE);
    }

    @Override
    public int CountCart() {
        try{
            int userId = userService.findUseridBySession();
            if(userId > 0){
                List<CartEntity> list = this.getListByUserid(userId);
                int cout=0;
                for (CartEntity item:list) {
                    cout+=item.getQuantity();
                }
                return cout;
            }
            return 0;
        }catch (Exception e){
            return 0;
        }
    }

    @Override
    public List<CartEntity> saveAll(List<CartEntity> listCart) {
        try{
            if(listCart != null){
                return cartRepo.saveAll(listCart);
            }
        }catch (Exception e){
            return null;
        }
        return null;
    }
}
