package com.team1.project.repository;

import com.team1.project.entity.FeedbackEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FeedbackRepo extends JpaRepository<FeedbackEntity,Integer> {
}
