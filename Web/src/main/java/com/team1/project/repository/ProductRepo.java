package com.team1.project.repository;

import com.team1.project.entity.ProductEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ProductRepo extends JpaRepository<ProductEntity,Integer> {
    Page<ProductEntity> findAllByNameContainingAndIsactiveOrderByIdDesc(String productname, int isactive, Pageable pageable);
    Page<ProductEntity> findAllByNameContainingAndCateidAndIsactiveOrderByIdDesc(String productname, int cateid,int isactive, Pageable pageable);
    ProductEntity findByIdAndIsactive(int id, int isactive);
    List<ProductEntity> findAllBySellerbyAndIsactiveOrderByIdDesc(int userid, int isactive);
    List<ProductEntity> findTop12ByNameContainingAndIsactiveOrderById(String name, int isactive);
    Page<ProductEntity> findAllByIsactiveOrderByIdDesc(int isactive, Pageable pageable);
    Page<ProductEntity> findAllByIsactiveAndSellerbyOrderByIdDesc(int isactive, int userid, Pageable pageable);
    List<ProductEntity> findTop8ByCateidAndIsactiveAndIdNotOrderByIdDesc( int cateid, int active,int productid);
    List<ProductEntity> findTop12ByIsactiveOrderByIdDesc(int isactive);


}
