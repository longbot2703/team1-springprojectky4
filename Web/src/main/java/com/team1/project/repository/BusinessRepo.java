package com.team1.project.repository;

import com.team1.project.entity.BusinessEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BusinessRepo extends JpaRepository<BusinessEntity, Integer> {
    BusinessEntity findByUserid(int userid);
}
