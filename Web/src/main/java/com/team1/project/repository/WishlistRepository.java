package com.team1.project.repository;

import com.team1.project.entity.WishlistEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WishlistRepository extends JpaRepository<WishlistEntity, Integer> {
    Page<WishlistEntity> findAllByIsactiveEquals(int i, Pageable pageable);
    Page<WishlistEntity> findAllByUseridAndIsactive(int userid, int isactive, Pageable pageable);
    WishlistEntity findByUseridAndProducid(int userid, int productid);
}
