package com.team1.project.repository;

import com.team1.project.entity.OrderEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface OrderRepo extends JpaRepository<OrderEntity,Integer> {
    Page<OrderEntity> findAllByBuyeridAndIsactiveOrderByIdDesc(int buyerid, int isactive, Pageable pageable);
    Page<OrderEntity> findAllByBuyeridAndStatusAndIsactiveOrderByIdDesc(int buyerid,int status, int isactive, Pageable pageable);
    OrderEntity findByIdAndIsactive(int id, int isactive);
    OrderEntity findByIdAndBuyeridAndIsactive(int id, int buyerid, int isactive);
    List<OrderEntity> findAllByStatusAndIsactive(int status, int isactive);
    OrderEntity findByCodeAndIsactive(String code,int isactive);
    OrderEntity findByCode(String code);


    @Query(value = "SELECT sum(totalPricePayment) FROM OrderEntity WHERE status = 2")
    public Integer sumTotalPricePayment();

    @Query(value = "select SUM(total_price_payment) from orders where status = 2 and month(createat) = ?1", nativeQuery = true)
    public Integer sumTotalPricePaymentByMonth(int i);

    // quản lý đơn hàng
    @Query(value = "SELECT o from OrderEntity o where o.isactive = 1 and o.id in :ids order by o.id desc ")
    Page<OrderEntity> listOrderByArrayId(@Param("ids") List<Integer> listId,Pageable pageable);

    @Query(value = "SELECT o from OrderEntity o where o.isactive = 1 and o.status = :status and o.id in :ids order by o.id desc ")
    Page<OrderEntity> listOrderByStatusAndArrayId(@Param("ids") List<Integer> listId,
                                                  @Param("status") int status,
                                                  Pageable pageable);
}
