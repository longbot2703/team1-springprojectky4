package com.team1.project.repository;

import com.team1.project.entity.OrderDetailEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface OrderDetailRepo extends JpaRepository<OrderDetailEntity,Integer> {
    OrderDetailEntity findByOrderidAndProductidAndIsactive(int orderId, int productid, int isactive);
    List<OrderDetailEntity> findAllByOrderidAndIsactiveOrderByIdDesc(int orderid, int isactive);
    List<OrderDetailEntity> findAllByOrderidOrderByIdDesc(int orderid);
    List<OrderDetailEntity> findDistinctBySelleridAndIsactiveOrderByIdDesc(int sellerid,int isactive);
    List<OrderDetailEntity> findAllByOrderidAndSelleridAndStatusAndIsactive(int orderid, int sellerid , int status, int isactive);
    List<OrderDetailEntity> findAllByOrderidAndSelleridAndIsactiveOrderByIdDesc(int orderid, int sellerid, int isactive);
    List<OrderDetailEntity> findAllBySelleridAndIsactiveAndStatus(int sellerid, int isactive, int status);
    List<OrderDetailEntity> findAllByIsactiveAndStatusAndSellerid(int isactive, int status, int userid);
    OrderDetailEntity findByIdAndIsactive(int id, int isactive);

//    @Query(value = "select o from OrderDetailEntity  o where o.orderid = :orderid and o.status <> :status and o.isactive = :isactive")
    List<OrderDetailEntity> findAllByOrderidAndStatusNotAndIsactive( int orderid, int status, int isactive);
    List<OrderDetailEntity> findAllByOrderidAndStatusAndIsactive( int orderid, int status, int isactive);

    @Query(value = "SELECT DISTINCT o.orderid from OrderDetailEntity o where o.isactive = 1 and o.sellerid = :sellerid")
    List<Integer> findDistinctBySellerID(@Param("sellerid") int sellerid);

    @Query(value = "SELECT DISTINCT o.orderid from OrderDetailEntity o where o.isactive = 1")
    List<Integer> findDistinct();

    @Query(value = "SELECT sum(price) FROM OrderDetailEntity WHERE status = 2 and sellerid= ?1")
    public Integer sumTotalPrice(int id);

    @Query(value = "select SUM(price) from order_detail where status = 2 and month(createat) = ?1 and sellerid = ?2", nativeQuery = true)
    public Integer sumSellerTotalPricePaymentByMonth(int i, int id);
}
