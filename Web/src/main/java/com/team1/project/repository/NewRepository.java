package com.team1.project.repository;

import com.team1.project.entity.NewEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface NewRepository extends JpaRepository<NewEntity, Integer> {
    Page<NewEntity> findAllByIsactiveEquals(int i, Pageable pageable);
    Page<NewEntity> findAllByTitleContainingAndTypeAndIsactiveOrderByIdDesc(String searchTitle, int type, int isactive,Pageable pageable);
    List<NewEntity> findAllByTypeAndIsactiveOrderByIdDesc(int type, int isactive);
}
