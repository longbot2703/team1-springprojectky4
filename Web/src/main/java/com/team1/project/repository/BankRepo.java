package com.team1.project.repository;

import com.team1.project.entity.BankEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BankRepo extends JpaRepository<BankEntity,Integer> {
}
