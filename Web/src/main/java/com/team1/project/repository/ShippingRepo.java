package com.team1.project.repository;

import com.team1.project.entity.CustomerEntity;
import com.team1.project.entity.ShippingEntity;
import com.team1.project.entity.UserEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ShippingRepo extends JpaRepository<ShippingEntity,Integer> {
    Page<ShippingEntity> findAllByIsactiveEquals(int i, Pageable pageable);
}
