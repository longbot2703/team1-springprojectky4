package com.team1.project.repository;

import com.team1.project.entity.RoleEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepo extends JpaRepository<RoleEntity, Integer> {
    RoleEntity findByName(String name);
}