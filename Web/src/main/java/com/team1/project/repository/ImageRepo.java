package com.team1.project.repository;

import com.team1.project.entity.ImageEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ImageRepo extends JpaRepository<ImageEntity,Integer> {
}
