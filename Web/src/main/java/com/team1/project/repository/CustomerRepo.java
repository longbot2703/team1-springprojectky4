package com.team1.project.repository;

import com.team1.project.entity.CustomerEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;


public interface CustomerRepo extends JpaRepository<CustomerEntity, Integer> {
    Page<CustomerEntity> findAllByIsactiveEquals(int i, Pageable pageable);
    CustomerEntity findByUserid(int userid);
    CustomerEntity findByPhone(String phone);
}
