package com.team1.project.repository;

import com.team1.project.entity.CartEntity;
import com.team1.project.entity.OrderEntity;
import com.team1.project.entity.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CartRepo extends JpaRepository<CartEntity,Integer> {
    CartEntity findByUseridAndProductid(int userid, int productid);
    List<CartEntity> findAllByUseridAndIsactiveOrderByIdDesc(int userid, int isactive);
    CartEntity findByIdAndIsactive(int id, int productid);
    List<CartEntity> findAllByUseridAndCartProductAndIsactive(int userid, ProductEntity cartProduct ,int isActive);
    CartEntity findByIdAndUseridAndIsactive(int id, int userid, int isactive);
}
