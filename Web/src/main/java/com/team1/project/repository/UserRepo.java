package com.team1.project.repository;

import com.team1.project.dto.StatisticalDto;
import com.team1.project.entity.UserEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface UserRepo extends JpaRepository<UserEntity,Integer> {
    UserEntity findByUsername(String username);
    UserEntity findByEmailAndIsactive(String email, int isactive);
    UserEntity findByUsernameAndIsactive(String username, int isactive);
    UserEntity findByIdAndIsactive(int id , int isactive);
    UserEntity findByUsernameAndRoleAndIsactive(String username, int role , int isactive);
    Page<UserEntity> findAllByUsernameAndIsactiveAndRole(String username, int isactive , int Role,  Pageable pageable);
    Page<UserEntity> findAllByRoleAndIsactive (int Role, int isactive , Pageable pageable);
    UserEntity findByTokenAndRoleAndIsactive(String token, int role,int isactive);
    List<UserEntity> findAllByRoleAndIsactive(int Role, int active);
    Page<UserEntity> findAllByRoleAndIsactiveOrderByIdDesc(int role, int active, Pageable pageable);

    @Query(value = "select u.username from user u", nativeQuery = true)
    List<StatisticalDto> StatisticalBySeller();
}
