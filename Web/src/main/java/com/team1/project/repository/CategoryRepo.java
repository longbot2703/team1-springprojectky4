package com.team1.project.repository;

import com.team1.project.entity.CategoryEntity;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CategoryRepo extends JpaRepository<CategoryEntity,Integer> {
    List<CategoryEntity> findAllByIsactiveOrderByIdDesc(int isactive);
    @Query("select c from CategoryEntity c where c.isactive = :isactive and c.name like %:name% order by c.id desc")
    List<CategoryEntity>findAllByNameAndIsactive(String name, int isactive);
    @Query("select c from CategoryEntity c where c.isactive = :isactive order by c.id desc")
    List<CategoryEntity> getAllCategory(@Param("isactive") int isactive);
}
