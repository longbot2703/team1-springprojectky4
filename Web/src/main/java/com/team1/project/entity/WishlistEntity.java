package com.team1.project.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.sql.Timestamp;

@Data
@Entity
@Table(name = "wishlist")
public class WishlistEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "userid")
    private int userid;

    @Column(name = "productid")
    private int producid;

    @JsonIgnore
    @Column(name = "isactive",nullable = false)
    @ColumnDefault("1")
    private int isactive;

    @JsonIgnore
    @Column(name = "createat")
    private Timestamp createat;

    @JsonIgnore
    @Column(name = "updateat")
    private Timestamp updateat;

    @JsonIgnore
    @ManyToOne() //EAGER
    @JoinColumn(name = "userid", insertable = false, updatable = false)
    private UserEntity userWishlist;

    @JsonIgnore
    @ManyToOne() //EAGER
    @JoinColumn(name = "productid", insertable = false, updatable = false)
    private ProductEntity productWishlist;


    public WishlistEntity() {
    }

    public WishlistEntity(int userid, int producid, int isactive, Timestamp createat) {
        this.userid = userid;
        this.producid = producid;
        this.isactive = isactive;
        this.createat = createat;
    }
}
