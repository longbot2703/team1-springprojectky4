package com.team1.project.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.sql.Timestamp;

@Data
@Entity
@Table(name = "freedback")
public class FeedbackEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "title")
    private String title;

    @Column(name = "description")
    private String description;

    @Column(name = "producid",nullable = false)
    private int productid;

    @Column(name = "status")
    @ColumnDefault("0") //  chua phan hoi
    private int status;

    @Column(name = "createby",nullable = false) // tạo bởi ai (user id )
    private int createby;

    @Column(name = "confirmby") // người phản hổi lại
    private int confirmby;

    @Column(name = "receiverby") // phản hồi ai (sản phẩm của ai)
    private int receiverby;

    @JsonIgnore
    @Column(name = "isactive",nullable = false)
    @ColumnDefault("1")
    private int isactive;

    @JsonIgnore
    @Column(name = "createat")
    private Timestamp createat;

    @JsonIgnore
    @Column(name = "updateat")
    private Timestamp updateat;

    @ManyToOne() //EAGER
    @JoinColumn(name = "producid", insertable = false, updatable = false)
    private ProductEntity productFreedBack;
    
    @ManyToOne() //EAGER
    @JoinColumn(name = "createby", insertable = false, updatable = false)
    private UserEntity createFreedBack;

    @ManyToOne() //EAGER
    @JoinColumn(name = "confirmby", insertable = false, updatable = false)
    private UserEntity confirmFreedBack;

    @ManyToOne() //EAGER
    @JoinColumn(name = "receiverby", insertable = false, updatable = false)
    private UserEntity receiverFreedBack;
}
