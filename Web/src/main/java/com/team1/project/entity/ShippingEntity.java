package com.team1.project.entity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "shipping")
public class ShippingEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name",nullable = false)
    private String name;

    @Column(name = "phone")
    private String phone;

    @Column(name = "email")
    private String email;

    @Column(name="description")
    private String description;

    @Column(name = "logo")
    private String logo;

    @JsonIgnore
    @Column(name = "isactive",nullable = false)
    private int isactive;

    @Column(name = "createdat")
    private Timestamp createdat;

    @Column(name = "updatedat")
    private Timestamp updatedat;

    @Column(name = "deletedat")
    private Timestamp deletedat;

}
