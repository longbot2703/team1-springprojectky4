package com.team1.project.entity;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.sql.Timestamp;

@Entity
@Table(name = "new")
public class NewEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotEmpty(message = "Title must be required")
    @Column(name = "title")
    private String title;

    @NotEmpty(message = "Description must be required")
    @Column(name = "description")
    private String description;

    @NotEmpty(message = "Content must be required")
    @Column(name = "content")
    private String content;

    @Column(name = "imageurl")
    private String imageurl;

    @Column(name = "type")
    private int type;

    @Column(name = "isactive")
    private int isactive;

    @Column(name = "createdat")
    private Timestamp createdat;

    @Column(name = "updatedat")
    private Timestamp updatedat;

    @Column(name = "deletedat")
    private Timestamp deletedat;

    public NewEntity() {
    }

    public NewEntity(int id, String title, String content, String imageurl) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.imageurl = imageurl;
    }

    @Transient
    public String getNewsImage() {
        if (imageurl == null) return null;
        return "/img/news/"+ imageurl;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImageurl() {
        return imageurl;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getIsactive() {
        return isactive;
    }

    public void setIsactive(int isactive) {
        this.isactive = isactive;
    }

    public Timestamp getCreatedat() {
        return createdat;
    }

    public void setCreatedat(Timestamp createdat) {
        this.createdat = createdat;
    }

    public Timestamp getUpdatedat() {
        return updatedat;
    }

    public void setUpdatedat(Timestamp updatedat) {
        this.updatedat = updatedat;
    }

    public Timestamp getDeletedat() {
        return deletedat;
    }

    public void setDeletedat(Timestamp deletedat) {
        this.deletedat = deletedat;
    }
}
