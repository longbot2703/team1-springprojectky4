package com.team1.project.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "business")
public class BusinessEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "taxcode")
    private String taxcode;

    @Column(name = "representative")
    private String representative;

    @Column(name = "phone")
    private String phone;

    @Column(name = "fax")
    private String fax;

    @Column(name = "bankname")
    private String bankname;

    @Column(name = "banknumber")
    private String banknumber;

    @Column(name = "bankaccount")
    private String bankaccount;

    @ColumnDefault("1")
    @Column(name = "rateting")
    private int rateting;

    @ColumnDefault("0")
    @Column(name = "longitude")
    private float longitude;

    @ColumnDefault("0")
    @Column(name = "latitude")
    private float latitude;

    @Column(name = "userid")
    private int userid;

    @OneToOne()
    @JoinColumn(name = "userid",insertable = false, updatable = false)
    private UserEntity user;

    @JsonIgnore
    @OneToMany(mappedBy = "business")
    private List<ProductEntity> listProduct;
}
