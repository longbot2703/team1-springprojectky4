package com.team1.project.entity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "customer")
public class CustomerEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "phone")
    private String phone;

    @Column(name = "job")
    private String job;

    @Column(name = "imgurl",nullable = false)
    @ColumnDefault("/images/logo.jpg")
    private String imgurl;

    @JsonIgnore
    @Column(name = "isactive",nullable = false)
    @ColumnDefault("1")
    private int isactive;

    @JsonIgnore
    @Column(name = "createdate")
    private Timestamp createdate;

    @JsonIgnore
    @Column(name = "updateat")
    private Timestamp updateat;

    @Column(name = "userid")
    private int userid;

    @JsonIgnore
    @OneToOne() //EAGER
    @JoinColumn(name = "userid", insertable = false, updatable = false)
    private UserEntity userCustomer;

    public CustomerEntity() {
    }

    public CustomerEntity(int id, String phone, String job, String imgurl, int isactive, Timestamp createdate, Timestamp updateat) {
        this.id = id;
        this.phone = phone;
        this.job = job;
        this.imgurl = imgurl;
        this.isactive = isactive;
        this.createdate = createdate;
        this.updateat = updateat;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getImgurl() {
        return imgurl;
    }

    public void setImgurl(String imgurl) {
        this.imgurl = imgurl;
    }

    public int getIsactive() {
        return isactive;
    }

    public void setIsactive(int isactive) {
        this.isactive = isactive;
    }

    public Timestamp getCreatedate() {
        return createdate;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public void setCreatedate(Timestamp createdate) {
        this.createdate = createdate;
    }

    public Timestamp getUpdateat() {
        return updateat;
    }

    public void setUpdateat(Timestamp updateat) {
        this.updateat = updateat;
    }


    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public UserEntity getUserCustomer() {
        return userCustomer;
    }

    public void setUserCustomer(UserEntity userCustomer) {
        this.userCustomer = userCustomer;
    }
}
