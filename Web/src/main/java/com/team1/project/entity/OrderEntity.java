package com.team1.project.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@Data
@Entity
@Table(name = "orders")
public class OrderEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "code")
    private String code;

    @Column(name = "note")
    private String note;

    @Column(name = "fullname")
    private String fullname;

    @Column(name = "address")
    private String address;

    @Column(name = "phone")
    private String phone;

    @Column(name = "email")
    private String email;


    @Column(name = "totalprice",nullable = false)
    private long totalprice;

    @Column(name = "total_price_payment",nullable = false)
    private long totalPricePayment;

    @Column(name = "discount",nullable = false)
    @ColumnDefault("0")
    private float discount;

    @Column(name = "ship_fee",nullable = false)
    @ColumnDefault("0")
    private int ship_fee;

    @Column(name = "status",nullable = false)
    @ColumnDefault("0")
    private int status;

    @Column(name = "paypal",nullable = false)
    @ColumnDefault("false")
    private boolean paypal; // check người dùng thanh toán paypal hay thanh toán trực tiếp : true : thanh toán  paypal

    @Column(name = "status_payment",nullable = false)
    @ColumnDefault("false")
    private boolean status_payment; // true đã thanh toán thành công , false chưa thanh toán

    @Column(name = "buyerid", nullable = false) // id user nguoi mua
    private int buyerid;

    @JsonIgnore
    @Column(name = "isactive",nullable = false)
    @ColumnDefault("1")
    private int isactive;

    @JsonIgnore
    @Column(name = "createat")
    private Timestamp createat;

    @JsonIgnore
    @Column(name = "updateat")
    private Timestamp updateat;

    @ManyToOne() //EAGER
    @JoinColumn(name = "buyerid", insertable = false, updatable = false)
    private UserEntity buyer;

    @JsonIgnore
    @OneToMany(mappedBy = "orderDetail")
    private List<OrderDetailEntity> orderDetail;

}
