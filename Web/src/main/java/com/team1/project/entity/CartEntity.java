package com.team1.project.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.team1.project.config.Util;
import lombok.Data;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.sql.Timestamp;

@Data
@Entity
@Table(name = "cart")
public class CartEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "productid",nullable = false)
    private int productid;

    @Column(name = "userid",nullable = false)
    private int userid;

    @ColumnDefault("0")
    @Column(name = "quantity",nullable = false)
    private int quantity;

    @JsonIgnore
    @ColumnDefault("1")
    @Column(name = "isactive",nullable = false)
    private int isactive;

    @JsonIgnore
    @Column(name = "createat")
    private Timestamp createat;

    @JsonIgnore
    @Column(name = "updateat")
    private Timestamp updateat;

    @ManyToOne() //EAGER
    @JoinColumn(name = "userid", insertable = false, updatable = false)
    private UserEntity cartUser;

    @ManyToOne() //EAGER
    @JoinColumn(name = "productid", insertable = false, updatable = false)
    private ProductEntity cartProduct;

    public CartEntity() {
    }

    public CartEntity(int productid, int userid, int quantity, int isactive) {
        this.productid = productid;
        this.userid = userid;
        this.quantity = quantity;
        this.isactive = isactive;
        this.createat = new Util().timestampNow();
    }
}
