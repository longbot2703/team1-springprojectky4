package com.team1.project.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.sql.Timestamp;

@Data
@Entity
@Table(name = "order_detail")
public class OrderDetailEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "orderid")
    private int orderid;

    @Column(name = "productid",nullable = false)
    private int productid;

    @Column(name = "sellerid",nullable = false)
    private int sellerid;

    @Column(name = "price",nullable = false)
    @ColumnDefault("0")
    private long price;

    @Column(name = "discount",nullable = false)
    @ColumnDefault("0")
    private int discount;

    @Column(name = "quantity",nullable = false)
    @ColumnDefault("1")
    private int quantity;

    @Column(name = "status",nullable = false)
    @ColumnDefault("0")
    private int status;

    @JsonIgnore
    @Column(name = "isactive",nullable = false)
    @ColumnDefault("1")
    private int isactive;

    @JsonIgnore
    @Column(name = "createat")
    private Timestamp createat;

    @JsonIgnore
    @Column(name = "updateat")
    private Timestamp updateat;

    @ManyToOne() //EAGER
    @JoinColumn(name = "orderid", insertable = false, updatable = false)
    private OrderEntity orderDetail;

    @ManyToOne() //EAGER
    @JoinColumn(name = "productid", insertable = false, updatable = false)
    private ProductEntity product_order;

    @ManyToOne(fetch = FetchType.LAZY) //EAGER
    @JoinColumn(name = "sellerid", insertable = false, updatable = false)
    private UserEntity seller;

    public OrderDetailEntity() {
    }

    public OrderDetailEntity( int productid, Timestamp createat) {
        this.productid = productid;
        this.createat = createat;
    }
}
