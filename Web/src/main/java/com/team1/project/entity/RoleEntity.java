package com.team1.project.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Table(name = "role")
@Entity
public class RoleEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name",nullable = false)
    private String name;

    @OneToMany(mappedBy = "roleUser")
    private List<UserEntity> user;


}