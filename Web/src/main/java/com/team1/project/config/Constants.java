package com.team1.project.config;

public class Constants {
    public static final int PAGE_SIZE_WEB = 8;
    public static final int PAGE_SIZE_APP = 12;
    public static final int ACTIVE = 1;
    public static final int NO_ACTIVE = 0;
    public static final int MALE = 0; // nam
    public static final int FEMALE = 1; // nữ


    // config table role

    public static final String ROLE_SUPERADMIN= "SuperAdmin";
    public static final String ROLE_SUPERADMIN_ID= "1";
    public static final String ROLE_ADMIN= "Admin";
    public static final String ROLE_SELLER= "Seller";
    public static final int ROLE_SELLER_ID= 3;
    public static final int ROLE_BUYER_ID= 4;
    public static final int ROLE_ADMIN_ID = 2;
    public static final String ROLE_BUYER= "Buyer";

    // config table order

    public static final int ORDER_ALL = 100; // get url all data . don't save to database
    public static final int ORDER_WAIT_CONFIRM= 0; // đơn hàng chờ xác nhận khi khởi tạo
    public static final int ORDER_CONFIRM = 1; // đơn hàng đang xác nhận khi có 1 trong nhiều người bán thay đổi trang thái
    public static final int ORDER_FINISH = 2; // đơn hàng đã xác nhận khi tất cả các order detail thay đổi trạng thái
    public static final int ORDER_SHIP = 3; // đơn hàng đang được ship
    public static final int ORDER_SUCCESS = 4; // đơn hoàn thành
    public static final int ORDER_CANCEL = -1; // đơn hàng đã hủy
    public static final int ORDER_REFUSE = -2; // đơn hàng đã bị từ chối khi tất cả các order detail bị từ chối

    public static final String ORDER_CREATE_SESSION = "ojhfie";

    // config chi tiết đơn hàng
    public static final int ORDER_DETAIL_WAIT_CONFIRM= 0; // đơn hàng chờ xác nhận
    public static final int ORDER_DETAIL_CONFIRM = 1; // đơn hàng đã xác nhận
    public static final int ORDER_DETAIL_FINISH = 2; // đơn đã hoàn thành ( nhận được hàng)
    public static final int ORDER_DETAIL_CANCEL = -1; // bị hủy bởi người mua
    public static final int ORDER_DETAIL_REFUSE = -2; // bị từ chối bởi người bán



    // config News
    public static final int NEWS_TYPE = 1;
    public static final int BANNER_TYPE = 2;

    // config sản phẩm
    public static final int PRODUCT_OUT_OF_STOCK = 0; // sản phẩm hết hàng
    public static final int PRODUCT_AVAILABLE = 1; // sản phẩm còn hàng
    public static final int PRODUCT_NOT_AVAILABLE = 99; // sản phẩm ngừng kinh doanh

    // Replace with your email here:
    public static final String SYSTEM_EMAIL = "sannongnghiepsach@gmail.com";

    // Replace password!!
    public static final String SYSTEM_PASSWORD = "hsobgfnqokhwlixz";

    // And receiver!
    public static final String ADMIN_EMAIL = "nguyenminhan696@gmail.com";

    // Folder Anh
    public static final String IMAGE_FOLDER = "./img";
    public static final String IMAGE_FOLDER_PRODUCT = "./img/product/";
    public static final String IMAGE_FOLDER_CATEGORY = "./img/category/";
    public static final String IMAGE_FOLDER_NEWS = "./img/news/";

    // Folder ảnh vps
    public static final String IMAGE_FOLDER_SERVER = "/var/www/html/nongnghiepsach.cf/img/";
    public static final String IMAGE_FOLDER_CATEGORY_SERVER = "/var/www/html/nongnghiepsach.cf/img/category/";
    public static final String IMAGE_FOLDER_PRODUCT_SERVER = "/var/www/html/nongnghiepsach.cf/img/product/";
    public static final String IMAGE_FOLDER_NEWS_SERVER = "/var/www/html/nongnghiepsach.cf/img/news/";
}
