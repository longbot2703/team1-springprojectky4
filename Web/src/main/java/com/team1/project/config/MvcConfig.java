package com.team1.project.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.nio.file.Path;
import java.nio.file.Paths;

@Configuration
public class MvcConfig implements WebMvcConfigurer {
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
//      fix read file in local host
        Path uploadDir = Paths.get(Constants.IMAGE_FOLDER);
        String uploadPath = uploadDir.toFile().getAbsolutePath();

        registry.addResourceHandler("/img/**").addResourceLocations("file:/" + uploadPath + "/");

//      fix url read file in vps
//        registry.addResourceHandler("/img/**")
//                .addResourceLocations("file://"+Constants.IMAGE_FOLDER_SERVER);
    }
}
