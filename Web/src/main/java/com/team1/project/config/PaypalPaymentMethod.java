package com.team1.project.config;

public enum PaypalPaymentMethod {
    credit_card, paypal
}
