package com.team1.project.config;

import java.io.*;
import java.nio.file.*;
import java.util.HashSet;
import java.util.Set;

import org.springframework.web.multipart.MultipartFile;

public class FileUploadUtil {
    public static void saveFile(String uploadDir, String fileName,
                                MultipartFile multipartFile,
                                String oldFileName) throws IOException {

        Path uploadPath = Paths.get(uploadDir);

        if (!Files.exists(uploadPath)) {
            Files.createDirectories(uploadPath);
        }

        if(Files.list(Paths.get(uploadDir)).findAny().isPresent() && oldFileName != null && !fileName.equals("")){
            String oldFile = uploadDir + "/" + oldFileName;
            Path fileToDelete = Paths.get(oldFile);
            Files.delete(fileToDelete);
        }

        try (InputStream inputStream = multipartFile.getInputStream()) {
            Path filePath = uploadPath.resolve(fileName);
            Files.copy(inputStream, filePath, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException ioe) {
            throw new IOException("Không thể upload được file: " + fileName, ioe);
        }
    }
}