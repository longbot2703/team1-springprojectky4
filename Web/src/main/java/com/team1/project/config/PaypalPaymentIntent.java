package com.team1.project.config;

public enum PaypalPaymentIntent {
    sale, authorize, order
}
