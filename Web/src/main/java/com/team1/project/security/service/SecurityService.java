package com.team1.project.security.service;

public interface SecurityService {

    boolean isAuthenticated();
    void autoLogin(String username, String password);
}
