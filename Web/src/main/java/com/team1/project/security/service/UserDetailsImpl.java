//package com.team1.project.security.service;
//
//import com.fasterxml.jackson.annotation.JsonIgnore;
//import com.team1.project.entity.UserEntity;
//import lombok.AllArgsConstructor;
//import lombok.Data;
//import org.springframework.security.core.GrantedAuthority;
//import org.springframework.security.core.authority.SimpleGrantedAuthority;
//import org.springframework.security.core.userdetails.UserDetails;
//
//import java.util.ArrayList;
//import java.util.Collection;
//import java.util.Collections;
//import java.util.List;
//import java.util.stream.Collectors;
//
//@Data
//public class UserDetailsImpl implements UserDetails {
//    private static final long serialVersionUID = 1L;
//
//    private int id;
//
//    private String username;
//
//    private String token;
//
//    private String email;
//
//    @JsonIgnore
//    private String password;
//
//    private Collection<? extends GrantedAuthority> authorities;
//
//    public UserDetailsImpl(int id, String username, String token ,String email, String password,
//                           Collection<? extends GrantedAuthority> authorities) {
//        this.id = id;
//        this.username = username;
//        this.token = token;
//        this.email = email;
//        this.password = password;
//        this.authorities = authorities;
//    }
//
//    public static UserDetailsImpl build(UserEntity user) {
//        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
//        SimpleGrantedAuthority authority  = new SimpleGrantedAuthority(user.getRoleUser().getName());
//        authorities.add(authority);
//
//        return new UserDetailsImpl(
//                user.getId(),
//                user.getUsername(),
//                user.getToken(),
//                user.getEmail(),
//                user.getPassword(),
//                authorities);
//    }
//
//    @Override
//    public Collection<? extends GrantedAuthority> getAuthorities() {
//        return authorities;
//    }
//
//    public int getId() {
//        return id;
//    }
//
//    public String getEmail() {
//        return email;
//    }
//
//    public String getToken() {
//        return token;
//    }
//
//    @Override
//    public String getPassword() {
//        return password;
//    }
//
//    @Override
//    public String getUsername() {
//        return username;
//    }
//
//    @Override
//    public boolean isAccountNonExpired() {
//        return true;
//    }
//
//    @Override
//    public boolean isAccountNonLocked() {
//        return true;
//    }
//
//    @Override
//    public boolean isCredentialsNonExpired() {
//        return true;
//    }
//
//    @Override
//    public boolean isEnabled() {
//        return true;
//    }
//
//}
