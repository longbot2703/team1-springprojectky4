package com.team1.project.security;

import com.team1.project.config.Constants;
import com.team1.project.security.service.PasswordConfig;
import com.team1.project.security.service.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;



@Configuration
@EnableWebSecurity
@Order(1)
public class AdminSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    UserDetailsServiceImpl userDetailsService;

    @Autowired
    PasswordConfig passwordConfig;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordConfig.passwordEncoder());
    }


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.antMatcher("/admin/**").csrf().disable().authorizeRequests()

                .antMatchers("/admin/js/**","/admin/js/core/**","/admin/js/plugins/**").permitAll()
                .antMatchers("/admin/demo/**").permitAll()
                .antMatchers("/admin/fonts/**","/admin/img/faces/**","/admin/img/**","/img/**")
                .permitAll()

                .antMatchers("/admin/scss/**","/admin/css/**").permitAll()

                .antMatchers("/admin/index","/admin/buyer/**","/admin/business/**","/admin/business",
                        "/admin/business/order","/admin/business/order/**")
                .hasAnyAuthority(Constants.ROLE_ADMIN,Constants.ROLE_SUPERADMIN, Constants.ROLE_SELLER)

                .antMatchers("/admin/user","admin/listshipping","/admin/news","/admin/user","/admin/news/create",
                        "/admin/shipping/create","/admin/listcustomer","/admin/seller","/admin/category/**","/admin/statistical","/admin/uploadimg","/admin/searchCate",
                        "/admin/customer/**")
                .hasAnyAuthority(Constants.ROLE_ADMIN, Constants.ROLE_SUPERADMIN)


                .and()
                .formLogin()
                .loginPage("/admin/login")
                .usernameParameter("username")
                .passwordParameter("password")
                .defaultSuccessUrl("/defaulturl")
                .failureUrl("/admin/login?error=true")
                .permitAll()

                .and()
                .logout()
                .logoutUrl("/admin/logout")
                .invalidateHttpSession(true)
                .clearAuthentication(true)
                .deleteCookies("JSESSIONID")
                .logoutSuccessUrl("/admin/login")
                .permitAll()

                .and()
                .exceptionHandling().accessDeniedPage("/error");
    }
}
