$(document).ready(function () {
    $('#input-url-image').dropify({
        messages: {
            default: 'Bấm để chọn ảnh ',
            replace: 'Bấm để chọn ảnh khác ',
            remove: 'Xóa ảnh'
        }
    });
})

function createCate() {
    let name = $.trim($('#txt-name').val());
    let status = $.trim($('#status-val').val());

    let des = $.trim($('#txt-description').val());

    let fileImage = $('#input-url-image').get(0).files;
    if (fileImage.length == 0) {
        Swal.fire({
            position: 'top-end',
            icon: 'warning',
            title: "Vui lòng chọn ảnh danh mục!",
            showConfirmButton: false,
            timer: 1500
        })
        return;
    }
    let fileName = fileImage[0].name;
    var fileData = new FormData();
    fileData.append("img", fileImage[0]);


    if (name.length == 0) {
        Swal.fire({
            position: 'top-end',
            icon: 'warning',
            title: "Vui lòng nhập tên danh mục",
            showConfirmButton: false,
            timer: 1500
        })
        return;
    }
    var data = {name: name, description: des, createat: parseInt(status), image: fileName};

    $.ajax({
        url: "/admin/uploadimg",
        type: "POST",
        data: fileData,
        contentType: false, // Not to set any content header
        processData: false, // Not to process data,
        cache: false,
        success: function (res) {
            if (res.status == 1) {
                $.ajax({
                    url: "/admin/category/createCate",
                    method: 'POST',
                    dataType: 'json',
                    contentType: 'application/json; charset=utf-8',
                    data: JSON.stringify(data),
                    success: function (res) {
                        if (res.status == 1) {
                            Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: res.message,
                                showConfirmButton: false,
                                timer: 1500
                            }).then((ok) => {
                                if (ok)
                                    location.reload();
                            })
                        } else {
                            Swal.fire({
                                position: 'top-end',
                                icon: 'error',
                                title: res.message,
                                showConfirmButton: false,
                                timer: 1500
                            })
                        }
                    }, error: function (err) {
                        console.log(err)
                    }
                })
            } else {
                Swal.fire({
                    position: 'top-end',
                    icon: 'error',
                    title: res.message,
                    showConfirmButton: false,
                    timer: 1500
                })
            }
        }
    })
}


$('.btn-detail').click(function () {
    $('#div-img-detail').html("");
    let id = $(this).attr('data-id');
    let name = $(this).attr('data-name');
    let des = $(this).attr('data-des');
    let status = $(this).attr('data-status');
    let date = $(this).attr('data-date');
    let image = $(this).attr('data-image');

    //Dưa các giá trị tương ứng vào modal
    $('#id-val').val(id);
    $('#txt-name-edit').val(name);
    $('#txt-description-edit').val(des);
    $('#status-val-edit').val(status.toString());
    $('#date-val').val(date);
    $('#cate-detai').modal('show');
    let imgFile = '<input type="file" id="input-url-image-edit" data-default-file ="' + window.location.origin + image + '" name="UrlImage" class="dropify" accept="image/*" />'
    $('#div-img-detail').html(imgFile);
    $('#input-url-image-edit').dropify({
        messages: {
            default: 'Bấm để chọn ảnh ',
            replace: 'Bấm để chọn ảnh khác ',
            remove: 'Xóa ảnh'
        }
    });

})

$('#update-cate').click(function () {
    let fileImage = $('#input-url-image-edit').get(0).files;
    let id = $('#id-val').val();
    let name = $('#txt-name-edit').val();
    let des = $('#txt-description-edit').val();
    let status = $('#status-val-edit').val();
    let date = $('#date-val').val();
    let d = new Date(date);
    let img = '';
    if (fileImage.length == 0) {
        let arr = $('#input-url-image-edit').attr('data-default-file').split('/');
        img = arr[arr.length - 1];
    } else {
        img = fileImage[0].name;
    }

    if (fileImage.length == 0 && img.length == 0) {
        Swal.fire({
            position: 'top-end',
            icon: 'warning',
            title: "Vui lòng chọn ảnh danh mục!",
            showConfirmButton: false,
            timer: 1500
        })
        return;
    }

    if (name.length == 0) {
        Swal.fire({
            position: 'top-end',
            icon: 'warning',
            title: "Vui lòng nhập tên danh mục",
            showConfirmButton: false
        })
        return;
    }

    var data = {
        id: id,
        name: name,
        description: des,
        updateat: d.getTime(),
        createat: d.getTime(),
        image: img,
        isactive: parseInt(status)
    };
    var fileData = new FormData();
    fileData.append("img", fileImage[0]);
    if (fileImage.length > 0) {
        $.ajax({
            url: "/admin/uploadimg",
            type: "POST",
            data: fileData,
            contentType: false, // Not to set any content header
            processData: false, // Not to process data,
            cache: false,
            success: function (res) {
                if (res.status != 1) {
                    Swal.fire({
                        position: 'top-end',
                        icon: 'error',
                        title: res.message,
                        showConfirmButton: false,
                        timer: 1500
                    })
                    return;
                }
            }
        })
    }
    $.ajax({
        url: "/admin/category/update",
        method: 'POST',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(data),
        success: function (res) {
            if (res.status == 1) {
                Swal.fire({
                    position: 'top-end',
                    icon: 'success',
                    title: res.message,
                    showConfirmButton: false
                }).then((ok) => {
                    if (ok)
                        location.reload();
                })
            } else {
                Swal.fire({
                    position: 'top-end',
                    icon: 'error',
                    title: res.message,
                    showConfirmButton: false,
                    timer: 1500
                })
            }
        }, error: function (err) {
            console.log(err)
        }
    })
})

$('#btn-search').click(function (){
    let name = $.trim($('#txt-search').val())
    $.ajax({
        url:"/admin/searchCate",
        type:"GET",
        data:{name:name},
        success:function (res){
            $('#tbl-cate').html(res)
        }, error: function (err) {
            console.log(err)
        }
    })
})

$('.delete-btn').click(function () {
    let id = $(this).attr('data-id');
    let name = $(this).attr('data-name');
    let des = $(this).attr('data-des');
    let date = $(this).attr('data-date');
    let image = $(this).attr('data-image');
    let arr = image.split('/');
    let d = new Date(date);
    var data = {id: id, name: name, description: des, createat: 0, createat: d.getTime(), image: arr[arr.length - 1]};

    Swal.fire({
        position: 'top-end',
        icon: 'warning',
        title: "Bạn chắc chắn muốn xóa?",
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ok!'
    }).then((sure) => {
        if (sure.value) {
            $.ajax({
                url: "/admin/category/delete",
                method: 'POST',
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(data),
                success: function (res) {
                    if (res.status == 1) {
                        Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            title: res.message,
                            showConfirmButton: false
                        }).then((ok) => {
                            if (ok)
                                location.reload();
                        })
                    } else {
                        Swal.fire({
                            position: 'top-end',
                            icon: 'error',
                            title: res.message,
                            showConfirmButton: false,
                            timer: 1500
                        })
                    }
                }, error: function (err) {
                    console.log(err)
                }
            })
        }
    })
})
