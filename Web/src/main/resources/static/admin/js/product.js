$(document).ready(function (){
    $('#inputProductImage1').change(function (){
        showImageThumbnail1(this);
    });
    $('#inputProductImage2').change(function (){
        showImageThumbnail2(this);
    });
    $('#inputProductImage3').change(function (){
        showImageThumbnail3(this);
    });
    $('#inputProductImage4').change(function (){
        showImageThumbnail4(this);
    });
    $('#inputProductDetailImage').change(function (){
        showImageProductDetailThumbnail(this);
    });
})

function showImageThumbnail1(fileInput){
    $('#productThumbnail1').css('display', 'block');
    file = fileInput.files[0];
    reader = new FileReader();
    reader.onload = function (e) {
        $('#productThumbnail1').attr('src',e.target.result);
    };
    reader.readAsDataURL(file);
}
function showImageThumbnail2(fileInput){
    $('#productThumbnail2').css('display', 'block');
    file = fileInput.files[0];
    reader = new FileReader();
    reader.onload = function (e) {
        $('#productThumbnail2').attr('src',e.target.result);
    };
    reader.readAsDataURL(file);
}
function showImageThumbnail3(fileInput){
    $('#productThumbnail3').css('display', 'block');
    file = fileInput.files[0];
    reader = new FileReader();
    reader.onload = function (e) {
        $('#productThumbnail3').attr('src',e.target.result);
    };
    reader.readAsDataURL(file);
}
function showImageThumbnail4(fileInput){
    $('#productThumbnail4').css('display', 'block');
    file = fileInput.files[0];
    reader = new FileReader();
    reader.onload = function (e) {
        $('#productThumbnail4').attr('src',e.target.result);
    };
    reader.readAsDataURL(file);
}

function showImageProductDetailThumbnail(fileInput2){
    $('#productDetailThumbnail').css('display', 'block');
    file2 = fileInput2.files[0];
    reader = new FileReader();
    reader.onload = function (e) {
        $('#productDetailThumbnail').attr('src',e.target.result);
    };
    reader.readAsDataURL(file2);
}

function delete_product(id){
    Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        if(result.value){
            var product = {
                "id" : id
            };
            $.ajax({
                url : '/admin/product/delete',
                method : 'POST',
                dataType : 'json',
                contentType : 'application/json; charset=utf-8',
                data : JSON.stringify(product),
                success: function(res) {
                    if(res.status == 1){
                        sweetalert_success(res.message);
                    }else{
                        sweetalert_err(res.message)
                    }
                },
                error: function(request, status, error) {
                    alert(error);
                }
            })
            // location.reload();
        }

    });
}