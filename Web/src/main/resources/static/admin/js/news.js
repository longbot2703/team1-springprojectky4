function delete_news(id){
    Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        if(result.value){
            var news = {
                "id" : id
            };
            $.ajax({
                url : '/admin/news/delete',
                method : 'POST',
                dataType : 'json',
                contentType : 'application/json; charset=utf-8',
                data : JSON.stringify(news),
                success: function(res) {
                    if(res.status == 1){
                        sweetalert_success(res.message);
                    }else{
                        sweetalert_err(res.message)
                    }
                },
                error: function(request, status, error) {
                    alert(error);
                }
            })
            // location.reload();
        }

    });
}

$(document).ready(function () {
    $('#input-url-image-edit').dropify({
        messages: {
            default: 'Bấm để chọn ảnh ',
            replace: 'Bấm để chọn ảnh khác ',
            remove: 'Xóa ảnh'
        }
    });
})
$(document).ready(function () {
    $('#input-url-image-create').dropify({
        messages: {
            default: 'Bấm để chọn ảnh ',
            replace: 'Bấm để chọn ảnh khác ',
            remove: 'Xóa ảnh'
        }
    });
})
$('#update-news').click(function () {

    let fileImage = $('#input-url-image-edit').get(0).files;
    let id = $('#id-val').val();
    let title = $('#inputTitle').val();
    let des = $('#inputDescription').val();
    let content=$('#inputContent').val();
    let type = $('#type-val-edit').val();
    let img = '';
    if (fileImage.length == 0) {
        let arr = $('#input-url-image-edit').attr('data-default-file').split('/');
        img = arr[arr.length - 1];
    } else {
        img = fileImage[0].name;
    }

    if (fileImage.length == 0 && img.length == 0) {
        Swal.fire({
            position: 'top-end',
            icon: 'warning',
            title: "Vui lòng chọn ảnh danh mục!",
            showConfirmButton: false,
            timer: 1500
        })
        return;
    }

    if (title.length == 0) {
        Swal.fire({
            position: 'top-end',
            icon: 'warning',
            title: "Vui lòng nhập tên tin tức",
            showConfirmButton: false
        })
        return;
    }
    if (des.length == 0) {
        Swal.fire({
            position: 'top-end',
            icon: 'warning',
            title: "Vui lòng nhập miêu tả tin tức",
            showConfirmButton: false
        })
        return;
    }if (content.length == 0) {
        Swal.fire({
            position: 'top-end',
            icon: 'warning',
            title: "Vui lòng nhập nội dung tin tức ",
            showConfirmButton: false
        })
        return;
    }
    var data = {
        id: id,
        title: title,
        description: des,
        imageurl: img,
        content:content,
        type:type,
        isactive:1,
        createdat:"",
        updatedat:"",
        deletedat:""
    };
    var fileData = new FormData();
    fileData.append("img", fileImage[0]);
    if (fileImage.length > 0) {
        $.ajax({
            url: "/admin/uploadimgnews",
            type: "POST",
            data: fileData,
            contentType: false, // Not to set any content header
            processData: false, // Not to process data,
            cache: false,
            success: function (res) {
                if (res.status != 1) {
                    Swal.fire({
                        position: 'top-end',
                        icon: 'error',
                        title: res.message,
                        showConfirmButton: false,
                        timer: 1500
                    })
                    return;
                }
            }
        })
    }
    $.ajax({
        url: "/admin/news/edit",
        method: 'POST',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(data),
        success: function (res) {
            if (res.status == 1) {
                Swal.fire({
                    position: 'top-end',
                    icon: 'success',
                    title: "update thành công",
                    showConfirmButton: false
                }).then((ok) => {
                    if (ok)
                        location.reload();
                })
            } else {
                Swal.fire({
                    position: 'top-end',
                    icon: 'error',
                    title: "",
                    showConfirmButton: false,
                    timer: 1500
                })
            }
        }, error: function (err) {
            console.log(err)
        }
    })
})

$('#create-news').click(function () {

    let fileImage = $('#input-url-image-create').get(0).files;
    let title = $('#inputTitle').val();
    let des = $('#inputDescription').val();
    let content=$('#inputContent').val();
    let type = $('#type-val-edit').val();
    let img = '';
    if (fileImage.length == 0) {
        let arr = $('#input-url-image-create').attr('data-default-file').split('/');
        img = arr[arr.length - 1];
    } else {
        img = fileImage[0].name;
    }

    if (fileImage.length == 0 && img.length == 0) {
        Swal.fire({
            position: 'top-end',
            icon: 'warning',
            title: "Vui lòng chọn ảnh danh mục!",
            showConfirmButton: false,
            timer: 1500
        })
        return;
    }

    if (title.length == 0) {
        Swal.fire({
            position: 'top-end',
            icon: 'warning',
            title: "Vui lòng nhập tên tin tức",
            showConfirmButton: false
        })
        return;
    }
    if (des.length == 0) {
        Swal.fire({
            position: 'top-end',
            icon: 'warning',
            title: "Vui lòng nhập miêu tả tin tức",
            showConfirmButton: false
        })
        return;
    }if (content.length == 0) {
        Swal.fire({
            position: 'top-end',
            icon: 'warning',
            title: "Vui lòng nhập nội dung tin tức ",
            showConfirmButton: false
        })
        return;
    }
    var data = {
        id: 0,
        title: title,
        description: des,
        imageurl: img,
        content:content,
        type:type,
        isactive:1,
        createdat:"",
        updatedat:"",
        deletedat:""
    };
    var fileData = new FormData();
    fileData.append("img", fileImage[0]);
    if (fileImage.length > 0) {
        $.ajax({
            url: "/admin/uploadimgnews",
            type: "POST",
            data: fileData,
            contentType: false, // Not to set any content header
            processData: false, // Not to process data,
            cache: false,
            success: function (res) {
                if (res.status != 1) {
                    Swal.fire({
                        position: 'top-end',
                        icon: 'error',
                        title: res.message,
                        showConfirmButton: false,
                        timer: 1500
                    })
                    return;
                }
            }
        })
    }
    $.ajax({
        url: "/admin/news/create",
        method: 'POST',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(data),
        success: function (res) {
            if (res.status == 1) {
                Swal.fire({
                    position: 'top-end',
                    icon: 'success',
                    title: "thêm thành công",
                    showConfirmButton: false
                }).then((ok) => {
                    if (ok)
                        location.reload();
                })
            } else {
                Swal.fire({
                    position: 'top-end',
                    icon: 'error',
                    title: "",
                    showConfirmButton: false,
                    timer: 1500
                })
            }
        }, error: function (err) {
            console.log(err)
        }
    })
})