function delete_ship(id){
    Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        if(result.value){
            var ship = {
                "id" : id
            };
            $.ajax({
                url : '/admin/delete/shipper',
                method : 'POST',
                dataType : 'json',
                contentType : 'application/json; charset=utf-8',
                data : JSON.stringify(ship),
                success: function(res) {
                    if(res.status == 1){
                        sweetalert_success(res.message);
                    }else{
                        sweetalert_err(res.message)
                    }
                },
                error: function(request, status, error) {
                    alert(error);
                }
            })
            // location.reload();
        }

    });
}
