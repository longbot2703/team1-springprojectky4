function delete_cus(id){
    Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        if(result.value){
            var cus = {
                "id" : id
            };
            $.ajax({
                url : '/admin/customer/delete',
                method : 'POST',
                dataType : 'json',
                contentType : 'application/json; charset=utf-8',
                data : JSON.stringify(cus),
                success: function(res) {
                    if(res.status == 1){
                        sweetalert_success(res.message);
                    }else{
                        sweetalert_err(res.message)
                    }
                },
                error: function(request, status, error) {
                    alert(error);
                }
            })
            // location.reload();
        }

    });
}