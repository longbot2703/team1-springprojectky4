function reload(){
    location.reload();
}
function sweetalert_success(mess ="Thành công !"){
    Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: mess,
        showConfirmButton: false,
        timer: 1500
    })
}
function sweetalert_err(mess ="Thất bại !"){
    Swal.fire({
        position: 'top-end',
        icon: 'error',
        title: mess,
        showConfirmButton: false,
        timer: 1500
    })
}


function showSeller(id){

    document.getElementById("idUpdateSeller").value = id;
    // document.getElementById("seller_full_name_modal").innerHTML = document.getElementById("seller_name_"+id).innerText;
    document.getElementById("seller_email_modal").value = document.getElementById("seller_email_"+id).innerText;
    document.getElementById("seller_full_name_modal").value = document.getElementById("seller_full_name_"+id).innerText;
}

/**
 * Super admin create amdin
 */
function createSeller(){
    var buyerCreate ={
        "username":document.getElementById("username_create_seller_frm").value,
        "fullname":document.getElementById("fullname_create_seller_frm").value,
        "email":document.getElementById("email_create_seller_frm").value,
        "password":document.getElementById("password_create_seller_frm").value
    }

    $.ajax({
        url : '/admin/seller/create',
        method : 'POST',
        dataType : 'json',
        contentType : 'application/json; charset=utf-8',
        data : JSON.stringify(buyerCreate),
        success: function(res) {
            if(res.status === 1){
                Swal.fire({
                    icon: 'success',
                    title: res.message,
                    showConfirmButton: false,
                    timer: 1500
                }).then((ok) => {
                    if (ok) window.location.reload();
                })
            }else{
                sweetalert_err();
            }
        },
        error: function(request, status, error) {
            alert("Not Call API");
        }
    })
};

/**
 * Admin delete Seller
 */
function delete_seller(id){
    Swal.fire({
        title: 'Xóa nhà cung cấp',
        text: "Bạn có thật sự muốn xóa nhà cung cấp này",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        if(result.value){
            var user = {
                "id" : id
            };
            $.ajax({
                url : '/admin/seller/delete',
                method : 'POST',
                dataType : 'json',
                contentType : 'application/json; charset=utf-8',
                data : JSON.stringify(user),
                success: function(res) {
                    if(res.status == 1){
                        Swal.fire({
                            icon: 'success',
                            title: res.message,
                            showConfirmButton: false,
                            timer: 1500
                        }).then((ok) => {
                            if (ok) window.location.reload();
                        })
                    }else{
                        sweetalert_err(res.message)
                    }
                },
                error: function(request, status, error) {
                    // alert("Not Call API");
                    alert(error);
                }
            })
            // location.reload();
        }

    });
}

/**
 * Admin update Seller
 */
function updateSeller(){
    let seller_update ={
        "id":document.getElementById("idUpdateSeller").value,
        "fullname":document.getElementById("seller_full_name_modal").value,
        "email":document.getElementById("seller_email_modal").value
    }

    $.ajax({
        url : '/admin/seller/update',
        method : 'POST',
        dataType : 'json',
        contentType : 'application/json; charset=utf-8',
        data : JSON.stringify(seller_update),
        success: function(res) {
            if(res.status == 1){
                sweetalert_success(res.message);
            }else{
                sweetalert_err(res.message);
            }
        },
        error: function(request, status, error) {
            alert("Not Call API");
        }
    })
};

/**
 * Super admin update amdin
 */
function resetpasswordseller(id){
    alert("Chức năng này không tồn tại");
    // var user ={
    //     "id":id
    // }
    //
    // $.ajax({
    //     url : '/admin/user/resetpassword',
    //     method : 'POST',
    //     dataType : 'json',
    //     contentType : 'application/json; charset=utf-8',
    //     data : JSON.stringify(user),
    //     success: function(res) {
    //         if(res.status == 1){
    //             sweetalert_success(res.message);
    //         }else{
    //             sweetalert_err(res.message);
    //         }
    //     },
    //     error: function(request, status, error) {
    //         alert("Not Call API");
    //     }
    // })
}

// xac nhan don hang
function sellerConfirmOrder(id){
    Swal.fire({
        title: 'Xác nhận',
        text: "Xác nhận đơn hàng",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes !'
    }).then((result) => {
        if (result.value) {
            let data = {
                "id": id,
                "status" : 1
            };
            $.ajax({
                url : '/admin/business/order/update',
                method : 'POST',
                dataType : 'json',
                contentType : 'application/json; charset=utf-8',
                data : JSON.stringify(data),
                success: function(res) {
                    if(res.status == 1){
                        Swal.fire({
                            icon: 'success',
                            title: res.message,
                            showConfirmButton: false,
                            timer: 1500
                        }).then((ok) =>{
                            if(ok)
                                window.location.href = '/admin/business/order/detail?code='+res.data;
                        })
                    }else{
                        sweetalert_err(res.message)
                    }
                },
                error: function(request, status, error) {
                    alert("Not Call API");
                }
            })
        }
    });
}


/**
 * từ chối đơn hàng
 * @param orderid
 */
let sellerCancelOrder = (orderid)=>{
    Swal.fire({
        title: 'Xác nhận',
        text: "Từ chối đơn hàng",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes !'
    }).then((result) => {
        if (result.value) {
            let data = {
                "id": orderid
            };
            $.ajax({
                url : '/admin/business/order/delete',
                method : 'POST',
                dataType : 'json',
                contentType : 'application/json; charset=utf-8',
                data : JSON.stringify(data),
                success: function(res) {
                    if(res.status == 1){
                        Swal.fire({
                            icon: 'success',
                            title: res.message,
                            showConfirmButton: false,
                            timer: 1500
                        }).then((ok) =>{
                            if(ok)
                                window.location.href = '/admin/business/order/index';
                        })
                    }else{
                        sweetalert_err(res.message)
                    }
                },
                error: function(request, status, error) {
                    alert("Not Call API");
                }
            })
        }
    });
}