function is_phonenumber(phonenumber) {
    const phoneno = /^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/;
    if(phonenumber.match(phoneno)) {
        return true;
    }
    else {
        return false;
    }
}

function ValidateEmail(mail)
{
    const validRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
    if (mail.match(validRegex))
    {
        return true;
    }
    return false;
}

function updateCustomer(){
    let job = document.getElementById('jobCustomer').value;
    let fullname = document.getElementById('fullnameCustomer').value;
    let email = document.getElementById('emailCustomer').value;
    let phone = document.getElementById('phoneCustomer').value;
    let address = document.getElementById('addressCustomer').value;
    let sex = document.getElementById('sex').value;

    if(!email && !ValidateEmail(email) ){
        sweetalert_err("Email không đúng định dạng !");
    }else if( !phone && !is_phonenumber(phone)){
        sweetalert_err("Số điện thoại không hợp lệ !");
    }else {
        let data = {
            "email": email,
            "fullname":fullname,
            "address": address,
            "sex": sex,
            "customer" : {
                "phone": phone,
                "job": job
            }
        }

        $.ajax({
            url : '/customer/detail/update',
            method : 'POST',
            dataType : 'json',
            contentType : 'application/json; charset=utf-8',
            data : JSON.stringify(data),
            success: function(res) {
                if(res.status === 1){
                    sweetalert_success(res.message);
                }else{
                    sweetalert_err(res.message)
                }
            },
            error: function(request, status, error) {
                alert("Not Call API");
            }
        })
    }
}
function UpdateAddressUser(){
    let city=document.getElementById('city').value;
    let district=document.getElementById('district').value;
    let addressDetail=document.getElementById('addressDetail').value;
    let phone = document.getElementById('phone').value;
    let address=addressDetail+" , "+district+" , "+city;
    let data = {
        "address": address,
        "phone" : phone,
    }
    $.ajax({
        url : '/customer/checkout/updateaddress',
        method : 'POST',
        dataType : 'json',
        contentType : 'application/json; charset=utf-8',
        data : JSON.stringify(data),
        success: function(res) {
            if(res.status === 1){
                Swal.fire({
                    icon: 'success',
                    title: res.message,
                    showConfirmButton: false,
                    timer: 1500
                }).then((ok) =>{
                    if(ok) window.location.href = '/customer/checkout';
                })
            }else{
                sweetalert_err(res.message)
            }
        },
        error: function(request, status, error) {
            alert("Not Call API");
        }
    })
}