
/**
 * create contact
 */
function sendContact(){
    let contact = {
        "name" : document.getElementById('contactName').value,
        "email" : document.getElementById('contactEmail').value,
        "subject" : document.getElementById('contactSubject').value,
        "message" : document.getElementById('contactMessage').value
    };

    if (contact.name == "") {
        sweetalert_err("Tên không thể để trống");
    }else if (contact.email == ""){
        sweetalert_err("Email không thể để trống");
    }else if (contact.subject == "") {
        sweetalert_err("Tiêu đề không thể để trống");
    }else if (contact.message == ""){
        sweetalert_err("Nội dung tin nhắn không thể để trống");
    }else {
        $.ajax({
            url : '/contact/send',
            method : 'POST',
            dataType : 'json',
            contentType : 'application/json; charset=utf-8',
            data : JSON.stringify(contact),
            success: function(res) {
                if(res.status === 1){
                    sweetalert_success(res.message);
                }else{
                    sweetalert_err(res.message)
                }
            },
            error: function(request, status, error) {
                alert("Not Call API");
            }
        })
    }


    // location.reload();

}
