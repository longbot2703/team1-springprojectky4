// noinspection JSUnresolvedVariable

function validateEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

function register(){
    let username = document.getElementById('username_register').value;
    let email = document.getElementById('email_register').value;
    let password = document.getElementById('password_register').value;
    let confirmPass = document.getElementById('confirm_password').value;
    const err = document.querySelector('#errRegister');

    if(!username || !email || !password || !confirmPass ){
        err.innerHTML  = "<div class='form-outline flex-fill mb-0 text-danger text-center'><span>Vui lòng nhập đủ thông tin !</span></div>";
    }else {
        if(!validateEmail(email)){
            err.innerHTML  = "<div class='form-outline flex-fill mb-0 text-danger text-center' ><span>Email không đúng định dạng !</span></div>";
        }else if(password.length < 6 ){
            err.innerHTML  = "<div class='form-outline flex-fill mb-0 text-danger text-center' ><span>Độ dài mật khẩu phải lớn hơn 6 ký tự !</span></div>";
        }else if(password !== confirmPass){
            err.innerHTML  = "<div class='form-outline flex-fill mb-0 text-danger text-center' ><span>Xác nhận mật khẩu không khớp với mật khẩu !</span></div>";
        }
        else {
            err.innerHTML = "";
            let data = {
                "username": username,
                "email": email,
                "password": password
            }

            $.ajax({
                url : '/register',
                method : 'POST',
                dataType : 'json',
                contentType : 'application/json; charset=utf-8',
                data : JSON.stringify(data),
                success: function(res) {
                    if(res.status === 1){
                        Swal.fire({
                            icon: 'success',
                            title: "Đăng ký tài khoản thành công !",
                            showConfirmButton: false,
                            timer: 2000
                        }).then((ok) =>{
                            if(ok)
                                window.location.href = '/index';
                        })
                    }else{
                        err.innerHTML  = "<div class='form-outline flex-fill mb-0 text-danger text-center' ><span>" + res.message + "</span></div>";
                    }
                },
                error: function(request, status, error) {
                    alert("Not Call API");
                }
            })
        }
    }
}