
/**
 * danh muc ưa thích
 */
let updateWishlist = (productid)=>{

    let data = {
        "productid" : productid
    }
    $.ajax({
        url : '/customer/wishlist/update',
        method : 'POST',
        dataType : 'json',
        contentType : 'application/json; charset=utf-8',
        data : JSON.stringify(data),
        success: function(res) {
            if(res.status === 1){
                Swal.fire({
                    icon: 'success',
                    title: res.message,
                    showConfirmButton: false,
                    timer: 1500
                }).then((ok) => {
                    if (ok) window.location.reload();
                })
            }else{
                if(res.code = 404){
                    Swal.fire({
                        icon: 'error',
                        title: res.message,
                        showConfirmButton: false,
                        timer: 1000
                    }).then((ok) =>{
                        if(ok)
                            window.location.href = '/login';
                    })
                }else {
                    sweetalert_err(res.message)
                }
            }
        },
        error: function(request, status, error) {
            alert("Not Call API");
        }
    })

}


