let createOrder = ()=>{
    Swal.fire({
        title: 'Are you sure?',
        text: "Bạn muốn tạo đơn hàng không ?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes'
    }).then((result) => {
        if(result.value){
            let data = {
                "ship_fee":25000,
                "discount":parseInt($('#orderDiscount').text()),
                "delivery":parseFloat($('#orderDelivery').text()),
                "totalPrice":parseInt($('#orderTotalPrice').text()),
                "fullname": document.getElementById("orderFullName").value, 
                "note": document.getElementById("orderNote").value ,
                "address": document.getElementById("orderAddress").value ,
                "email": document.getElementById("orderEmail").value ,
                "phone": document.getElementById("orderPhone").value ,
                "check_paypal": $('input[name="orderCheckoutType"]:checked').val()
            };
            $.ajax({
                url : '/customer/checkout/create',
                method : 'POST',
                dataType : 'json',
                contentType : 'application/json; charset=utf-8',
                data : JSON.stringify(data),
                success: function(res) {
                    if(res.status === 1){
                        if(res.code === 555){
                            window.location.href = res.data;
                        }else {
                            Swal.fire({
                                icon: 'success',
                                title: res.message,
                                showConfirmButton: false,
                                timer: 1500
                            }).then((ok) =>{
                                if(ok)
                                    // window.location.href = '/customer/order/detail?id='+res.data;
                                    window.location.href = '/customer/order';
                            })
                        }

                    }else{
                        sweetalert_err(res.message)
                    }
                },
                error: function(request, status, error) {
                    alert("Not Call API:" + error.message());
                }
            })

        }

    });
}

/**
 * xóa đơn hàng
 */

function deleteOrder(id) {
    Swal.fire({
        title: 'Are you sure?',
        text: "Bạn muốn xóa đơn hàng này không ?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes'
    }).then((result) => {
        if(result.value){
            let dataDelete = {
                "id": id
            };

            $.ajax({
                url : '/customer/order/cancel',
                method : 'POST',
                dataType : 'json',
                contentType : 'application/json; charset=utf-8',
                data : JSON.stringify(dataDelete),
                success: function(res) {
                    if(res.status === 1){
                        Swal.fire({
                            icon: 'success',
                            title: res.message,
                            showConfirmButton: false,
                            timer: 1500
                        }).then((ok) =>{
                            if(ok)
                                window.location.href = '/customer/order';
                        });
                    }else{
                        if(res.code == 404){
                            window.location.href = '/login';
                        }else {
                            sweetalert_err(res.message)
                        }
                    }
                },
                error: function(request, status, error) {
                    alert("Not Call API " );
                }
            })
        }
    });
};

function confirmShipping(id) {
    Swal.fire({
        title: 'Bạn đã nhận được hàng?',
        // text: "Bạn muốn xóa đơn hàng này không ?",
        // type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes'
    }).then((result) => {
        if(result.value){
            let dataConfirm = {
                "id": id,
                "status" : 2
            };

            $.ajax({
                url : '/customer/order/update',
                method : 'POST',
                dataType : 'json',
                contentType : 'application/json; charset=utf-8',
                data : JSON.stringify(dataConfirm),
                success: function(res) {
                    if(res.status === 1){
                        Swal.fire({
                            icon: 'success',
                            title: res.message,
                            showConfirmButton: false,
                            timer: 1500
                        }).then((ok) =>{
                            if(ok) window.location.reload();
                        });
                    }else{
                        if(res.code == 404){
                            window.location.href = '/login';
                        }else {
                            sweetalert_err(res.message)
                        }
                    }
                },
                error: function(request, status, error) {
                    alert("Not Call API " );
                }
            })
        }
    });
}

