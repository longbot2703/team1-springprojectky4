function reload(){
    location.reload();
}
function sweetalert_success(mess ="Thành công !"){
    Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: mess,
        showConfirmButton: false,
        timer: 1500
    })
}
function sweetalert_err(mess ="Thất bại !"){
    Swal.fire({
        position: 'top-end',
        icon: 'error',
        title: mess,
        showConfirmButton: false,
        timer: 1500
    })
}

function searchProduct(){
    let search = document.getElementById('searchPr').value;
    let currentLocation = window.location.href;
    let url = "";

    if(search.length == 0){
        search = "";
    }
    if(currentLocation.includes("?cate=")){
        if(currentLocation.includes("search=")){
            let indexOf = currentLocation.indexOf("search=");
            url = currentLocation.slice(0,indexOf+7)+search;
        }else {
            url = currentLocation + "&&search="+search;
        }
    }else {
        if(currentLocation.includes("search=")){
            let indexOf = currentLocation.indexOf("search=");
            url = currentLocation.slice(0,indexOf+7)+search;
        }else {
            url = currentLocation + "?search=" + search;
        }
    }
    // console.log(url);
    location.replace(url);
}