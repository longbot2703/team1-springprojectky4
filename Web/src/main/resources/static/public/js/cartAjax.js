let addtoCart = (productid)=>{
    let quantity = "1";
    if (document.getElementById('quantity')) {
        quantity = document.getElementById('quantity').value;
    }
    let data = {
        "productid" : productid,
        "quantity": quantity
    }
    $.ajax({
        url : '/customer/cart/add',
        method : 'POST',
        dataType : 'json',
        contentType : 'application/json; charset=utf-8',
        data : JSON.stringify(data),
        success: function(res) {
            if(res.status === 1){
                sweetalert_success(res.message);
                $('#countCart').html(parseInt($('#countCart').html(), 10)+parseInt(quantity))
            }else{
                sweetalert_err(res.message)
            }
        },
        error: function(request, status, error) {
            alert("Not Call API");
        }
    })
}
/**
 * user deleteCart
 */
let deleteCart = (cartid)=>{
    Swal.fire({
        title: 'Are you sure?',
        text: "Bạn muốn xóa sản phẩm khỏi giỏ hàng!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        if(result.value){
            let data = {
                "id":cartid
            };
            $.ajax({
                url : '/customer/cart/delete',
                method : 'POST',
                dataType : 'json',
                contentType : 'application/json; charset=utf-8',
                data : JSON.stringify(data),
                success: function(res) {
                    if(res.status === 1){
                        Swal.fire({
                            icon: 'success',
                            title: res.message,
                            showConfirmButton: false,
                            timer: 1500
                        }).then((ok) => {
                            if (ok) window.location.reload();
                        })
                    }else{
                        sweetalert_err(res.message)
                    }
                },
                error: function(request, status, error) {
                    alert("Not Call API");
                }
            })
        }

    });
}
let checkoutCart = ()=>{
    let data = {
        "totalPrice": parseInt($('#totalPriceCart').text()),
        "delivery": parseFloat($('#countDelivery').text()),
        "discount": parseInt($('#countDiscount').text()),
    };
    $.ajax({
        url : '/customer/cart/createOrder',
        method : 'POST',
        dataType : 'json',
        contentType : 'application/json; charset=utf-8',
        data : JSON.stringify(data),
        success: function(res) {

            if(res != null){
                window.location.href = '/customer/checkout';
            }
        },
        error: function(request, status, error) {
            alert("Not Call API");
        }
    })

}

// $( document ).ready(function() {
//     countPrice += (parseInt($("#cartProductPrice"+id).text())* parseInt($("#cartProductQuantity"+id).text())) ;
//     $('#totalPriceCart').text(countPrice);
// });
// var countPrice = 0;
// let checkCart = (id)=>{
//     if ($('#checkCart'+id).is(":checked"))
//     {
//
//     }else {
//         countPrice -= (parseInt($("#cartProductPrice"+id).text())* parseInt($("#cartProductQuantity"+id).text()));
//     }
//
//
// }